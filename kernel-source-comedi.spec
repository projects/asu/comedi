%define module_name comedi
%define module_version 0.7.76
%define module_release alt11

#### MODULE SOURCES ####
Name: kernel-source-%module_name
Version: %module_version
Release: %module_release

Summary: Sources for COMEDI (Data Acquisition driver for several DAQ boards)

License: GPL
Group: Development/Kernel
URL: http://www.comedi.org

BuildArch: noarch

BuildPreReq: kernel-build-tools

Source: http://www.comedi.org/download/%module_name-%module_version.tar
#Source1: fastwel.h

#The two drivers: adv_pci1710 and adv_pci_dio are temporary disabled due to strange error coming with new kernel
#Patch0: temporary-disable.patch
#Patch1: unioxx5.patch
#Patch2: adv_pci1712.patch
#Patch3: kernel-source-comedi-0.7.76-fastwel-alt.patch
#Patch4: kernel-source-comedi-0.7.76-newkernelfix-alt.patch

%description
Comedi is a data acquisition driver for Linux. 
This package contains its sources for Linux kernel.

%package -n libcomedi-fastwel-devel
Summary: Extensions header for comedi Fastwel drivers.
Group: Development/C

%description -n libcomedi-fastwel-devel
This package contains a header file, fastwel.h where some
instructions are introduced for more convenient work with
Fastwel cards.

#### MODULE DOCUMENTATION ####
%package -n kernel-doc-%module_name
Version: %module_version
Summary: Linux %module_name modules documentation
Group: Development/Kernel

%description -n kernel-doc-%module_name
%module_name modules documentation for Linux kernel
pwd

%prep
%setup -q -n %module_name-%version
#%patch1 -p1
#%patch2 -p1
#%patch3 -p1
#%patch4 -p1
#%patch0 -p1
#%__autoreconf -i -f



%install
mkdir -p %buildroot%_includedir
cp -a include/fastwel.h %buildroot%_includedir/
%__mkdir_p %buildroot%_defaultdocdir
%__mv Documentation/comedi %buildroot%_defaultdocdir/%module_name-doc-%module_version
#%__mv README TODO %buildroot%_defaultdocdir/%module_name-doc-%module_version
%__mkdir_p %buildroot%kernel_src/kernel-source-%module_name-%module_version
%__mv * %buildroot%kernel_src/kernel-source-%module_name-%module_version
#%__mv include %buildroot%kernel_src/kernel-source-%module_name-%module_version/include
cd %buildroot%kernel_src
%__tar -c kernel-source-%module_name-%module_version | bzip2 -c > \
    %buildroot%kernel_src/kernel-source-%module_name-%module_version.tar.bz2

%files
%kernel_src/kernel-source-%module_name-%module_version.tar.bz2

#%%files -n kernel-doc-%module_name
#%doc %_defaultdocdir/%module_name-doc-%module_version

%files -n libcomedi-fastwel-devel
%_includedir/*

%changelog
* Sun Feb 26 2012 Pavel Vainerman <pv@altlinux.ru> 0.7.76-alt11
- fixed bug in unioxx5

* Fri Nov 11 2011 Pavel Vainerman <pv@altlinux.ru> 0.7.76-alt10
- fixed bug in km1624

* Fri Nov 11 2011 Pavel Vainerman <pv@altlinux.ru> 0.7.76-alt8
- add new driver adv_pci1750

* Wed Nov 09 2011 Pavel Vainerman <pv@altlinux.ru> 0.7.76-alt7
- enable 'adv_pci_dio' driver for build

* Sat Nov 05 2011 Pavel Vainerman <pv@altlinux.ru> 0.7.76-alt6
- add new modules

* Fri Nov 04 2011 Pavel Vainerman <pv@altlinux.ru> 0.7.76-alt5
- add new drivers (km1624,km5856,cpc307_dio etc)

* Sat Mar 05 2011 Pavel Vainerman <pv@altlinux.ru> 0.7.76-alt4
- add cpc307_dio driver

* Sat Oct 03 2009 Pavel Vainerman <pv@etersoft.ru> 0.7.76-alt2
- .. 

* Wed Apr 22 2009 Yury Aliaev <mutabor@altlinux.ru> 0.7.76-alt1.1
- unioxx5 crashes at driver removing fixed

* Fri Mar 27 2009 Yury Aliaev <mutabor@altlinux.ru> 0.7.76-alt1.1
- unioxx5 driver fixed (now it works)

* Thu Mar 19 2009 Yury Aliaev <mutabor@altlinux.ru> 0.7.76-alt1
- new version
- some more Fastwel cards support added
- separate package libomedi-devel-fastwel containing extended config
  instructions for Fastwel cards

* Mon May 14 2007 Lunar Child <luch@altlinux.ru> 0.7.73-alt0.1
- add new drivers + patches.

* Mon Sep 05 2005 Vitaly Lipatov <lav@altlinux.ru> 0.7.70-alt0.1
- build with new kernel policy

* Wed Apr 02 2003 Vitaly Lipatov <lav@altlinux.ru> 2.4.20_alt7_0.7.66-alt4
- build with new kernel

* Sun Jan 12 2003 Vitaly Lipatov <lav@altlinux.ru> 2.4.20_alt0.8_0.7.66-alt3
- build with new kernel
- add kernel version to package's version
- now doc/comedi* dir owned by the package

* Sun Dec 01 2002 Vitaly Lipatov <lav@altlinux.ru> 0.7.66-alt2
- add depends to kernel (for modversions.h :()
- change modversions from current kernel to needed kernel

* Mon Nov 25 2002 Vitaly Lipatov <lav@altlinux.ru> 0.7.66-alt1
- new version
- use optflags in CFLAGS
- set correct path to kernel headers

* Mon Nov 11 2002 Vitaly Lipatov <lav@altlinux.ru> 0.7.65-alt0.3
- removed patch for source, all hacks done in spec now

* Sat Nov 09 2002 Vitaly Lipatov <lav@altlinux.ru> 0.7.65-alt0.2
- rewrite spec 
- hack all makes in driver source for kernel source independing

* Wed Nov 06 2002 Vitaly Lipatov <lav@altlinux.ru> 0.7.65-alt0.1
- first build for ALT Linux system

* Wed Feb 20 2002 Tim Ousley <tim.ousley@ni.com>
- initial build of comedi RPM


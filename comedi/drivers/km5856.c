/***************************************************************************\
 *                                                                         *
 *  comedi/drivers/km5856.c                                                *
 *  Driver for Kaskod KM5856 board (digital input and output)              *
 *                                                                         *
 *  Copyright (C) 2011 Pavel Vainerman (pv) [pv@etersoft.ru]               *
 *                                                                         *
 *  COMEDI - Linux Control and Measurement Device Interface                *
 *  Copyright (C) 1998,2000 David A. Schleef <ds@schleef.org>              *
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
 *                                                                         *
 *                                                                         *
 *  driver name: km5856.o                                                  *
 *  kernel module: km5856.ko                                               *
 *                                                                         *
 *  This card supports digital input and output                            *
 *  channels range: 0 .. 15 DI channels   (subdev 0)                       *
 *                  0 .. 15 DO channels   (subdev 1)                       *
 *                                                                         *
 *  # comedi_config /dev/comedi0 km5856 0x150                              *
 *                                                                         *
 *  Parameter 1: IO base address                                           *
 *                                                                         *
 * драйвер написан с использованием примеров из документации на плату      *
 * http://www.kaskod.ru                                                    *
\***************************************************************************/

#include <linux/comedidev.h>
#include <linux/ioport.h>

#define DRIVER_NAME "km5856"
#define KM5856_SIZE 0x05
#define SUBDEV_NUM 2
#define KM5856_DO_NCHANS 16
#define KM5856_DI_NCHANS 16

//------------------------------------------------------------------------------
// 'private' structure of subdevice
typedef struct km5856_subd_private {
  unsigned short prev_val;
} km5856_private;
#define devpriv ((km5856_private *) dev->private)

//------------------------------------------------------------------------------

static int km5856_attach(comedi_device* dev, comedi_devconfig* it);
static int km5856_detach(comedi_device* dev);

static int km5856_do_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							 comedi_insn* insn, lsampl_t* data);
static int km5856_do_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data);
static int km5856_di_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							 comedi_insn* insn, lsampl_t* data);
//-----------------------------------------------------------------------------

static comedi_driver km5856_driver = {
 driver_name: DRIVER_NAME,
 module:      THIS_MODULE,
 attach:      km5856_attach,
 detach:      km5856_detach
};

COMEDI_INITCLEANUP(km5856_driver);

//------------------------------------------------------------------------------
// Init card
static int km5856_attach(comedi_device* dev, comedi_devconfig* it)
{
  int iobase; //id, num, i;
  comedi_subdevice* subdev;

  iobase = it->options[0];
  
  dev->board_name = DRIVER_NAME;
  dev->iobase = 0;

  if(!request_region(iobase, KM5856_SIZE, DRIVER_NAME)) {
	printk(KERN_ERR "comedi%d(km5856): I/O ports conflict (iobase=%d)\n", dev->minor,iobase);
	return -1;
  }

  dev->iobase = iobase;

  /* defining device type */
  printk("comedi%d: cannot check type of the card, decided KM5856\n", dev->minor);

  if(alloc_subdevices(dev, SUBDEV_NUM) < 0) {
	printk(KERN_ERR "comedi%d(km5856): out of memory\n", dev->minor);
	return -ENOMEM;
  }

  if (alloc_private(dev, sizeof(km5856_private)) < 0) {
	printk(KERN_ERR "comedi%d(km5856): out of memory\n", dev->minor);
	return -ENOMEM;
  }

  memset(devpriv, 0, sizeof(km5856_private));
  devpriv->prev_val 	= 0xFFFF;
  outw(0xFFFF, dev->iobase);
	
  /* initializing subdevice */
  subdev = &dev->subdevices[0];
  subdev->type 		= COMEDI_SUBD_DI;
  subdev->subdev_flags = SDF_READABLE;
  subdev->n_chan 	= KM5856_DI_NCHANS;
  subdev->maxdata 	= 0xFF;
  subdev->insn_read  = km5856_di_subd_read;
	
  subdev = &dev->subdevices[1];
  subdev->type 		= COMEDI_SUBD_DIO; /* COMEDI_SUBD_DO; */
  subdev->subdev_flags = SDF_READABLE | SDF_WRITABLE;
  subdev->n_chan 	= KM5856_DO_NCHANS;
  subdev->maxdata 	= 0xFF;
  subdev->insn_write = km5856_do_subd_write;
  subdev->insn_read  = km5856_do_subd_read;

  printk("comedi%d: KM5856 attached\n", dev->minor);
  return 0;
}


//------------------------------------------------------------------------------
static int km5856_do_subd_read( comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data )
{
	int chan = CR_CHAN(insn->chanspec);
	if( chan<0 || chan >= KM5856_DO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(km5856_do_subdev_read): undefined chan %d. chan range is 0..%d\n", dev->minor, chan, KM5856_DO_NCHANS);
		return -EINVAL;
	}

	(*data) = ( (devpriv->prev_val >> chan)&1 ? 0 : 1 ); // значение у нас инвертированное хранится
	return insn->n;
}

//------------------------------------------------------------------------------
static int km5856_do_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data)
{
	unsigned int chan = CR_CHAN(insn->chanspec);
	unsigned int mask = 1 << (chan%16);
	unsigned int val;

	if( chan < 0 || chan >= KM5856_DO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(km5856_subdev_write): undefined chan %d. chan range is 0..%d\n", dev->minor, chan,KM5856_DO_NCHANS);
		return -EINVAL;
	}
	
	val = devpriv->prev_val;

	// у платы логика инвертирована 0 - включить, 1 - выключить..
	if (*data)
		val &= ~mask;
	else
		val |= mask;
		
	outw(val ,dev->iobase);
	devpriv->prev_val = val;

	return insn->n;
}
//------------------------------------------------------------------------------
static int km5856_di_subd_read( comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data )
{
	unsigned int chan = CR_CHAN(insn->chanspec);

    if( chan<0 || chan >= KM5856_DI_NCHANS )
	{
		printk(KERN_ERR "comedi%d(km5856_di_subdev_read): undefined chan %d. chan range is 0..%d\n", dev->minor, chan, KM5856_DI_NCHANS);
		return -EINVAL;
	}

	// в этой плате "инвертированная" логика.
    // 1 - нет сигнала
	// 0 - есть сигнал
	// поэтому "переворачиваем"..
	(*data) = ( inw(dev->iobase)>>chan ) & 1 ? 0 : 1;
	return insn->n;
}

//------------------------------------------------------------------------------
static int km5856_detach( comedi_device* dev )
{
	printk("comedi%d: remove km5856\n",dev->minor);

	if( dev->iobase )
		release_region(dev->iobase, KM5856_SIZE);
	
	return 0;
}
//------------------------------------------------------------------------------

/***************************************************************************\
 *                                                                         *
 *  comedi/drivers/cpc307_dio.c                                            *
 *  Driver for Fastwel CPC307 DIO                                          *
 *                                                                         *
 *  Copyright (C) 2011 Pavel Vainerman (pv) [pv@etersoft.ru]               *
 *                                                                         * 
 *  Used code from Fastwel CPC307 DIO driver. Release 0.21                 *
 *  Author: Konstantin Chaplaev <chaplaev@fastwel.ru>                      *
 *  Copyright (C) 2010 Fastwel Co Ltd. All rights reserved.                * 
 *                                                                         *
 *  COMEDI - Linux Control and Measurement Device Interface                *
 *  Copyright (C) 1998,2000 David A. Schleef <ds@schleef.org>              *
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
 *                                                                         *
 *  --------------------------------                                       *
 *   Fastwel CPC307 DIO specification                                      *
 *  --------------------------------                                       *
 *                                                                         *
 *  driver name: cpc307_dio.o                                              *
 *  kernel module: cpc307.ko                                               *
 *                                                                         *
 *  This card supports digital input.                                      *
 *  channels range: 0 .. 7 DIO channels                                    *
 *  Special channels (for details, see CPC307 documentation):              *
 *  channel 8  - HL2                                                       *
 *  channel 9  - HL3                                                       *
 *  channel 10 - SA1 переключатель 1 (read only)                           *
 *  channel 11 - SA1 переключатель 2 (read only)                           *
 *                                                                         *
 *  Default: setup all channels to DI                                      *
 *                                                                         *
\***************************************************************************/
#include <linux/comedidev.h>
#include <linux/ioport.h>

#define DRIVER_NAME "cpc307_dio"
#define DIO_SIZE 1
#define SUBDEV_NUM 1
#define DIO_NCHANS 12

#define GPIO0_DATA	0x78
#define GPIO0_DIR	0x98
#define GPIO2_ADDR	0x7A

#define DIO_SPEC_CHAN 8
#define LED2_CHAN	8
#define LED2_NBIT	0x4
#define LED3_CHAN	9
#define LED3_NBIT	0x5
#define SA1_P1_CHAN	10
#define SA1_P1_NBIT	0x6
#define SA1_P2_CHAN	11
#define SA1_P2_NBIT	0x7

//------------------------------------------------------------------------------
// 'private' structure of subdevice
typedef struct cpc307_dio_subd_private {
  uint8_t dir;  // direction: 0 - input 1 - output
  uint8_t val;	// state
} cpc307_dio_private;
#define devpriv ((cpc307_dio_private *) dev->private)

//------------------------------------------------------------------------------
static int cpc307_dio_attach(comedi_device* dev, comedi_devconfig* it);
static int cpc307_dio_detach(comedi_device* dev);

static int cpc307_dio_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							 comedi_insn* insn, lsampl_t* data);
static int cpc307_dio_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data);

static int cpc307_dio_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data);
//-----------------------------------------------------------------------------

static comedi_driver cpc307_dio_driver = {
 driver_name: DRIVER_NAME,
 module:      THIS_MODULE,
 attach:      cpc307_dio_attach,
 detach:      cpc307_dio_detach
};

COMEDI_INITCLEANUP(cpc307_dio_driver);

//------------------------------------------------------------------------------
// Init card

static int cpc307_dio_attach(comedi_device* dev, comedi_devconfig* it)
{
  comedi_subdevice* subdev;
 
  dev->board_name = DRIVER_NAME;
  dev->iobase = 0;

  if(!request_region(GPIO0_DATA, DIO_SIZE, DRIVER_NAME)) {
	printk(KERN_ERR "comed%d(cpc307_dio): I/O ports conflict\n", dev->minor);
	return -EIO;
  }
  
  dev->iobase = GPIO0_DATA;
  
  if(alloc_subdevices(dev, SUBDEV_NUM) < 0) {
	printk(KERN_ERR "comedi%d(cpc307_dio): out of memory\n", dev->minor);
	return -ENOMEM;
  }

  if (alloc_private(dev, sizeof(cpc307_dio_private)) < 0) {
	printk(KERN_ERR "comedi%d(cpc307_dio): out of memory\n", dev->minor);
	return -ENOMEM;
  }

  memset(devpriv, 0, sizeof(cpc307_dio_private));

// Read current direction
// devpriv->dir = inb(GPIO0_DIR);
// Set default all channels to INPUT
  devpriv->dir = 0;
  outb(0,GPIO0_DIR);
  
  devpriv->val = inb(GPIO0_DATA);
  
  /* initializing output subdevice */
  subdev 		= &dev->subdevices[0];
  subdev->type 		= COMEDI_SUBD_DIO;
  subdev->subdev_flags = SDF_READABLE | SDF_WRITABLE;
  subdev->n_chan 	= DIO_NCHANS;
  subdev->maxdata 	= 0xFF;
  subdev->insn_write = cpc307_dio_subd_write;
  subdev->insn_read  = cpc307_dio_subd_read;
  subdev->insn_config = cpc307_dio_insn_config;

  printk("comedi%d: (cpc307_dio) attached\n", dev->minor);
  return 0;
}

//------------------------------------------------------------------------------
static int cpc307_read_spec_channel( int iobase, uint8_t chan )
{
	uint8_t val = inb(iobase);
	return (val >> chan) & 1;
}

static int cpc307_write_spec_channel( int iobase, uint8_t chan, int data )
{
	uint8_t mask = (1 << chan);
	uint8_t val = inb(iobase);
	
	if (data)
		val |= mask;
	else
		val &= ~mask;
		
	outb(val ,iobase);
	return 1;
}
//------------------------------------------------------------------------------
static int cpc307_dio_subd_read( comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data )
{
	int chan = CR_CHAN(insn->chanspec);
	cpc307_dio_private* priv = dev->private;
	
	if( chan < 0 || chan >= DIO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(cpc307_dio_subdev_read): undefined chan %d. DIO channel range is 0..%d\n", dev->minor, chan, DIO_NCHANS-1);
		return -EINVAL;
	}

	if( chan >= DIO_SPEC_CHAN )
	{
		if( chan == LED2_CHAN )
		{
			(*data) = cpc307_read_spec_channel(GPIO2_ADDR,LED2_NBIT);
			return 1;
		}	
		else if( chan == LED3_CHAN )
		{
			(*data) = cpc307_read_spec_channel(GPIO2_ADDR,LED3_NBIT);
			return 1;
		}
		else if( chan == SA1_P1_CHAN )
		{
			(*data) = cpc307_read_spec_channel(GPIO2_ADDR,SA1_P1_NBIT);
			return 1;
		}
		else if( chan == SA1_P2_CHAN )
		{
			(*data) = cpc307_read_spec_channel(GPIO2_ADDR,SA1_P2_NBIT);
			return 1;
		}		
		printk(KERN_ERR "comedi%d(cpc307_dio_subdev_read): undefined chan %d. DIO channel range is 0..%d\n", dev->minor, chan, DIO_NCHANS-1);
		return -EINVAL;
	}

	if( (priv->dir >> chan)&1 )
	{
		(*data) = (priv->val >> chan) & 1;
		return 1;
	}
	
	priv->val = inb(dev->iobase);
	(*data) = (priv->val >> chan) & 1;
	
	return 1;
}

//------------------------------------------------------------------------------
static int cpc307_dio_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data)
{
	unsigned short chan = CR_CHAN(insn->chanspec);
	uint8_t mask;
	cpc307_dio_private* priv = dev->private;

//	if( chan == LED_CHAN_NUM )
//		return cpc307_dio_set_led_state(dev,data);

	if( chan < 0 || chan >= DIO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(cpc307_dio_subdev_write): undefined chan %d. DIO channel range is %d..%d\n", dev->minor, chan, 0, DIO_NCHANS-1);
		return -EINVAL;
	}
	
	if( chan >= DIO_SPEC_CHAN )
	{
		if( chan == LED2_CHAN )
			return cpc307_write_spec_channel(GPIO2_ADDR, LED2_NBIT, (*data));
		else if( chan == LED3_CHAN )
			return cpc307_write_spec_channel(GPIO2_ADDR, LED3_NBIT, (*data));
		
		printk(KERN_ERR "comedi%d(cpc307_dio_subdev_write): undefined chan %d. DIO channel range is %d..%d\n", dev->minor, chan, 0, DIO_NCHANS-1);
		return -EINVAL;
	}
	
	mask = (1 << chan);

	if( !( (priv->dir >> chan)&1 ) )
	{
		printk(KERN_ERR "comedi%d(cpc307_dio_subdev_write): channel %d is configured as a INPUT.\n", dev->minor, chan);
		return -EINVAL;
	}
	
	if (*data)
		priv->val |= mask;
	else
		priv->val &= ~mask;
	
	outb(priv->val,dev->iobase);
	return 1;
}

//------------------------------------------------------------------------------
static int cpc307_dio_detach( comedi_device* dev )
{
	printk("comedi%d: remove cpc307_dio\n",dev->minor);
	if( dev->iobase )
		release_region(dev->iobase, DIO_SIZE);
	
	return 0;
}
//------------------------------------------------------------------------------
static int cpc307_dio_insn_config(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data)
{
	unsigned short chan = CR_CHAN(insn->chanspec);
	uint8_t mask;
	cpc307_dio_private* priv = dev->private;
	
	if( chan < 0 || chan >= DIO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(cpc307_dio_insn_config): undefined chan %d. DIO channel range is %d..%d\n", dev->minor, chan, 0, DIO_NCHANS-1);
		return -EINVAL;
	}	

	mask = (1 << chan);
	//priv->dir = inb(dev->iobase);
	
	switch( data[0] ) 
	{
		case INSN_CONFIG_DIO_INPUT:
			priv->dir &= ~mask;
//			printk("comedi%d(cpc307_dio_insn_config): set INPUT for channel %d \n", dev->minor, chan);
			break;
		
		case INSN_CONFIG_DIO_OUTPUT:
			priv->dir |= mask;
//			printk("comedi%d(cpc307_dio_insn_config): set OUTPUT for channel %d \n", dev->minor, chan);
			break;
		
		case INSN_CONFIG_DIO_QUERY:
			data[1] = ( priv->dir & (1<<chan) ) ? COMEDI_OUTPUT : COMEDI_INPUT;
			return insn->n;
		
		default:
			printk(KERN_ERR "comedi%d(cpc307_dio_do_insn_config): invalid command\n", dev->minor);
			return -EINVAL;
	}

	outb(priv->dir,GPIO0_DIR);
	return insn->n;
}

/***************************************************************************\
 *                                                                         *
 *  comedi/drivers/di32_5.c                                                *
 *  Driver for Fastwel DI32-5 (digital input) boards.                      *
 *                                                                         *
 *  Copyright (C) 2008 Pavel Vainerman (pv) [pv@etersoft.ru]               *
 *  Copyright (C) 2009 Yury Aliaev (mutabor) [mutabor@etersoft.ru]         *
 *                                                                         *
 *  COMEDI - Linux Control and Measurement Device Interface                *
 *  Copyright (C) 1998,2000 David A. Schleef <ds@schleef.org>              *
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
 *                                                                         *
 *                                                                         *
 *  --------------------------------                                       *
 *   Fastwel DI32-5 specification                                          *
 *  --------------------------------                                       *
 *                                                                         *
 *  driver name: di32_5.o                                                  *
 *  kernel module: di32_5.ko                                               *
 *                                                                         *
 *  This card supports digital input.                                      *
 *  channels range: 0 .. 31 DI channels                                    *
 *                                                                         *
 *  # comedi_config /dev/comedi0 di32_5 0x150                              *
 *  or                                                                     *
 *  # comedi_config /dev/comedi0 di32_5 0x150,1                            *
 *                                                                         *
 *  Parameter 1: I/O base address                                          *
 *                                                                         *
 *  Parameter 2 (optional): bounce suppression time:                       *
 *                0 -- 40 ns, 1 -- 400 ns, 2 -- 4.5 us, 3 -- 140 us        *
 *                                                                         *
 *  Pseudo-channels (using for control purposes and extended functions):   *
 *  32 + channel (read) -- measuring frequency at given channel            *
 *                                                                         *
 *  Instructions using to control the device:                              *
 *  INSN_CONFIG_COUNTER                                                    *
 *  Configuring frequency meter, first parameter -- number of measuring    *
 *  frequency periods, second -- filling frequency code:                   *
 *   F(MHz) = 25 / (code + 1)                                              *
 *                                                                         *
 *  INSN_CONFIG_DI_MODE                                                    *
 *  sets input mode and event type: 0 -- input state reading,              *
 *  1-3 -- events capturing: 1: front, 2: rear, 3: front then rear         *
 *                                                                         *
 *  INSN_CONFIG_BOUNCE_SUPPRESSION                                         *
 *  sets bounce suppression time, see Parameter 2                          *
 *                                                                         *
\***************************************************************************/

#include <linux/comedidev.h>
#include <linux/ioport.h>

#define DRIVER_NAME "di32_5"
#define DI32_5_SIZE 0x10
#define DI32_5_NUM_OF_CHANS 32

/* Ports */
#define DI32_5_BOUNCE 5 /* Bounce suppression control port */
#define DI32_5_EVENT 6 /* Event type control port */
#define DI32_5_FREQ_CONTROL 7 /* Control register of frequency meter */
#define DI32_5_FREQ_NUM 8 /* Number of periods */
#define DI32_5_FREQ_FIL 9 /* Filling frequency code */
#define DI32_5_FREQ_DATA 10 /* Resulting frequency meter value (0x10000 means overfill) */

#define DI32_5_MEASURE_FREQ 32 /* Measure the frequency at given channel */

/* 'private' structure for the device */
typedef struct di32_5_private {
  int mode; /* 0 -- input mode, 1 -- event mode */
  int bounce_byte; /* Store the current bounce suppresseion byte */
  int event_byte; /* Store the current event type byte */
  int periods; /* Periods of measuring frequency */
} di32_5_private;
#define devpriv ((di32_5_private *) dev->private)

static int di32_5_attach( comedi_device* dev, comedi_devconfig* it );
static int di32_5_subdev_write( comedi_device* dev, comedi_subdevice* subdev,
								comedi_insn* insn, lsampl_t* data );
static int di32_5_subdev_read( comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data );
static int di32_5_detach( comedi_device* dev );
static int di32_5_insn_config( comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data );

static comedi_driver di32_5_driver = {
 driver_name:   DRIVER_NAME,
 module:        THIS_MODULE,
 attach:        di32_5_attach,
 detach:        di32_5_detach
};

COMEDI_INITCLEANUP(di32_5_driver);

static int di32_5_attach( comedi_device* dev, comedi_devconfig* it )
{
	comedi_subdevice* subdev;
	int iobase;
	int id=0,num=0;

	iobase = it->options[0];

	dev->board_name 	= DRIVER_NAME;
	dev->iobase 		= iobase;

	id 	= inb(iobase + 14);
	num = inb(iobase + 15);

	if( id!='D' || num!=0 ) {
		printk(KERN_ERR "your card must has 'D00' subdevices\n");
		return -EINVAL;
	}

	if(alloc_subdevices(dev,1) < 0) {
		printk(KERN_ERR "out of memory\n");
		return -ENOMEM;
	}

	if(!request_region(iobase, DI32_5_SIZE, DRIVER_NAME)) {
		printk(KERN_ERR "comedi%d: I/O port conflict\n", dev->minor);
		return -EIO;
	}

	if (alloc_private(dev, sizeof(di32_5_private)) < 0) {
		printk(KERN_ERR "comedi%d: out of memory\n", dev->minor);
		return -ENOMEM;
	}
	memset(devpriv, 0, sizeof(di32_5_private));

	/* initial subdevice for digital input */
	subdev = &dev->subdevices[0];
	subdev->type 		= COMEDI_SUBD_DIO;
	subdev->subdev_flags = SDF_READABLE | SDF_WRITABLE;
	subdev->n_chan 		=  DI32_5_NUM_OF_CHANS * 2; /* Upper 32 channels are used for frequency measuring */
	subdev->maxdata 	= 0xFFF;
	subdev->range_table = &range_digital;
	subdev->insn_read 	= di32_5_subdev_read;
	subdev->insn_write  = di32_5_subdev_write;
	subdev->insn_config = di32_5_insn_config;

	if(COMEDI_NDEVCONFOPTS > 1) { /* Optional parameter: bounce suppression type */
		switch(it->options[1]) {
			case 0:
				devpriv->bounce_byte = 0;
				break;
			case 1:
				devpriv->bounce_byte = 0x55;
				break;
			case 2:
				devpriv->bounce_byte = 0xaa;
				break;
			case 3:
				devpriv->bounce_byte = 0xff;
				break;
			default:
				printk(KERN_ERR "comedi%d(di32_5_subdev_write): incorrect value %i. Value 0..3\n", dev->minor, it->options[1]);
				return -EINVAL;
		}
		outb(devpriv->bounce_byte, dev->iobase + DI32_5_BOUNCE);
	}

	outb(0, dev->iobase + 0); // set BNK=0
	devpriv->mode = 0;

	printk("comedi%d: (DI32_5) attached\n", dev->minor);
	return 1;
}

static int di32_5_subdev_read(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data)
{
	unsigned int chan = CR_CHAN(insn->chanspec);

	if((chan >= DI32_5_MEASURE_FREQ) && (chan < DI32_5_MEASURE_FREQ + DI32_5_NUM_OF_CHANS)) {
	/* Measuring frequency at given channel. Before this one must set the correct number of periods and
	filling frequency code */
		int retcode;
		chan -= DI32_5_MEASURE_FREQ;

		outb(0x80 | chan, dev->iobase + DI32_5_FREQ_CONTROL);
		outb(chan, dev->iobase + DI32_5_FREQ_CONTROL); /* Counter reset */

		outb(devpriv->periods, dev->iobase + DI32_5_FREQ_NUM); /* Number of periods should be restored before each measurement */
		outb(0x80 | chan, dev->iobase + DI32_5_FREQ_CONTROL);
		while(!((retcode = inb(dev->iobase + DI32_5_FREQ_CONTROL)) & 0x80))
			comedi_udelay(10); /* Wait 10 us if not ready */

		*data = (retcode & 0x40) ? 0x10000 : inw(dev->iobase + DI32_5_FREQ_DATA);
		return 1; /* Should we always return 1 nevertheless of overfill? I mean yes, cause this is
			     not hw error. Or maybe -ETIMEOUT? */
	}

	*data = inb(dev->iobase + (chan >> 3) + 1);
	*data >>= (chan & 0x7); /* lower 3 bits in the channel number give us the desired offset */
	*data &= 0x1; /* Only the lower bit */

	return 1;
}

static int di32_5_subdev_write( comedi_device* dev, comedi_subdevice* subdev,
								 comedi_insn* insn, lsampl_t* data )
{
	unsigned int chan = CR_CHAN(insn->chanspec);

	if(chan < DI32_5_NUM_OF_CHANS) { /* Clear event in event mode */
		if(!devpriv->mode) {
			printk(KERN_ERR "comedi%d(di32_5_subdev_write): WRITE not supported in direct mode (channel=%d)\n", dev->minor,chan);
			return -EINVAL;
		} else {
			outb(1 << (chan & 0x7), dev->iobase + (chan >> 3) + 1);
			return 1;
		}
	}

	printk(KERN_ERR "comedi%d(di32_5_subdev_write): inappropriate channel number: %d (should be 0..31)\n", dev->minor, chan);
	return -EINVAL;
}

static int di32_5_insn_config( comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data )
{
	int chan = CR_CHAN(insn->chanspec);
	int mask;

	switch(data[0]) {
		case INSN_CONFIG_DIO_INPUT: /* Do nothing */
			break;
		case INSN_CONFIG_BOUNCE_SUPPRESSION: /* Bounce suppression for the group of inputs, data[1]: code of suppr. time */
			if((data[1] < 0) || (data[1] > 3)) {
				printk(KERN_ERR "comedi%d(di32_5_subdev_write): incorrect value %i. Value 0..3\n", dev->minor, *data);
				return -EINVAL;
			}
			mask = 0x3 << ((chan >> 2) & 0x6);
			devpriv->bounce_byte &= (~mask);
			devpriv->bounce_byte |= (*data) << ((chan >> 2) & 0x6);
			outb(devpriv->bounce_byte, dev->iobase + DI32_5_BOUNCE);

			break;
		case INSN_CONFIG_DI_MODE: /* Input mode, data[1]: 0 -- direct sampling, 1 -- front, 2 -- rear, 3 -- front then rear */
			outb(devpriv->mode = (data[1] ? 1 : 0), dev->iobase);
			if(!data[1]) /* Direct sampling, further steps can be omitted */
				break;

			if((data[1] < 0) || (data[1] > 3)) {
				printk(KERN_ERR "comedi%d(di32_5_insn_config): incorrect value %i. Value 0..3\n", dev->minor, *data);
				return -EINVAL;
			}
			mask = 0x3 << ((chan >> 2) & 0x6);
			devpriv->event_byte &= (~mask);
			devpriv->event_byte |= (data[1]) << ((chan >> 2) & 0x6);
			outb(devpriv->event_byte, dev->iobase + DI32_5_EVENT);

			break;
		case INSN_CONFIG_COUNTER: /* Set frequency meter, data[1] -- number of measuring frequency periods,
					     data[2] -- filling frequency code */
			devpriv->periods = data[1]; /* Should be remembered and restored at each measurement */
			outb(data[2], dev->iobase + DI32_5_FREQ_FIL);
			break;
		default:
			printk(KERN_ERR "comedi%d(di32_5_insn_config): incorrect instruction %i.\n", dev->minor, data[0]);
			return -EINVAL;
		break;
	}

	return insn->n;
}

static int di32_5_detach(comedi_device* dev)
{
	printk("comedi%d: remove di32_5\n",dev->minor);

	if(dev->iobase)
		release_region(dev->iobase, DI32_5_SIZE);

	return 0;
}

/***************************************************************************\
 *                                                                         *
 *  comedi/drivers/do32.c                                                  *
 *  Driver for Fastwel DO32 (analog output) boards.                        *
 *                                                                         *
 *  Copyright (C) 2008 Larik Ishkulov (gentro) [gentro@etersoft.ru]        *
 *  Copyright (C) 2009 Yury Aliaev (mutabor) [mutabor@etersoft.ru]         *
 *                                                                         *
 *  COMEDI - Linux Control and Measurement Device Interface                *
 *  Copyright (C) 1998,2000 David A. Schleef <ds@schleef.org>              *
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
 *                                                                         *
 *                                                                         *
 *  --------------------------------                                       *
 *   Fastwel DO32 specification                                            *
 *  --------------------------------                                       *
 *                                                                         *
 *  driver name: do32.o                                                    *
 *  kernel module: do32.ko                                                 *
 *                                                                         *
 *  This card supports digital output.                                     *
 *  channels range: 0 .. 31 DO channels                                    *
 *                                                                         *
 *                                                                         *  
\***************************************************************************/

#include <linux/comedidev.h>
#include <linux/ioport.h>

#define DRIVER_NAME "do32"
#define DO32_SIZE 0x10
#define SUBDEV_NUM 1
#define DO32_DO_NCHANS 32

#define LSB 1.22

//------------------------------------------------------------------------------
// 'private' structure of subdevice
typedef struct do32_subd_private {
  int asp_iobase;
  unsigned short buff[2];
} do32_private;
#define devpriv ((do32_private *) dev->private)

//------------------------------------------------------------------------------

static int do32_attach(comedi_device* dev, comedi_devconfig* it);
static int do32_detach(comedi_device* dev);

static int do32_do_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							 comedi_insn* insn, lsampl_t* data);
static int do32_do_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data);

static int do32_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data);


//-----------------------------------------------------------------------------

static comedi_driver do32_driver = {
 driver_name: DRIVER_NAME,
 module:      THIS_MODULE,
 attach:      do32_attach,
 detach:      do32_detach
};

COMEDI_INITCLEANUP(do32_driver);

//------------------------------------------------------------------------------
// Init card

static int do32_attach(comedi_device* dev, comedi_devconfig* it)
{
  int iobase; //id, num, i;
  comedi_subdevice* subdev;

// do32 doesn't support any kind of auto-check or auto-detection
  iobase = it->options[0];
  
  dev->board_name = DRIVER_NAME;

  if(!request_region(iobase, DO32_SIZE, DRIVER_NAME)) {
	printk(KERN_ERR "comed-i%d: I/O ports conflict\n", dev->minor);
	return -1;
  }

  /* defined iobase address */
  dev->iobase = iobase;

  /* defining device type */
  printk("comedi%d: cannot check type of the card, decided DO32\n", dev->minor);

  if(alloc_subdevices(dev, SUBDEV_NUM) < 0) {
	printk(KERN_ERR "comedi%d: out of memory\n", dev->minor);
	return -ENOMEM;
  }

  if (alloc_private(dev, sizeof(do32_private)) < 0) {
	printk(KERN_ERR "comedi%d: out of memory\n", dev->minor);
	return -ENOMEM;
  }

  memset(devpriv, 0, sizeof(do32_private));

	/* initializing output subdevice */
	devpriv->asp_iobase 	= dev->iobase;
	subdev 			= &dev->subdevices[0];
	subdev->type 		= COMEDI_SUBD_DIO; /* COMEDI_SUBD_DO; */
	subdev->subdev_flags = SDF_READABLE | SDF_WRITABLE;
	subdev->n_chan 	= DO32_DO_NCHANS;
	subdev->maxdata 	= 0xFFFF;
	subdev->insn_write = do32_do_subd_write;
	subdev->insn_read  = do32_do_subd_read;
	subdev->insn_config = do32_insn_config;	

	printk("comedi%d: (DO32) attached\n", dev->minor);
	return 0;
}


//------------------------------------------------------------------------------
static int do32_do_subd_read( comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data )
{
	int chan = CR_CHAN(insn->chanspec);
	if( chan<0 || chan>= DO32_DO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(do32_do_subdev_read): undefined chan %d. chan range is 0..31\n", dev->minor, chan);
		return -EINVAL;
	}

	(*data) = (devpriv->buff[chan/16] >> chan) & 1;

	return 1;
}

//------------------------------------------------------------------------------
static int do32_do_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data)
{
	unsigned int chan = CR_CHAN(insn->chanspec);
	unsigned int mask = 1 << (chan%16);
	short ind = chan/16;
	unsigned int val;

	if(chan < 0 || chan > 31)
	{
		printk(KERN_ERR "comedi%d(do32_subdev_write): undefined chan %d. chan range is 0..31\n", dev->minor, chan);
		return -EINVAL;
	}
	
	 val = devpriv->buff[ind];

	if (*data)
		val |= mask;
	 else
		val &= ~mask;
		
	outw(val ,dev->iobase + (ind)*2);
	devpriv->buff[ind] = val;

	return 1;
}

//------------------------------------------------------------------------------
static int do32_detach( comedi_device* dev )
{
	printk("comedi%d: remove do32\n",dev->minor);

	release_region(dev->iobase, DO32_SIZE);
	
	return 0;
}
//------------------------------------------------------------------------------
static int do32_insn_config(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data)
{
	return insn->n;
}

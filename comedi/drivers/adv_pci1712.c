//*****************************************************************************
//				
//		Copyright (c) 2001 IAG Software Team, 
//
//			Beijing R&D Center 
//	
//			Advantech Co., Ltd.
//
//		    Linux comedi device driver
//
//      This is An Unpublished Work Containing Confidential  And Proprietary
//         Information Which Is The Property Of Advantech  Automation Corp.
//
//    Any Disclosure, Use, Or Reproduction, Without Written Authorization From
//               Advantech Automation Corp., Is Strictly Prohibit.
//
//
//*****************************************************************************

/*
 * File:			pci1712.c
 *
 * Author: 		Michal Dobes <liluosheng@yahoo.com.cn>
 *
 * Support card:	Advantech card pci1712
 *
 * Status: 		works
 *
 *
 * Description:	This driver supports insn and cmd interfaces. 
 *		AI and AO subdevice supports both cmd and insn interface,
 *		other subdevices support only insn interface.
 *		Data transfer over DMA is supported.
 */

/*
 * Date:			06-Aug-2004
 *
 * Modifier:		Eddy Yang <yanggg97@mails.tsinghua.edu.cn> 
 *
 * Options:
 *  [0] - PCI bus number - if bus number and slot number are 0, 
 *                         then driver search for first unused card
 *  [1] - PCI slot number 
 *
 * Look at the script file "comedi-0.7.60/etc/pci1712.conf" for details.
 * 
*/

#include <linux/comedidev.h>

#include <linux/pci.h>

#include "8253.h"
//#include "9054.h"

#include "amcc_s5933.h"


/* #include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/ioport.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
//#include <asm/hw_irq.h>
#include <linux/timex.h>
#include <linux/timer.h>
#include <linux/pci.h>
#include <asm/io.h>
#include <asm/dma.h>
#include <linux/comedidev.h>
#include "8253.h"
#include "amcc_s5933.h" */


#undef PCI1712_EXTDEBUG			/* if this is defined then a lot of messages is printed */

#define ADVANTECH_VENDOR	0x13fe	/* Advantech PCI vendor ID */
#define TYPE_PCI1712		0
#define IORANGE_PCI1712		50
#define SUBDEVICES_PCI1712	4

#define PCI1712_CHAN_AD		0x00	/* R:   channel and A/D data */
#define PCI1712_AD_RANGE	0x02	/* W:   A/D gain/range register */
#define PCI1712_AD_MUX		0x04	/* W:   A/D multiplexer control */
#define PCI1712_AD_STATUS	0x06	/* R:   A/D status register */
#define PCI1712_AD_CONTROL	0x06	/* W:   A/D control register */
#define PCI1712_INTFIFO_STATUS	0x08	/* R:   interrupt and FIFO status */
#define PCI1712_CLEAR_INTFIFO	0x08	/* W:   clear interrupt and FIFO */
#define PCI1712_DA_STATUS	0x0A	/* R:   D/A status register */
#define PCI1712_DA_CONTROL	0x0A	/* W:   D/A control register */
#define PCI1712_DA0		0x0C	/* W:   D/A channel 0 data */
#define PCI1712_DA1		0x0E	/* W:   D/A channel 1 data */
#define PCI1712_DA_CNT		0x10	/* R/W: 0 chip 8254 D/A counter 0*/
#define PCI1712_AD_CNT		0x12	/* R/W: 0 chip 8254 A/D counter 1*/
#define PCI1712_DMA_CNT		0x14	/* R/W: 0 chip 8254 DMA counter 2*/
#define PCI1712_CHIP0_CONTROL	0x16	/* R/W: 0 chip 8254 counter control */
#define PCI1712_CNT0		0x18	/* R/W: 8254 couter 0 */
#define PCI1712_CNT1		0x1A	/* R/W: 8254 couter 1 */
#define PCI1712_CNT2		0x1C	/* R/W: 8254 couter 2 */
#define PCI1712_CHIP1_CONTROL	0x1E	/* R/W: 1 chip 8254 counter control */
#define PCI1712_CNT0_STATUS	0x20	/* R: counter 0 gate and clock status */
#define PCI1712_CNT0_CONTROL	0x20	/* W: counter 0 gate and clock control */
#define PCI1712_CNT1_STATUS	0x22	/* R: counter 1 gate and clock status */
#define PCI1712_CNT1_CONTROL	0x22	/* W: counter 1 gate and clock control */
#define PCI1712_CNT2_STATUS	0x24	/* R: counter 2 gate and clock status */
#define PCI1712_CNT2_CONTROL	0x24	/* W: counter 2 gate and clock control */
#define PCI1712_CNT_SELECT	0x26	/* W: counter internal clock source select register */
#define PCI1712_DI		0x28	/* R: digital input */
#define PCI1712_DO		0x28	/* W: digital output */
#define PCI1712_DIO		0x28	/* R/W: digital input & output*/
#define PCI1712_DIO_CONFIG	0x2A	/* R/W: digital I/O configuration register */
#define PCI1712_CALIB		0x2C	/* W:   calibration command and data */
#define PCI1712_DA_CONT		0x30	/* W:   D/A channel data for continuous output operation mode */

//for int&fifo control/status register
#define Clear_AD_FIFO		0x8000	/* clear the AD FIFO and interrupt */
#define Clear_DA_FIFO		0x0800	/* clear the DA FIFO and interrupt */
#define CLear_INT		0x8800	/* clear the interrupt */

#define Status_INT_F		0x0001	/* 1=interrupt occured */
#define Status_AD_FE		0x0100	/* 1=A/D FIFO is empty */
#define Status_AD_FH		0x0200	/* 1=A/D FIFO is half full */
#define Status_AD_FF		0x0400	/* 1=A/D FIFO is full */
#define Status_DA_FE		0x1000	/* 1=D/A FIFO is empty */
#define Status_DA_FH		0x2000	/* 1=D/A FIFO is half full */
#define Status_DA_FF		0x4000	/* 1=D/A FIFO is full */

//for AD control/status register
#define Mode_SINGLE		0x0000
#define Mode_PACER		0x0001
#define Mode_POST_TR		0x0002
#define Mode_DELAY_TR		0x0003
#define Mode_ABOUT_TR		0x0004

#define Status_AD_CLK		0x0008
#define Status_AD_TR		0x0010
#define Status_AD_TRE		0x0040
#define Status_DMA_TCF		0x4000
#define Status_AI_TRGF		0x8000

#define Control_AD_CLK		0x0008	/* 0=internal clock,1=have an extern clock(From AI_CLK)*/
#define Control_AD_TR		0x0010	/* 0=extern TTL-trriger(from pin AI_TRG),1=analog threshold triger */
#define Control_AD_TRE		0x0040	/* 0=rising edge,1=falling edge*/
#define Control_AIO_CAL		0x0080	/* 0=normal mode,1=calibrate mode*/
#define Control_DMA_TCF		0x4000 	/* 1=terminal,0=unterminal*/
#define Control_AI_TRGF		0x8000	/* 1=AD event occur*/

//for DA control/status register
#define Status_DA0_510		0x0001
#define Status_DA0_IE		0x0002
#define Status_DA0_UB		0x0004
#define Status_DA1_510		0x0010
#define Status_DA1_IE		0x0020
#define Status_DA1_UB		0x0040
#define Status_DAM0		0x0100
#define Status_DAM1		0x0200
#define Status_DA_CLK		0x0800
#define Status_AO_TRGF		0x8000

#define Control_DA0_510		0x0001
#define Control_DA0_IE		0x0002
#define Control_DA0_UB		0x0004
#define Control_DA1_510		0x0010
#define Control_DA1_IE		0x0020
#define Control_DA1_UB		0x0040
#define Control_DAM0		0x0100
#define Control_DAM1		0x0200
#define Control_DA_CLK		0x0800
#define Control_AO_TRGF		0x8000

//for DIO config register
#define Control_DIO_C0	0x0001
#define Control_DIO_C1	0x0010
#define Status_DIO_C0	0x0001
#define Status_DIO_C1	0x0010

//for cnt_insn bits function
#define PULSE_START		1
#define FREQ_IN			2
#define PWM_IN			3
#define CNT_RESET		0
//for internal clock select register
#define INTER_CLK_10M	0x00
#define INTER_CLK_1M	0x01
#define INTER_CLK_100K	0x02
#define INTER_CLK_10K	0x03


#define MAX_CHANLIST_LEN	256	/* length of scan list */
#define EVENT_AT    		40
#define MAX_INT_CASES	20


static comedi_lrange range_pci1712_ai={ 9, {
	BIP_RANGE(5),
	BIP_RANGE(2.5),
	BIP_RANGE(1.25),
	BIP_RANGE(0.625),
	BIP_RANGE(10),
	UNI_RANGE(10),
	UNI_RANGE(5),
	UNI_RANGE(2.5),
	UNI_RANGE(1.25)
	}
};
static char range_codes_pci1712_ai[]={ 0x00, 0x01, 0x02, 0x03, 0x04, 0x10, 0x11, 0x12, 0x13 };

static comedi_lrange range_pci1712_ao={ 4, {
	UNI_RANGE(5),
	UNI_RANGE(10),
	BIP_RANGE(5),
	BIP_RANGE(10)
	}
};
static char range_codes_pci1712_ao[]={ 0x00, 0x01, 0x04, 0x05 };


static int pci1712_attach(comedi_device *dev,comedi_devconfig *it);
static int pci1712_detach(comedi_device *dev);

typedef struct {
	const char	*name;		// board name
	int		board_type;	// type of this board
	int		vendor_id;	// PCI vendor a device ID of card
	int		device_id;
	char		have_irq;	// 1=card support IRQ
	int		iorange;	// I/O range len
	int 		n_aichan;	// num of A/D chans in S.E.
	int 		n_aichand;	// DIFF num of A/D chans
	int 		n_aochan;	// num of D/A chans
	int 		n_diochan;	// num of DIO chans 
	int		n_cntchan;	// num of counter chans
	int		n_tmrchan;	// num of timer chans
	int 		ai_maxdata;	// A/D resolution
	int		ao_maxdata;	// D/A resolution
	comedi_lrange	*rangelist_ai;	// rangelist for A/D
	char		*rangecode_ai;	// range codes for programming
	comedi_lrange	*rangelist_ao;	// rangelist for D/A
	char		*rangecode_ao;	// range codes for programming
	unsigned int	ai_ns_min;	// AI max sample speed (mks)
	unsigned int	ao_ns_min;	// AO max sample speed (mks)
	unsigned int	fifo_half_size;	// size of FIFO/2
} boardtype;

static boardtype boardtypes[] =
{
	{"pci1712", TYPE_PCI1712,ADVANTECH_VENDOR, 0x1712, 
	1, IORANGE_PCI1712, 16, 8, 2, 16, 3, 3, 0x0fff, 0xfff, 
	&range_pci1712_ai, range_codes_pci1712_ai, &range_pci1712_ao, 
	range_codes_pci1712_ao, 1, 1, 512},
};

#define n_boardtypes (sizeof(boardtypes)/sizeof(boardtype))
#define this_board ((boardtype *)dev->board_ptr)

comedi_driver driver_pci1712={
	driver_name:	"pci1712",
	module:		THIS_MODULE,
	attach:		pci1712_attach,
	detach:		pci1712_detach,
	board_name:	&boardtypes[0].name,
	num_names:	n_boardtypes,
	offset:		sizeof(boardtype),
};

typedef struct {
	unsigned char	valid;			// =1 device is OK
	unsigned char	ai_use_ext_trg;		// 1=board uses external trigger
	unsigned char	ai_running;			// =1 ao is running
	unsigned char	ai_neverending;		// =1 we do unlimited AI
	unsigned char	ai_terminated;		// =1 ai dma transfer terminated
	unsigned char	ai_about_term;		// =1 ai about mode terminated
	unsigned char	ai_eos;			// 1=EOS wake up
	unsigned int	ai_scans;		// len of scanlist
	unsigned int	ai_act_scan;		// how many scans we finished
	unsigned int	*ai_chanlist;		// our copy of channel/range list for AI
	unsigned int	ai_n_chan;		// how many AI channels is measured	
	unsigned char	act_chanlist_len;	//actural chanlist length after checkin
	unsigned char	act_chanlist_pos;	//actural chanlist position after checkin
	unsigned int	ai_flags;		// flaglist
	unsigned int	ai_data_len;		// len of data buffer
	sampl_t		*ai_data;		// data buffer
	unsigned char	ao_use_ext_trg;		// 1=board uses external trigger
	unsigned int	*ao_chanlist;	// our copy of channel/range list for AI
	unsigned int	ao_n_chan;		// how many AO channels is measured	
	unsigned char	ao_running;		// =1 ao channel 0 is running cmd, =2 ao channel 1 is running cmd, =3 both channel is running cmd
	unsigned char	ao_neverending;		// =1 we do unlimited AO
	unsigned int	ao_data_len;	// len of data buffer
	sampl_t		*ao_data;		// data buffer
	lsampl_t	ao_data_insn[2];		// data output buffer for insn
	unsigned char	ad_mod;	// AD mode
	unsigned char	da_mod;	// DA mode
	unsigned int	ad_delay_cnt;		//delay ad aquisition mode delay number
	struct pci_dev	*pci_dev;		//the struct data of pci
	unsigned char	irq;			// irq number
	unsigned short	cnt_init_data[3];	//counter data
	unsigned short	cnt_mode;		// Current mode of counter
	unsigned int clocktype;			// Clock type (3 = 10kHz)
//	void		*lcfg;			// PCI related register base address
//	void		*las1;			// PCI related register base address
//	void		*las0;			// PCI related register base address
} pci1712_private;

#define devpriv ((pci1712_private *)dev->private)

static unsigned int muxonechan[] ={ 0x0000, 0x0101, 0x0202, 0x0303, 0x0404, 0x0505, 0x0606, 0x0707, // used for gain list programming
                                    0x0808, 0x0909, 0x0a0a, 0x0b0b, 0x0c0c, 0x0d0d, 0x0e0e, 0x0f0f,
                                    0x1010, 0x1111, 0x1212, 0x1313, 0x1414, 0x1515, 0x1616, 0x1717,
                                    0x1818, 0x1919, 0x1a1a, 0x1b1b, 0x1c1c, 0x1d1d, 0x1e1e, 0x1f1f};

/* 
==============================================================================
*/

//============================================================================
// check_and_setup_channel_list ()
// Function:	Check if channel list from user is builded correctly.
// Argument: 
//		dev 		the operated comedi device
//		s		the operated subdevice
//		chanlist	channel list
//		n_chan		the number of channel
// Return value: 
//		type:	int
// Global variable:
//
//=============================================================================
int check_and_setup_channel_list(comedi_device * dev, comedi_subdevice * s, unsigned int *chanlist, unsigned int n_chan, char check) 
{
        unsigned int chansegment[32];  
        unsigned int i, nowmustbechan, seglen, segpos,range,chanprog;
    
#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG:  check_and_setup_channel_list(...,%d)\n",n_chan);
#endif
    /* correct channel and range number check itself comedi/range.c */
	if (n_chan<1) {
		if (!check) comedi_error(dev,"range/channel list is empty!");
		return 0;
        }

        if (n_chan > 1) {
		chansegment[0]=chanlist[0]; // first channel is everytime ok except odd&diff
		if (CR_CHAN(chanlist[0]) & 1) // odd channel cann't by differencial
			if (CR_AREF(chanlist[0])==AREF_DIFF) {
				if (!check) rt_printk("comedi%d: adv_pci1712: Odd channel cann't be differencial input!\n", dev->minor);
					return 0;
			}
		for (i=1, seglen=1; i<n_chan; i++, seglen++) { // build part of chanlist
			if (chanlist[0]==chanlist[i]) break; // we detect loop, this must by finish
			if (CR_CHAN(chanlist[i]) & 1) // odd channel cann't by differencial
				if (CR_AREF(chanlist[i])==AREF_DIFF) {
					if (!check) rt_printk("comedi%d: adv_pci1712: Odd channel cann't be differencial input!\n", dev->minor);
					return 0;             
				}
			nowmustbechan=(CR_CHAN(chansegment[i-1])+1) % s->n_chan;
			if (CR_AREF(chansegment[i-1])==AREF_DIFF) 
				nowmustbechan=(nowmustbechan+1) % s->n_chan;
			if (nowmustbechan!=CR_CHAN(chanlist[i])) { // channel list isn't continous :-(
				if (!check) rt_printk("comedi%d: adv_pci1712: channel list must be continous! chanlist[%i]=%d but must be %d or %d!\n",
					    dev->minor,i,CR_CHAN(chanlist[i]),nowmustbechan,CR_CHAN(chanlist[0]) );
				return 0;             
			}
			chansegment[i]=chanlist[i]; // well, this is next correct channel in list
		}

		for (i=0, segpos=0; i<n_chan; i++) {  // check whole chanlist
			if (chanlist[i]!=chansegment[i%seglen]) {
				if (!check) rt_printk("comedi%d: adv_pci1712: bad channel, reference or range number! chanlist[%i]=%d,%d,%d and not %d,%d,%d!\n",
					    dev->minor,i,CR_CHAN(chansegment[i]),CR_RANGE(chansegment[i]),CR_AREF(chansegment[i]),CR_CHAN(chanlist[i%seglen]),CR_RANGE(chanlist[i%seglen]),CR_AREF(chansegment[i%seglen]));
				return 0; // chan/gain list is strange
			}
		}
	} else {
		if (CR_CHAN(chanlist[0]) & 1) // odd channel cann't by differencial
			if (CR_AREF(chanlist[0])==AREF_DIFF) {
				if (!check) rt_printk("comedi%d: adv_pci1712: Odd channel cann't be differencial input!\n", dev->minor);
					return 0;             
			}
		seglen=1;
	}
	
	if (check) return 1;
	
	devpriv->act_chanlist_len=seglen;
	devpriv->act_chanlist_pos=0;

#ifdef PCI1712_EXTDEBUG
	rt_printk("SegLen: %d\n", seglen);
#endif
 	for (i=0; i<seglen; i++) {  // store range list to card
		chanprog=muxonechan[CR_CHAN(chanlist[i])];
		outw(chanprog, dev->iobase+PCI1712_AD_MUX); /* select channel */
		range=this_board->rangecode_ai[CR_RANGE(chanlist[i])];
		if (CR_AREF(chanlist[i])==AREF_DIFF) range|=0x0020;
		outw(range, dev->iobase+PCI1712_AD_RANGE); /* select gain */
#ifdef PCI1712_PARANOIDCHECK
		devpriv->act_chanlist[i]=(CR_CHAN(chanlist[i])<<12) & 0xf000;
#endif
#ifdef PCI1712_EXTDEBUG
		rt_printk("GS: %2d. [%4x]=%4x %4x\n", i, chanprog, range, devpriv->act_chanlist[i]);
#endif
	}

	udelay(1);

	outw(CR_CHAN(chanlist[0]) | (CR_CHAN(chanlist[seglen-1]) << 8) , dev->iobase+PCI1712_AD_MUX); /* select channel interval to scan */
#ifdef PCI1712_EXTDEBUG
	rt_printk("MUX: %4x L%4x.H%4x\n", CR_CHAN(chanlist[0]) | (CR_CHAN(chanlist[seglen-1]) << 8), CR_CHAN(chanlist[0]), CR_CHAN(chanlist[seglen-1]));
#endif
	return 1; // we can serve this with MUX logic
}

//============================================================================
// pci1712_ai_insn_read ()
// Function:	this function provide a method to read from an 
//		AI subdevice in insn mode
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
//		insn:	the insn structure which to be performed
//		data:	the data read from the subdevice
// Return value: 
//		type:		static int
//		description:	the number of data read from the subdevice
// Global variable:
//
//=============================================================================
static int pci1712_ai_insn_read(comedi_device *dev,comedi_subdevice *s,	comedi_insn *insn,lsampl_t *data)
{
        int timeout,i;
#ifdef PCI1712_PARANOIDCHECK
	unsigned int temp;
#endif

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: BGN: pci1712_ai_insn_read(...)\n");
#endif

	if (devpriv->ai_running) // ai is running cmd
		return -EINVAL;
	outw(0, dev->iobase+PCI1712_AD_CONTROL);	
	outw(0,dev->iobase + PCI1712_CLEAR_INTFIFO);
	if (!check_and_setup_channel_list(dev,s,&insn->chanspec, 1, 0))
		return -EINVAL;

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 A ST=%4x IO=%x\n",inw(dev->iobase+PCI1712_AD_STATUS), dev->iobase+PCI1712_AD_STATUS);
#endif

	for (i=0; i<(insn->n); i++) {
		outw(0, dev->iobase+PCI1712_CHAN_AD); /* start conversion */
#ifdef PCI1712_EXTDEBUG
		rt_printk("adv_pci1712 B i=%d ST=%4x\n",i,inw(dev->iobase+PCI1712_AD_STATUS));
#endif
		udelay(1);
#ifdef PCI1712_EXTDEBUG
		rt_printk("adv_pci1712 C i=%d ST=%4x\n",i,inw(dev->iobase+PCI1712_AD_STATUS));
#endif
    		timeout=100;
    		while (timeout--) {
			if (!(inw(dev->iobase+PCI1712_INTFIFO_STATUS) & Status_AD_FE))
				goto conv_finish;
#ifdef PCI1712_EXTDEBUG
			if (!(timeout%10))
				rt_printk("adv_pci1712 D i=%d tm=%d ST=%4x\n",i,timeout,inw(dev->iobase+PCI1712_INTFIFO_STATUS));
#endif
			udelay(1);
    		}
    		comedi_error(dev,"A/D mode0 timeout");
    		data[i]=0;
		outw(0,dev->iobase + PCI1712_CLEAR_INTFIFO);
#ifdef PCI1712_EXTDEBUG
		rt_printk("adv_pci1712 i=%d ST=%4x DT=%4x to=%d\n",i,inw(dev->iobase+PCI1712_INTFIFO_STATUS),inw(dev->iobase+PCI1712_CHAN_AD),timeout);
		rt_printk("adv_pci1712 EDBG: END pci1712_ai_insn_read(...)\n");
#endif
    		return -ETIME;
 
conv_finish:
#ifdef PCI1712_PARANOIDCHECK
		temp=inw(dev->iobase+PCI1712_CHAN_AD);
		if ((temp & 0xf000)!=devpriv->act_chanlist[0]) {
			comedi_error(dev,"A/D mode0 data droput!");
			return -ETIME;
		}
		data[i] = temp & 0x0fff; 
#else
		data[i] = inw(dev->iobase+PCI1712_CHAN_AD) & 0x0fff; 
#endif
	}

	outw(0,dev->iobase + PCI1712_CLEAR_INTFIFO);

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: END pci1712_ai_insn_read(...) i=%d\n",i);
#endif

        return i;
}

//============================================================================
// pci1712_ao_insn_write ()
// Function:	this function provide a method to write to an 
//		AO subdevice in insn mode
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
//		insn:	the insn structure which to be performed
//		data:	the data write to the subdevice
// Return value: 
//		type:		static int
//		description:	the number of data write to subdevice
// Global variable:
//
//=============================================================================
static int pci1712_ao_insn_write(comedi_device *dev,comedi_subdevice *s,comedi_insn *insn,lsampl_t *data)
{
	int n,chan,range,ofs;
	unsigned int control;

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: BGN: pci1712_ao_insn_write(...)\n");
#endif

	chan=CR_CHAN(insn->chanspec);
	if (chan>=s->n_chan)
		return -EINVAL;

	if (0x3==devpriv->ao_running)	//all channel is running cmd
		return -EINVAL;

	if ((0x2==devpriv->ao_running)&&(1==chan))	//channel 1 is running cmd
		return -EINVAL;

	if ((0x1==devpriv->ao_running)&&(0==chan))	//channel 0 is running cmd
		return -EINVAL;

	range=CR_RANGE(insn->chanspec);
	if (range>=s->range_table->length)
		return -EINVAL;

	control = inw(dev->iobase + PCI1712_DA_STATUS);
	if (chan) { 
		ofs=PCI1712_DA1;
		control=control & 0xffaf;
		control=control | (this_board->rangecode_ao[range]<<4);
	} else {
		ofs=PCI1712_DA0;
		control=control & 0xfffa;
		control=control | this_board->rangecode_ao[range];		
	}

	outw(control, dev->iobase + PCI1712_DA_CONTROL);
	for (n=0; n<insn->n; n++) 
		outw(data[n], dev->iobase + ofs);

	devpriv->ao_data_insn[chan]=data[n];

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: END pci1712_ao_insn_write(...) n=%d\n",n);
#endif

	return n;
}

//============================================================================
// pci1712_ao_insn_read ()
// Function:	this function provide a method to read from an 
//		AO subdevice in insn mode
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
//		insn:	the insn structure which to be performed
//		data:	the data read from the subdevice
// Return value: 
//		type:		static int
//		description:	the number of data read from the subdevice
// Global variable:
//
//=============================================================================
static int pci1712_ao_insn_read(comedi_device *dev,comedi_subdevice *s,	comedi_insn *insn,lsampl_t *data)
{
	int chan,i;

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: BGN: pci1712_ao_insn_write(...)\n");
#endif

	chan = CR_CHAN(insn->chanspec);
	if (chan>=s->n_chan)
		return -EINVAL;
	
	for(i=0;i<insn->n;i++){
		data[i] = devpriv->ao_data_insn[chan];
	}

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: END pci1712_ao_insn_write(...) i=%d\n",i);
#endif

	return i;
}

//============================================================================
// pci1712_dio_insn_config ()
// Function:	this function provide a method to config an 
//		dio subdevice in insn mode.
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
//		insn:	the insn structure which to be performed
//		data:	the data read from the subdevice
// Return value: 
//		type:		static int
//		description:	the number of data to config the subdevice
// Global variable:
//
//=============================================================================
static int pci1712_dio_insn_config(comedi_device *dev,comedi_subdevice *s, comedi_insn *insn,lsampl_t *data)
{
	int iobits,chan;
	
#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: BGN: pci1712_dio_insn_config(...)\n");
#endif

	if(insn->n!=1) return -EINVAL;
	chan=CR_CHAN(insn->chanspec);
	if(chan>15) return -EINVAL;
	
	iobits = inw(dev->iobase+PCI1712_DIO_CONFIG);
	switch(data[0]){
	case COMEDI_OUTPUT:
		if(chan>7) {
			iobits &= ~Control_DIO_C1;
			s->io_bits |= 0xff00;
		} else {
			iobits &= ~Control_DIO_C0;
			s->io_bits |= 0x00ff;
		}
		break;
	case COMEDI_INPUT:
		if(chan>7) {
			iobits |= Control_DIO_C1;
			s->io_bits &= 0x00ff;
		} else {
			iobits |= Control_DIO_C0;
			s->io_bits &= 0xff00;
		}
		break;
	case 2:
		data[0] = (lsampl_t) s->io_bits;
		break;
	default:
		return -EINVAL;
	}
	outw(iobits,dev->iobase+PCI1712_DIO_CONFIG);

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: END pci1712_dio_insn_config(...)\n");
#endif

	return 1;
}

//============================================================================
// pci1712_dio_insn_bits ()
// Function:	this function provide a method to read/write from/to a 
//		dio subdevice in insn mode.
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
//		insn:	the insn structure which to be performed
//		data:	the data read from the subdevice
// Return value: 
//		type:		static int
//		description:	the number of data read/write from/to the subdevice
// Global variable:
//
//=============================================================================
static int pci1712_dio_insn_bits(comedi_device *dev,comedi_subdevice *s, comedi_insn *insn,lsampl_t *data)
{
#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: BGN: pci1712_dio_insn_bits(...)\n");
#endif

	if(insn->n!=2) return -EINVAL;
	
	if(data[0]){
		s->state &= ~data[0];
		s->state |= (data[0]&data[1]);
		outw(s->state,dev->iobase+PCI1712_DIO);
	}
	data[1] = inw(dev->iobase+PCI1712_DIO);

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: END pci1712_dio_insn_bits(...)\n");
#endif

	return 2;
}

/* 
==============================================================================
special function for counter subdevice:
	counter_reset
	freq_in
==============================================================================
*/
//============================================================================
// counter_reset ()
// Function:	this function provide a method to reset a counter
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
//		insn:	the insn structure which to be performed
//		data:	the data read from the subdevice
// Return value: 
//		type:		static int
//		description:	success or fail
// Global variable:
//
// data description:
// 	data[0]:	function type	= CNT_RESET
//=============================================================================
static int counter_reset(comedi_device *dev,comedi_subdevice *s, comedi_insn *insn,lsampl_t *data)
{	
	int chan;
	unsigned int ofs,ofs1;
	ofs=ofs1=0;

	chan=CR_CHAN(insn->chanspec);
	if (chan>=s->n_chan) return 0;

	switch(chan) {
	case 0:
		ofs=PCI1712_CNT0;
		ofs1=PCI1712_CNT0_CONTROL;
		outw(0x39, dev->iobase + PCI1712_CHIP1_CONTROL);
		break;
	case 1:
		ofs=PCI1712_CNT1;
		ofs1=PCI1712_CNT1_CONTROL;
		outw(0x79, dev->iobase + PCI1712_CHIP1_CONTROL);
		break;
	case 2:
		ofs=PCI1712_CNT2;
		ofs1=PCI1712_CNT2_CONTROL;
		outw(0xb9, dev->iobase + PCI1712_CHIP1_CONTROL);
		break;
	}
	outw(0, dev->iobase + ofs);	 // write the low byte
	outw(0, dev->iobase +ofs);		// write the high byte

	devpriv->cnt_mode = 0;

	return 1;
}

//============================================================================
// freq_in ()
// Function:	this function provide a method to do frequency measurement
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
//		insn:	the insn structure which to be performed
//		data:	the data read from the subdevice
// Return value: 
//		type:		static int
//		description:	success or fail
// Global variable:
//
// data description:
// 	data[0]:	function type	= FREQ_IN
//	data[1]:	clock type (0~3)	0--10M, 1--1M, 2--100K, 3--10K
//  return:
//	data[0]:	period (ns)
//=============================================================================
static int freq_in(comedi_device *dev,comedi_subdevice *s, comedi_insn *insn,lsampl_t *data)
{
	unsigned short val;
	long cnt;

	devpriv->clocktype = data[1];
	if(devpriv->clocktype>3) return 0;

	// ������ ������� ��� �������
	outw(devpriv->clocktype, dev->iobase + PCI1712_CNT_SELECT);

	// ���������� �������
	outw(0x00, dev->iobase + PCI1712_CNT2_CONTROL);
	outw(0x00, dev->iobase + PCI1712_CNT1_CONTROL);
	
	// ������ ������ � ��������� ��������
	outw(0x72, dev->iobase + PCI1712_CHIP1_CONTROL);
	// ������������� ���������� ������ �� ��������� �������
	// //divisor 10000 = 0x2710
	outw(0x10, dev->iobase + PCI1712_CNT1);		// ������� ����
	outw(0x27, dev->iobase + PCI1712_CNT1);		// ������� ����

	// ������ ������ � mode ����� �����
	outw(0xb0, dev->iobase + PCI1712_CHIP1_CONTROL);
	// ���������� ������ ������
	outw(0xff, dev->iobase + PCI1712_CNT2);
	outw(0xff, dev->iobase + PCI1712_CNT2);

	// ���������� ����� � ������ ������
	// ��������� ���� �������
	outw(0x80, dev->iobase + PCI1712_CNT2_CONTROL);
	// ����� �����
	outw(0x88, dev->iobase + PCI1712_CNT2_CONTROL);
	// ����� ����, ����� ��������, �� ���������� ������ ������ ��� ����
	outw(0x80, dev->iobase + PCI1712_CNT2_CONTROL);
	// ��������� ���� ����
	outw(0x00, dev->iobase + PCI1712_CNT2_CONTROL);

	// ���������� ����� � ������ ������ � ��������� ���
	outw(0x80, dev->iobase + PCI1712_CNT1_CONTROL);
	outw(0x88, dev->iobase + PCI1712_CNT1_CONTROL);
	outw(0x80, dev->iobase + PCI1712_CNT1_CONTROL);
	outw(0x81, dev->iobase + PCI1712_CNT1_CONTROL);

	// ��������� ������ ������
	outw(0x52, dev->iobase + PCI1712_CNT2_CONTROL);

	// ���������, ���������� �� ������� ���������
	val = 0x00;
	cnt = 10000000;
	while((val!=0x0100) && (cnt>0)) {
		val = inw(dev->iobase + PCI1712_CNT2_STATUS) & 0x0100;
		cnt--;
	}

	if (cnt==0)
	{
		return 0;
	}
	devpriv->cnt_mode = FREQ_IN;

	return 1;
}

//============================================================================
// pci1712_cnt_insn_bits ()
// Function:	this function provide a method to read/write from/to a 
//		cnt subdevice in insn mode.
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
//		insn:	the insn structure which to be performed
//		data:	the data read from the subdevice
// Return value: 
//		type:		static int
//		description:	the number of data read/write from/to the subdevice
// Global variable:
//
//=============================================================================
static int pci1712_cnt_insn_bits(comedi_device *dev,comedi_subdevice *s, comedi_insn *insn,lsampl_t *data)
{
#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: BGN: pci1712_cnt_insn_bits(...)\n");
#endif
	switch (data[0]) {
	case FREQ_IN:
		if (!freq_in(dev, s, insn, data))
			return -EINVAL;
		break;
	case CNT_RESET:
		if(!counter_reset(dev, s, insn, data))
			return -EINVAL;
		break;
	default:
		printk("The driver do not support this function.\n");
		break;
	}

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: END pci1712_cnt_insn_bits(...)\n");
#endif

	return 1;
}

//============================================================================
// pci1712_cnt_insn_write ()
// Function:	this function provide a method to write to an 
//		count subdevice in insn mode
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
//		insn:	the insn structure which to be performed
//		data:	the data read from the subdevice
// Return value: 
//		type:		static int
//		description:	the number of data write to the subdevice
// Global variable:
//
//=============================================================================
static int pci1712_cnt_insn_write(comedi_device *dev,comedi_subdevice *s, comedi_insn *insn,lsampl_t *data)
{
#ifdef PCI1712_EXTDEBUG
        rt_printk("adv_pci1712 EDBG: BGN: pci1712_cnt_insn_write(...)\n");
#endif

	int chan;
	unsigned int ofs,ofs1;
	ofs=ofs1=0;

	chan=CR_CHAN(insn->chanspec);
	if (chan>=s->n_chan)
		return -EINVAL;
	if (data[0]>s->maxdata)
		devpriv->cnt_init_data[chan]=s->maxdata;
	else
		devpriv->cnt_init_data[chan]=data[0];

	switch(chan) {
	case 0:
		ofs=PCI1712_CNT0;
		ofs1=PCI1712_CNT0_CONTROL;
		outw(0x30, dev->iobase + PCI1712_CHIP1_CONTROL);
		break;
	case 1:
		ofs=PCI1712_CNT1;
		ofs1=PCI1712_CNT1_CONTROL;
		outw(0x70, dev->iobase + PCI1712_CHIP1_CONTROL);
		break;
	case 2:
		ofs=PCI1712_CNT2;
		ofs1=PCI1712_CNT2_CONTROL;
		outw(0xb0, dev->iobase + PCI1712_CHIP1_CONTROL);
		break;
	}

	outw(0x81,dev->iobase +ofs1);
	outw(data[0] & 0xff, dev->iobase + ofs);          // write the first low byte
	outw((data[0] >> 8) & 0xff, dev->iobase +ofs);   // write the high byte
	
#ifdef PCI1712_EXTDEBUG
        rt_printk("adv_pci1712 EDBG: END: pci1712_cnt_insn_write(...)\n");
#endif

	return 1;
}

//============================================================================
// pci1712_cnt_insn_read ()
// Function:	this function provide a method to read from an 
//		count subdevice in insn mode
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
//		insn:	the insn structure which to be performed
//		data:	the data read from the subdevice
// Return value: 
//		type:		static int
//		description:	the number of data read from the subdevice
// Global variable:
//
//=============================================================================
static int pci1712_cnt_insn_read(comedi_device *dev,comedi_subdevice *s, comedi_insn *insn,lsampl_t *data)
{
#ifdef PCI1712_EXTDEBUG
        rt_printk("adv_pci1712 EDBG: BGN: pci1712_cnt_insn_read(...)\n");
#endif

	short low_byte, high_byte;
	int chan;
	unsigned int ofs,ofs1;
	unsigned short val, curcnt;
	unsigned int freq=0;

	ofs=ofs1=0;

	chan=CR_CHAN(insn->chanspec);
	if (chan>=s->n_chan)
		return -EINVAL;

	if (devpriv->cnt_mode==0)
	{
		switch(chan) {
		case 0:
			ofs=PCI1712_CNT0;
			ofs1=PCI1712_CNT0_CONTROL;
			outw(0x04, dev->iobase + PCI1712_CHIP0_CONTROL);
			break;
		case 1:
			ofs=PCI1712_CNT1;
			ofs1=PCI1712_CNT1_CONTROL;
			outw(0x44, dev->iobase + PCI1712_CHIP0_CONTROL);
			break;
		case 2:
			ofs=PCI1712_CNT2;
			ofs1=PCI1712_CNT2_CONTROL;
			outw(0x84, dev->iobase + PCI1712_CHIP0_CONTROL);
			break;
		}
	
		outw(0x81, dev->iobase + ofs1);
		low_byte = inw(dev->iobase + ofs);
		high_byte = inw(dev->iobase + ofs);
	
		data[0] = ((high_byte << 8) & 0xff00) | (low_byte & 0x00ff);
		data[0] = devpriv->cnt_init_data[chan]-data[0];
		data[1] = 0;
	}
	else if (devpriv->cnt_mode==FREQ_IN)
	{
		// ���� ��������� �� ���������, ������ 0
		val = 0x00;
		val = inw(dev->iobase + PCI1712_CNT2_STATUS) & 0x0100;
		if(val==0x0100)
		{
			data[0] = 0;
			data[1] = 1;
		}
		else
		{
			// ������ ��������
			curcnt = inw(dev->iobase + PCI1712_CNT2) & 0x00ff;
			curcnt += ((inw(dev->iobase + PCI1712_CNT2)<<8)&0xff00);
			
			// ���������� �������
			outw(0x00, dev->iobase + PCI1712_CNT2_CONTROL);
			outw(0x00, dev->iobase + PCI1712_CNT1_CONTROL);

			// ��������� �������
			switch(devpriv->clocktype) {
			case INTER_CLK_10K:
				freq = 0xffff - curcnt;// 10K/10000 
				break;
			case INTER_CLK_100K:
				freq = (0xffff - curcnt)*10;// 100k/10000
				break;
			case INTER_CLK_1M:
				freq = (0xffff - curcnt)*100;// 1M/10000
				break;
			case INTER_CLK_10M:
				freq = (0xffff - curcnt)*1000;// 10M/10000
				break;
			}

			// ���������� ������� � ��
			data[1] = 2;
			data[0] = freq;
			devpriv->cnt_mode=0;
		}
	}
	

#ifdef PCI1712_EXTDEBUG
        rt_printk("adv_pci1712 EDBG: END: pci1712_cnt_insn_read(...)\n");
#endif

	return 1; 
}



#ifdef PCI1712_EXTDEBUG
//============================================================================
// pci1712_cmdtest_out ()
// Function:	test whether the command is right.
// Argument: 
//		e	error number
//		cmd	command to be tested
// Return value: 
//		type:		static void
// Global variable:
//
//=============================================================================
void pci1712_cmdtest_out(int e,comedi_cmd *cmd) {
	rt_printk("pci1712 e=%d startsrc=%x scansrc=%x convsrc=%x\n",e,cmd->start_src,cmd->scan_begin_src,cmd->convert_src);
	rt_printk("pci1712 e=%d startarg=%d scanarg=%d convarg=%d\n",e,cmd->start_arg,cmd->scan_begin_arg,cmd->convert_arg);
	rt_printk("pci1712 e=%d stopsrc=%x scanend=%x\n",e,cmd->stop_src,cmd->scan_end_src);
	rt_printk("pci1712 e=%d stoparg=%d scanendarg=%d chanlistlen=%d\n",e,cmd->stop_arg,cmd->scan_end_arg,cmd->chanlist_len);
}
#endif

//============================================================================
// pci1712_ai_cmdtest ()
// Function:	test whether an ai command is right.
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
//		cmd	command to be tested
// Return value: 
//		type:		static int
// Global variable:
//
//=============================================================================
static int pci1712_ai_cmdtest(comedi_device *dev,comedi_subdevice *s,comedi_cmd *cmd)
{
	int err=0;
	int tmp;


#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: BGN: pci1712_ai_cmdtest(...)\n");
	pci1712_cmdtest_out(-1, cmd);
#endif

	/* step 1: make sure trigger sources are trivially valid */

	tmp=cmd->start_src;
	cmd->start_src &= TRIG_NOW|TRIG_EXT |TRIG_COUNT|TRIG_OTHER;
	if(!cmd->start_src || tmp!=cmd->start_src)err++;

	tmp=cmd->scan_begin_src;
	cmd->scan_begin_src &= TRIG_FOLLOW;
	if(!cmd->scan_begin_src || tmp!=cmd->scan_begin_src)err++;

	tmp=cmd->convert_src;
	if (devpriv->ai_use_ext_trg) { cmd->convert_src &= TRIG_EXT; }
	  else { cmd->convert_src &= TRIG_TIMER; }
	if(!cmd->convert_src || tmp!=cmd->convert_src)err++;

	tmp=cmd->scan_end_src;
	cmd->scan_end_src &= TRIG_COUNT;
	if(!cmd->scan_end_src || tmp!=cmd->scan_end_src)err++;

	if(err) {
#ifdef PCI1712_EXTDEBUG
		pci1712_cmdtest_out(1, cmd);
		rt_printk("adv_pci1712 EDBG: END: pci1712_ai_cmdtest(...) err=%d ret=1\n",err);
#endif
		return 1;
	}

	/* step 2: make sure trigger sources are unique and mutually compatible */

	if(cmd->start_src!=TRIG_NOW && cmd->start_src!= TRIG_EXT && 
	  cmd->start_src!=TRIG_COUNT && cmd->start_src!=TRIG_OTHER) {
		cmd->start_src=TRIG_NOW;
		err++;
	}

	if(cmd->scan_begin_src!=TRIG_FOLLOW) {
		cmd->scan_begin_src=TRIG_FOLLOW;
		err++;
	}

	if (devpriv->ai_use_ext_trg) {
		if(cmd->convert_src!=TRIG_EXT) {
			cmd->convert_src=TRIG_EXT;
			err++;
		}
	} else {
		if(cmd->convert_src!=TRIG_TIMER) {
			cmd->convert_src=TRIG_TIMER;
			err++;
		}
	}

	if(cmd->scan_end_src!=TRIG_COUNT) {
		cmd->scan_end_src=TRIG_COUNT;
		err++;
	}


	if(err) {
#ifdef PCI1712_EXTDEBUG
		pci1712_cmdtest_out(2, cmd);
		rt_printk("adv_pci1712 EDBG: END: pci1712_ai_cmdtest(...) err=%d ret=2\n",err);
#endif
		return 2;
	}

	/* step 3: make sure arguments are trivially compatible */

	if(cmd->start_src==TRIG_COUNT || cmd->start_src==TRIG_OTHER) {
		if(cmd->start_arg<0x2||cmd->start_arg>0xFFFF){
			cmd->start_arg=0x2;
			err++;
		}
	}


	if(cmd->convert_src==TRIG_TIMER){
		if(cmd->convert_arg<this_board->ai_ns_min){
			cmd->convert_arg=this_board->ai_ns_min;
			err++;
		}
	} else { /* TRIG_EXT */
		if(cmd->convert_arg!=0){
			cmd->convert_arg=0;
			err++;
		}
	}

	if(!cmd->chanlist_len){
		cmd->chanlist_len=1;
		err++;
	}
	if(cmd->chanlist_len>MAX_CHANLIST_LEN){
		cmd->chanlist_len=this_board->n_aichan;
		err++;
	}
	if(cmd->scan_end_arg!=cmd->chanlist_len){
		cmd->scan_end_arg=cmd->chanlist_len;
		err++;
	}

	if(err) {
#ifdef PCI1712_EXTDEBUG
		pci1712_cmdtest_out(3, cmd);
		rt_printk("adv_pci1712 EDBG: END: pci1712_ai_cmdtest(...) err=%d ret=3\n",err);
#endif
		return 3;
	}

	/* step 4: fix up any arguments */

	if(cmd->convert_src==TRIG_TIMER){
		tmp=cmd->convert_arg;
		if(cmd->convert_arg<this_board->ai_ns_min)
			cmd->convert_arg=this_board->ai_ns_min;
		if(tmp!=cmd->convert_arg)err++;
	}

	if(err) {
#ifdef PCI1712_EXTDEBUG
		rt_printk("adv_pci1712 EDBG: END: pci1712_ai_cmdtest(...) err=%d ret=4\n",err);
#endif
		return 4;
	}

	return 0;
}

//============================================================================
// pci1712_ai_cancel ()
// Function:	cancel the running ai.
// Argument: 
//		dev 		the operated comedi device
//		s		the operated subdevice
// Return value: 
//		type:	static int
// Global variable:
//
//=============================================================================
static int pci1712_ai_cancel(comedi_device * dev, comedi_subdevice * s)
{
#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: BGN: pci1712_ai_cancel(...)\n");
#endif

	outw(0, dev->iobase+PCI1712_AD_CONTROL);	// reset any operations
	outw(Clear_AD_FIFO,dev->iobase + PCI1712_CLEAR_INTFIFO);

	devpriv->ai_act_scan=0;
	s->async->cur_chan=0;
	devpriv->ai_neverending=0;
	devpriv->ai_running=0;
	devpriv->ai_eos=0;

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: END: pci1712_ai_cancel(...)\n");
#endif
	return 0;
}

//============================================================================
// pci1712_ai_cmd ()
// Function:	do an ai command.
// Argument: 
//		dev 	the operated comedi device
//		s	the operated subdevice
// Return value: 
//		type:	static int
// Global variable:
//
//=============================================================================
static int pci1712_ai_cmd(comedi_device *dev,comedi_subdevice *s)
{
	unsigned int	tmp;
	comedi_cmd	*cmd = &s->async->cmd;
	int ttt, all_scan, chan_now;

#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: BGN: pci1712_ai_cmd(...)\n");
#endif	

	devpriv->ai_n_chan=cmd->chanlist_len;
	devpriv->ai_chanlist=cmd->chanlist;
	devpriv->ai_flags=cmd->flags;
	devpriv->ai_data_len=cmd->data_len;	
	devpriv->ai_data=cmd->data;

        devpriv->ai_act_scan=0;
        s->async->cur_chan=0;
        devpriv->ai_neverending=0;
        devpriv->ai_running=0;
        devpriv->ai_eos=0;
	
	outw(0x74, dev->iobase + PCI1712_CHIP0_CONTROL); // stop timer
	outw(0, dev->iobase+PCI1712_AD_CONTROL);	// reset any operations
	outw(Clear_AD_FIFO,dev->iobase + PCI1712_CLEAR_INTFIFO);

	//rt_printk("Enter ai data acquisition mode,data_len is:=%d\n",devpriv->ai_data_len);
	if (!check_and_setup_channel_list(dev, s, devpriv->ai_chanlist, devpriv->ai_n_chan, 0)) return -EINVAL;
	if(cmd->start_src!=TRIG_NOW && cmd->start_src!= TRIG_EXT && 
		cmd->start_src!=TRIG_COUNT && cmd->start_src!=TRIG_OTHER)
		return -EINVAL;
	else {
		switch (cmd->start_src) {
		case TRIG_NOW:
			devpriv->ad_mod=Mode_PACER;
			break;
		case TRIG_EXT:
			devpriv->ad_mod=Mode_POST_TR;
			break;
		case TRIG_COUNT:
			devpriv->ad_mod=Mode_DELAY_TR;
			break;
		case TRIG_OTHER:
			devpriv->ad_mod=Mode_ABOUT_TR;
			break;
		default:
			devpriv->ad_mod=Mode_PACER;
		}
		if ((Mode_ABOUT_TR==devpriv->ad_mod)  || (Mode_DELAY_TR==devpriv->ad_mod))
			if(cmd->start_arg<0x2||cmd->start_arg>0xFFFF){
				printk("AI ad delay count out of range! should be 2~65535.\n");
				return -EINVAL;
			}
		devpriv->ad_delay_cnt = cmd->start_arg;

	}
	if(cmd->scan_begin_src!=TRIG_FOLLOW) return -EINVAL;
	if (devpriv->ai_use_ext_trg) {
		if(cmd->convert_src!=TRIG_EXT) return -EINVAL;
	} else {
		if(cmd->convert_src!=TRIG_TIMER) return -EINVAL;
	}
	if(cmd->scan_end_src!=TRIG_COUNT) return -EINVAL;
	if(cmd->scan_end_arg!=cmd->chanlist_len) return -EINVAL;
	if(cmd->chanlist_len>MAX_CHANLIST_LEN) return -EINVAL;

	if (cmd->convert_src==TRIG_TIMER) {
		if(cmd->convert_arg<this_board->ai_ns_min) {
			printk("AI sampling frequency > 1Mhz/s!\n");
			return -EINVAL;
		}
	}
	if (cmd->stop_src==TRIG_COUNT) {
		devpriv->ai_scans=cmd->stop_arg;
		devpriv->ai_neverending=0;
	} else {
		devpriv->ai_scans=0;
		devpriv->ai_neverending=1;
	}

	memcpy(devpriv->ai_chanlist,cmd->chanlist,sizeof(unsigned int)*cmd->scan_end_arg);
	
	devpriv->ai_act_scan=0;
        s->async->cur_chan=0;

	if ((devpriv->ai_flags & TRIG_WAKE_EOS)) { 	// don't we want wake up every scan?		
		devpriv->ai_eos=1;
	}


	tmp = devpriv->ad_mod;
	outw(Clear_AD_FIFO, dev->iobase+PCI1712_CLEAR_INTFIFO);//clear the interrupt and AD fifo

	// set up the acquisition mode
	outw(tmp, dev->iobase+PCI1712_AD_CONTROL);//eddy: original 0x8001

	//setup the AD clock
	outw(0x76, dev->iobase + PCI1712_CHIP0_CONTROL);
	outw((cmd->convert_arg*10)&0xff, dev->iobase + PCI1712_AD_CNT);
	outw(((cmd->convert_arg*10)>>8)&0xff, dev->iobase +PCI1712_AD_CNT);

	// Enable IRQ
/*	writew(0x0001, devpriv->lcfg+0x04);//allow device to respond to I/O space accesses
	writeb(0x0, devpriv->lcfg+0x08);
	writeb(0x0, devpriv->lcfg+0x09);
	writel(0x100|0x800, devpriv->lcfg + 0x68);*/

	while(inw(dev->iobase+PCI1712_INTFIFO_STATUS) & Status_AD_FE);
	all_scan = cmd->stop_arg * cmd->chanlist_len;
	udelay(all_scan * (cmd->convert_arg) + all_scan);
/*	prev_chan = -1;
	ttt = 0;
	while (ttt < all_scan){
		chan_now = inw(dev->iobase+PCI1712_CHAN_AD);
		if (prev_chan != chan_now){
			ttt++;
			prev_chan = chan_now;
		}
	}
*/
	outw(0x76, dev->iobase + PCI1712_CHIP0_CONTROL); // Stop timer
	outw(0, dev->iobase+PCI1712_AD_CONTROL);	// reset any operations
	for (ttt = 0; ttt < all_scan; ttt++){
		chan_now = inw(dev->iobase+PCI1712_CHAN_AD);
		comedi_buf_put( s->async, chan_now & 0x0fff );
//		printk("%d\n", chan_now & 0x0fff);
		
	}
//	printk("%d\n", ttt);
	pci1712_ai_cancel(dev, s);
	s->async->events |= COMEDI_CB_EOA;
	comedi_event(dev,s);

//	printk("true");
#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: END: pci1712_ai_cmd(...)\n");
#endif
	return 0;
}

/* 
==============================================================================
*/
static int move_block_from_fifo(comedi_device *dev,comedi_subdevice *s, int n)
{
	int i,j;
	j=s->async->cur_chan;
	for(i=0;i<n;i++) {
		comedi_buf_put( s->async, inw(dev->iobase+PCI1712_CHAN_AD) & 0x0fff );
		j++;
		if(j>=devpriv->ai_n_chan){
			j=0;
		        devpriv->ai_act_scan++;
		}
	}
	return 0;
}


/*
==============================================================================
*/
static void interrupt_pci1712_every_sample(void *d)
{
	comedi_device 	*dev = d;
	comedi_subdevice *s = dev->subdevices + 0;
	int		m;
#ifdef PCI171x_PARANOIDCHECK
	sampl_t sampl;
#endif

	m=inw(dev->iobase + PCI1712_INTFIFO_STATUS);

	if (m & Status_AD_FE) {
		rt_printk("comedi%d: A/D FIFO empty (%4x)\n", dev->minor, m);
		pci1712_ai_cancel(dev,s);
		s->async->events |= COMEDI_CB_EOA | COMEDI_CB_ERROR;
		comedi_event(dev,s);
		return;
	}
	if (m & Status_AD_FF) {
		rt_printk("comedi%d: A/D FIFO Full status (Fatal Error!) (%4x)\n", dev->minor, m);
		pci1712_ai_cancel(dev,s);
		s->async->events |= COMEDI_CB_EOA | COMEDI_CB_ERROR;
		comedi_event(dev,s);
		return;
	}

	outb(0, dev->iobase + PCI1712_CLEAR_INTFIFO);			// clear our INT request

	for (;!(inw(dev->iobase+PCI1712_INTFIFO_STATUS)&Status_AD_FE);) {

		comedi_buf_put( s->async, inw(dev->iobase+PCI1712_CHAN_AD) & 0x0fff);

		++s->async->cur_chan;

		if(s->async->cur_chan>=devpriv->ai_n_chan){
			s->async->cur_chan=0;
		}

		if(s->async->cur_chan == 0){	// one scan done
		        devpriv->ai_act_scan++;
    			if ((!devpriv->ai_neverending)&&(devpriv->ai_act_scan>=devpriv->ai_scans)) { // all data sampled
		    		pci1712_ai_cancel(dev,s);
				s->async->events |= COMEDI_CB_EOA;
				comedi_event(dev,s);
				return;
			}
		}
	}

	outb(0, dev->iobase + PCI1712_CLEAR_INTFIFO);			// clear our INT request

	comedi_event(dev,s);
}


/* 
==============================================================================
*/
static void interrupt_pci1712_half_fifo(void *d) 
{
	comedi_device 	*dev = d;
	comedi_subdevice *s = dev->subdevices + 0;
	int		m, samplesinbuf;
	
	m=inw(dev->iobase + PCI1712_INTFIFO_STATUS);

	if (m & Status_AD_FE) {
		rt_printk("comedi%d: A/D FIFO empty (%4x)\n", dev->minor, m);
		pci1712_ai_cancel(dev,s);
		s->async->events |= COMEDI_CB_EOA | COMEDI_CB_ERROR;
		comedi_event(dev,s);
		return;
	}
	if (m & Status_AD_FF) {
		rt_printk("comedi%d: A/D FIFO Full status (Fatal Error!) (%4x)\n", dev->minor, m);
		pci1712_ai_cancel(dev,s);
		s->async->events |= COMEDI_CB_EOA | COMEDI_CB_ERROR;
		comedi_event(dev,s);
		return;
	}

	samplesinbuf=this_board->fifo_half_size;
	if(samplesinbuf*sizeof(sampl_t)>=devpriv->ai_data_len){
		m=devpriv->ai_data_len/sizeof(sampl_t);
		if (move_block_from_fifo(dev,s,m))
			return;
		samplesinbuf-=m;
	}

	if (samplesinbuf) {
		if (move_block_from_fifo(dev,s,samplesinbuf))
			return;
	}

	if (!devpriv->ai_neverending)
    		if ( devpriv->ai_act_scan>=devpriv->ai_scans ) { /* all data sampled */
		        pci1712_ai_cancel(dev,s);
			s->async->events |= COMEDI_CB_EOA;
			comedi_event(dev,s); 
			return;
		}

	outb(0, dev->iobase + PCI1712_CLEAR_INTFIFO);			// clear our INT request

	comedi_event(dev,s);
}

//============================================================================
// interrupt_service_pci1712 ()
// Function:	interrupt service function.
// Argument: 
//		irq	irq number
//		d	data pointer
//		regs	register data
// Return value: 
//		type:	static void
// Global variable:
//
//=============================================================================
static irqreturn_t interrupt_service_pci1712(int irq, void *d PT_REGS_ARG)
{
	comedi_device *dev = d;

//	disable_irq(devpriv->irq);
	
	if (!(inw(dev->iobase + PCI1712_INTFIFO_STATUS) & Status_INT_F)) 	// is this interrupt from our board?
		return IRQ_NONE; // no, exit

	rt_printk("Interr\n");

	if (devpriv->ai_eos) {  // We use FIFO half full INT or not?
		interrupt_pci1712_every_sample(d);
	} else {
		interrupt_pci1712_half_fifo(d);
	}
	
//	enable_irq(devpriv->irq);

	return IRQ_HANDLED;
}

//============================================================================
// pci1712_reset ()
// Function:	hardware reset function.
// Argument: 
//		dev 		the operated comedi device
//		s		the operated subdevice
// Return value: 
//		type:	static void
// Global variable:
//
//=============================================================================
static int pci1712_reset(comedi_device *dev)
{
#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: BGN: pci1712_reset(...)\n");
#endif

	outw(0x30, dev->iobase + PCI1712_CHIP1_CONTROL);
	outw(0, dev->iobase+PCI1712_AD_CONTROL);	// reset any operations
	outw(0, dev->iobase + PCI1712_CLEAR_INTFIFO);		// clear interrupt and FIFO
	if (this_board->n_aochan) {
		outw(0, dev->iobase+PCI1712_DA_CONTROL); // set DACs to 0..5V
		outw(0, dev->iobase+PCI1712_DA0);		// set DA outputs to 0V
		devpriv->ao_data_insn[0]=0x0000; 
		if (this_board->n_aochan>1) {
			outw(0, dev->iobase+PCI1712_DA1);
			devpriv->ao_data_insn[1]=0x0000;
		}
	}
	outw(0, dev->iobase + PCI1712_DIO_CONFIG);		// DIO configure
	outw(0, dev->iobase + PCI1712_DIO);		// digital outputs to 0
	
#ifdef PCI1712_EXTDEBUG
	rt_printk("adv_pci1712 EDBG: END: pci1712_reset(...)\n");
#endif
	return 0;
}


//================================================================================================================
// Functions for attach

static int pci1712_find_device(comedi_device *dev, int bus, int slot)
{
	struct pci_dev *pcidev;
	
	for(pcidev = pci_get_device(PCI_ANY_ID, PCI_ANY_ID, NULL); pcidev != NULL ; 
		pcidev = pci_get_device(PCI_ANY_ID, PCI_ANY_ID, pcidev)) {
		if (pcidev->vendor == PCI_VENDOR_ID_ADVANTECH)
		{
			if(pcidev->device == 0x1712)
			{
				// was a particular bus/slot requested?
				if((bus != 0) || (slot != 0))
				{
					// are we on the wrong bus/slot?
					if(pcidev->bus->number != bus || PCI_SLOT(pcidev->devfn) != slot)
					{
						continue;
					}
				}
				goto found;
			}
		}
	}
	
	printk ("comedi%d: no supported board found! (req. bus/slot : %d/%d)\n",
		  dev->minor, bus, slot);
	return -EIO;

  found:
	printk("comedi%d: found %s (b:s:f=%d:%d:%d) , irq=%d\n", 
		dev->minor,
		this_board->name,
		pcidev->bus->number,
		PCI_SLOT(pcidev->devfn),
		PCI_FUNC(pcidev->devfn),
		pcidev->irq);

	devpriv->pci_dev = pcidev;
	
	return 0;
}


//-----------------------------------------------------------------------------------------------------------------

static int pci1712_pci_setup(comedi_device *dev, struct pci_dev *pcidev, int *io_base_ptr, int dev_minor)
{
	int io_base, io_range, lcr_io_base, lcr_io_range /*, cf_io_base, cf_io_range*/;

	// Enable PCI device
	if (pci_enable_device(pcidev) < 0) {
		printk("comedi%d: Failed to enable PCI device\n", dev_minor);
		return -EIO;
	}

	// Read local configuration register base address [PCI_BASE_ADDRESS #1].
//	cf_io_base = pci_resource_start(pcidev, 0);
//	cf_io_range = pci_resource_end(pcidev, 0) - cf_io_base +1;

//	((pci1712_private *)dev->private)->lcfg = ioremap(cf_io_base,0x100);

	// Read local configuration register base address [PCI_BASE_ADDRESS #1].
	lcr_io_base = pci_resource_start(pcidev, 1);
	lcr_io_range = pci_resource_end(pcidev, 1) - lcr_io_base +1;
	printk("comedi%d: local config registers at address 0x%4x [0x%4x]\n", dev_minor, lcr_io_base, lcr_io_range);

//	((pci1712_private *)dev->private)->las1=ioremap(lcr_io_base,0x200);
	
	// Read PCI1712 register base address [PCI_BASE_ADDRESS #2].
	io_base = pci_resource_start (pcidev, 2);
	io_range = pci_resource_end (pcidev, 2) - io_base +1;
	printk ("comedi%d: 1712 registers at address 0x%4x [0x%4x]\n", dev_minor, io_base, io_range);

//	((pci1712_private *)dev->private)->las0=ioremap(io_base,0x200);
	
	//Allocate IO ressources	  
	if (pci_request_regions(pcidev, "pci1712") < 0) {
		printk("comedi%d: I/O port conflict\n",dev_minor);
		return -EIO;
	}

	*io_base_ptr = io_base;

	return 0;
}

//============================================================================
// pci1712_attach ()
// Function:	driver attach function.
// Argument: 
//		dev	the operated comedi device
//		it	comedi config argument
// Return value: 
//		type:	static int
// Global variable:
//
//=============================================================================
static int pci1712_attach(comedi_device * dev, comedi_devconfig * it)
{
	comedi_subdevice *s;
	int subdev;
	unsigned short irq;
	unsigned int iobase;
	int retval;

	printk("comedi%d: pci1712: ", dev->minor);

	if((alloc_private(dev,sizeof(pci1712_private)))<0) {
		printk(" - Allocation failed!\n");
		return -ENOMEM;
	}

	retval = pci1712_find_device(dev, it->options[0], it->options[1]);
	if (retval < 0) return retval;

	retval = pci1712_pci_setup(dev, devpriv->pci_dev, &iobase, dev->minor);
	if (retval < 0) return retval;

	dev->iobase=iobase;

	dev->n_subdevices = SUBDEVICES_PCI1712;

	if(alloc_subdevices(dev, SUBDEVICES_PCI1712) < 0) {
		printk(KERN_ERR "comedi%d: out of memory\n", dev->minor);
		return -ENOMEM;
	}


	dev->board_name = this_board->name;

	irq = devpriv->pci_dev->irq;
	devpriv->ad_mod = Mode_PACER;

	if (this_board->have_irq) {
		if (irq) {
			if (comedi_request_irq(irq, interrupt_service_pci1712, IRQF_SHARED, "Advantech PCI-1712", dev)) {
				printk(" - unable to allocate IRQ %d, DISABLING IT", irq);
				irq=0; /* Can't use IRQ */
			} else {
				printk("irq=%d\n", irq);
			}    
		} else {
			printk("IRQ disabled\n");
		}
	} else {
		irq=0;
	}
	devpriv->irq=irq;
	dev->irq = irq;

	subdev = 0;	//ai subdevice
	if (this_board->n_aichan>0) {
		s = dev->subdevices + subdev;
		dev->read_subdev = s;
		s->type = COMEDI_SUBD_AI;
		s->subdev_flags = SDF_READABLE|SDF_GROUND;
		if (this_board->n_aichand) s->subdev_flags |= SDF_DIFF;
		s->n_chan = this_board->n_aichan;
		s->maxdata = this_board->ai_maxdata;
		s->len_chanlist = this_board->n_aichan;
		s->range_table = this_board->rangelist_ai;
		s->insn_read = pci1712_ai_insn_read;
		s->cancel = pci1712_ai_cancel;
		if (dev->irq) {
			s->do_cmdtest = pci1712_ai_cmdtest;
			s->do_cmd = pci1712_ai_cmd;
		}
	}

	subdev = 1;	//ao subdevice
	if (this_board->n_aochan>0) {
		s = dev->subdevices + subdev;
		dev->write_subdev = s;
		s->type = COMEDI_SUBD_AO;
		s->subdev_flags = SDF_WRITEABLE|SDF_GROUND;
		s->n_chan = this_board->n_aochan;
		s->maxdata = this_board->ao_maxdata;
		s->len_chanlist = this_board->n_aochan;
		s->range_table = this_board->rangelist_ao;
		s->insn_read = pci1712_ao_insn_read;
		s->insn_write = pci1712_ao_insn_write;
	}

	subdev = 2;	//dio subdevice
	if (this_board->n_diochan>0) {
		s = dev->subdevices + subdev;
		s->type = COMEDI_SUBD_DIO;
		s->subdev_flags = SDF_READABLE|SDF_WRITEABLE|SDF_GROUND|SDF_COMMON;
		s->n_chan = this_board->n_diochan;
		s->maxdata = 1;
		s->len_chanlist = this_board->n_diochan;
		s->range_table=&range_digital;
		s->io_bits = (1 << this_board->n_diochan)-1;	// all bits output
		s->state = 0;
		s->insn_config = pci1712_dio_insn_config;
		s->insn_bits = pci1712_dio_insn_bits;
	}
	
	subdev = 3;	//counter subdevice
	if (this_board->n_cntchan > 0) {
		s = dev->subdevices + subdev;
		s->type = COMEDI_SUBD_COUNTER;
		s->subdev_flags = SDF_WRITEABLE|SDF_READABLE|SDF_GROUND|SDF_COMMON;
		s->n_chan = this_board->n_cntchan;
		s->maxdata = 65535;
		s->len_chanlist = this_board->n_cntchan;
		s->insn_read = pci1712_cnt_insn_read;
		s->insn_write = pci1712_cnt_insn_write;
		s->insn_bits = pci1712_cnt_insn_bits;
	}

	devpriv->valid = 1;

	pci1712_reset(dev);

	return 0;
}

//============================================================================
// pci1712_detach ()
// Function:	driver detach function.
// Argument: 
//		dev 		the operated comedi device
// Return value: 
//		type:	static int
// Global variable:
//
//=============================================================================
static int pci1712_detach(comedi_device * dev)
{
	if (dev->private) {
		if (devpriv->valid) pci1712_reset(dev);
		if (dev->irq) comedi_free_irq(dev->irq,dev);
		if (devpriv->pci_dev) {
			if (dev->iobase) {
				pci_release_regions(devpriv->pci_dev);
				pci_disable_device(devpriv->pci_dev);
			}
			pci_dev_put(devpriv->pci_dev);
		}
	}
	return 0;
}

/* 
==============================================================================
*/
COMEDI_INITCLEANUP(driver_pci1712);
/* 
==============================================================================
*/

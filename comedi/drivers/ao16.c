/***************************************************************************\
 *                                                                         *
 *  comedi/drivers/ao16.c                                                  *
 *  Driver for Fastwel AO16 (analog output) boards.                        *
 *                                                                         *
 *  Copyright (C) 2008 Pavel Vainerman (pv) [pv@etersoft.ru]               *
 *                                                                         *
 *  COMEDI - Linux Control and Measurement Device Interface                *
 *  Copyright (C) 1998,2000 David A. Schleef <ds@schleef.org>              *
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
 *                                                                         *
 *                                                                         *
 *  --------------------------------                                       *
 *   Fastwel AO16 specification                                            *
 *  --------------------------------                                       *
 *                                                                         *
 *  driver name: ao16.o                                                    *
 *  kernel module: ao16.ko                                                 *
 *                                                                         *
 *  This card supports analog output.                                      *
 *  channels range: 0 .. 15 AO channels                                    *
 *                                                                         *
 *                                                                         *  
\***************************************************************************/

#include <linux/comedidev.h>
#include <linux/ioport.h>

#define DRIVER_NAME "ao16"
#define AO16_SIZE 0x10
#define SUBDEV_NUM 1

#define AO16_AO_NCHANS 16
#define AOR 0x20				// AO ready bit
#define ChSi 0x80				// ChSi bit
#define WAIT_DATA_TIMEOUT 1000
#define LSB 1.22

//------------------------------------------------------------------------------
// 'private' structure of subdevice
typedef struct ao16_subd_private {
  int asp_iobase;
  int val[AO16_AO_NCHANS];	// ������� ��������
} ao16_subd_private;

//------------------------------------------------------------------------------

static int ao16_attach(comedi_device* dev, comedi_devconfig* it);
static int ao16_detach(comedi_device* dev);

static int ao16_ao_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							 comedi_insn* insn, lsampl_t* data);
static int ao16_ao_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data);

static int ao16_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data);

//-----------------------------------------------------------------------------

static comedi_driver ao16_driver = {
 driver_name: DRIVER_NAME,
 module:      THIS_MODULE,
 attach:      ao16_attach,
 detach:      ao16_detach
};

COMEDI_INITCLEANUP(ao16_driver);

//------------------------------------------------------------------------------
// Init card

static int ao16_attach(comedi_device* dev, comedi_devconfig* it)
{
  int iobase, id, num;
  comedi_subdevice* subdev;
  ao16_subd_private* asp;

// \todo ����� ������� ��������������� ���������� for(BA=0x100; BA<0x400; BA+=0x10)
  iobase = it->options[0];
  
  dev->board_name = DRIVER_NAME;

  if(!request_region(iobase, AO16_SIZE, DRIVER_NAME)) {
	printk(KERN_ERR "comedi%d: I/O ports conflict\n", dev->minor);
	return -1;
  }

  /* defined iobase address */
  dev->iobase = iobase;
  id 	= inb(dev->iobase + 14);
  num 	= inb(dev->iobase + 15);

  /* defining device type */
  if( (id!='C' && id!='V') || (num!=8 && num!=16) ) {
		printk("comedi%d: your device must be 'C8','C16','V8','V16'!\n", dev->minor);
	return -1;
  }

  printk("comedi%d: found AO16 card type '%c%d'\n", dev->minor,id,num);

  if(alloc_subdevices(dev, SUBDEV_NUM) < 0) {
	printk(KERN_ERR "comedi%d: out of memory\n", dev->minor);
	return -ENOMEM;
  }

  if((asp = (ao16_subd_private*)kmalloc(sizeof(*asp), GFP_KERNEL)) == NULL) {
	printk(KERN_ERR "comedi%d: out of memory\n", dev->minor);
	return -ENOMEM;
  }

  memset(asp, 0, sizeof(*asp));

  /* initializing 1st analog output subdevice */
  asp->asp_iobase 	= dev->iobase;
  subdev 			= &dev->subdevices[0];
  subdev->private 	= asp;
  subdev->type 		= COMEDI_SUBD_AO;
  subdev->subdev_flags = SDF_READABLE | SDF_WRITABLE;
  subdev->n_chan 	= AO16_AO_NCHANS;
  subdev->maxdata 	= 0xFFF;
  subdev->insn_write = ao16_ao_subd_write;
  subdev->insn_read  = ao16_ao_subd_read;
  subdev->insn_config = ao16_insn_config;	

//	for( i=0; i<AO16_AO_NCHANS; i++ )
//		asp->val[i] = 0;

	// set ChSi bit
	outb(ChSi, dev->iobase + 0);
	printk("comedi%d: (AO16) attached\n", dev->minor);
	return 0;
}


//------------------------------------------------------------------------------
static int ao16_ao_subd_read( comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data )
{
	ao16_subd_private* asp = subdev->private;
	int chan = CR_CHAN(insn->chanspec);
	if( chan<0 || chan>=AO16_AO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(ao16_ao_subdev_read): undefined chan %d. chan range is 0..15\n", dev->minor, chan);
		return -EINVAL;
	}

//	printk("comedi%d(ao16_ao_subdev_read): chan=%d val=%d\n", dev->minor, chan,asp->val[chan]);
	(*data) = asp->val[chan];
	return 1;
}

//------------------------------------------------------------------------------
static int ao16_ao_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data)
{
	int i;
	ao16_subd_private* asp = subdev->private;
	int chan = CR_CHAN(insn->chanspec);
	int num = chan<<12;
	
	if( chan<0 || chan>=AO16_AO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(ao16_ao_subdev_write): undefined chan %d. chan range is 0..15\n", dev->minor, chan);
		return -EINVAL;
	}

	if( (*data)>4095 )
	{
		printk(KERN_ERR "comedi%d(ao16_ao_subdev_write): incorrect data=%d Must be in range 0..4095\n", dev->minor, (*data));
		return -EINVAL;
	}

	/* waiting for AOR bit */
	for( i=0; (AOR&inb(dev->iobase+0)) && i<WAIT_DATA_TIMEOUT; i++ )
	{
		comedi_udelay(1);
	}
	
	if( i >= WAIT_DATA_TIMEOUT )
	{
		return -ETIMEDOUT;
	}		

// 	printk("comedi%d(ao16_ao_subdev_write): chan=%d data=%d\n", dev->minor, chan, (*data) );	
	outw(num|(*data), dev->iobase + 2);
	asp->val[chan] = (*data);
	
	return 1;
}

//------------------------------------------------------------------------------
static int ao16_detach( comedi_device* dev )
{
	int i;
	//ao16_subd_private* asp;
	comedi_subdevice* subdev;

	printk("comedi%d: remove ao16\n",dev->minor);
  
	for(i = 0; i < dev->n_subdevices; i++) {
		subdev = &dev->subdevices[i];
		kfree(subdev->private);
	}

	release_region(dev->iobase, AO16_SIZE);
	return 0;
}
//------------------------------------------------------------------------------
static int ao16_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data)
{
	return insn->n;
}

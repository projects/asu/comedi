/***************************************************************************\
 *                                                                         *
 *  comedi/drivers/km1624.c                                                *
 *  Driver for Kaskod KM1624 board (analog input and output)               *
 *                                                                         *
 *  Copyright (C) 2011 Pavel Vainerman (pv) [pv@etersoft.ru]               *
 *                                                                         *
 *  COMEDI - Linux Control and Measurement Device Interface                *
 *  Copyright (C) 1998,2000 David A. Schleef <ds@schleef.org>              *
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
 *                                                                         *
 *                                                                         *
 *  driver name: km1624.o                                                  *
 *  kernel module: km1624.ko                                               *
 *                                                                         *
 *  This card supports analog input and output                             *
 *  channels range: 0 .. 15 AI channels                                    *
 *                  0 .. 3 AO channels (DAC 16bit) [val.range: 0...65535]  *
 *                                                                         *
 *  # comedi_config /dev/comedi0 km1624 0x150,pwrON_mask                   *
 *                                                                         *
 *  Parameter 1: IO base address                                           *
 *  Parameter 2: PowerON mask. Default: ALL                                *
 *        Маска для включения питания АЦП и ЦАП                            *
 *        Значения см. PWR_ADC|PWR_DAC0|PWR_DAC1|PWR_DAC2|PWR_DAC3         *
 *                                                                         *
 * ***********************                                                 *
 * драйвер написан с использованием примеров из документации на плату      *
 * http://www.kaskod.ru                                                    *
\***************************************************************************/
/*
	TODO:
  - сделать интерфейс для управления питанием через write и insn_config.
    Например write( 1000+x, [1,0]), где x:
	x=0 - ADC
    x=1 - DAC0
    ...
    x=4 - DAC3
    x>4 - включить ВСЁ питание

  - Задействовать прерывания
  - Как-то задейстовать обработку ошибок в DAC (обрыв линии канала). См. доку..
  - Работа с таймерами
  - Управление режимом пониженного энергопотребления для АЦП (нужно ли)
  - Реализовать работу с флэш-памятью (нужна ли)..
*/

#include <linux/comedidev.h>
#include <linux/ioport.h>

#define DRIVER_NAME "km1624"
#define KM1624_SIZE 0xFF

/* analog input/output */
#define KM1624_AI_NCHANS 16
#define KM1624_AO_NCHANS 4

#define KM1624_ADC0_MAXCHAN 8
#define KM1624_ADC1_MAXCHAN 16

#define	KM1624_ADC0	0x0  // АЦП0
#define	KM1624_ADC1	0x4	 // АЦП1

#define	KM1624_TIMER_L	0x8	// младший счётчик таймера
#define	KM1624_TIMER_H	0x9	// старший байт и регистр управления

#define	KM1624_DAC_DAT	0x10	// адрес регистра данных ЦАП
#define	KM1624_DAC_COM	0x11	// адрес регистра комманд ЦАП

#define ID_REG 0xFF

/* Wait data timeout */
#define KM1624_DATA_TIMEOUT 50000
#define ADC0_RDY 0x10
#define ADC1_RDY 0x20
#define DAC_RDY 0x40

#define PWR_TIMEOUT 50000
#define PWR_REG 0xFE
#define PWR_READY_MASK 0x8000
#define PWR_ADC 0x1
#define PWR_DAC0 0x2
#define PWR_DAC1 0x4
#define PWR_DAC2 0x8
#define PWR_DAC3 0x10
#define PWR_ALL (PWR_ADC|PWR_DAC0|PWR_DAC1|PWR_DAC2|PWR_DAC3)


//------------------------------------------------------------------------------
// 'private' structure of digital output subdevice
typedef struct km1624_subd_private {
  int asp_iobase;
} km1624_private;
#define devpriv ((km1624_private *) dev->private)
//------------------------------------------------------------------------------

static int km1624_attach(comedi_device* dev, comedi_devconfig* it);
static int km1624_detach(comedi_device* dev);

static int km1624_ai_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							 comedi_insn* insn, lsampl_t* data);
static int km1624_ao_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data);

static int km1624_ai_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data);
static int km1624_ao_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data);

//------------------------------------------------------------------------------

static comedi_driver km1624_driver = {
 driver_name: DRIVER_NAME,
 module:      THIS_MODULE,
 attach:      km1624_attach,
 detach:      km1624_detach
};

COMEDI_INITCLEANUP(km1624_driver);

//------------------------------------------------------------------------------
// read data
inline static int rd( int ba, int addr )
{
	outb(addr,ba+2);
	return inw(ba);
}

// write data
inline static void wr( int ba, int addr, int val )
{
	outb(addr,ba+2);
	outw(val,ba);
}
//------------------------------------------------------------------------------
static int km1624_set_power( comedi_device* dev, int pwr )
{
	int i;
	wr(dev->iobase, PWR_REG, pwr);
	comedi_udelay(10);
	for(i = 0; i < PWR_TIMEOUT; i++) {
		if( !(rd(dev->iobase,PWR_REG) & PWR_READY_MASK) )
			return 0;

		comedi_udelay(10);
	}	
	
	return 	-ETIME;
}
//------------------------------------------------------------------------------
static int km1624_power_on( comedi_device* dev, int mask )
{
// В документации к плате (в примерах) сказано включать не всё сразу
// иначе внутренний источник может "перегрузиться"
// поэтому включаем "последовательно"
   int i,ret=0;
   int pwr = rd(dev->iobase, PWR_REG);
   int s[5] = {PWR_ADC,PWR_DAC0,PWR_DAC1,PWR_DAC2,PWR_DAC3};
   for( i=0; i<5; i++ )
   {
		if( (s[i])& mask )
		{
			pwr |= s[i];
	  	   	ret = km1624_set_power(dev,pwr);
		}
   }

	return ret;
}
//------------------------------------------------------------------------------
static int km1624_power_off( comedi_device* dev, int mask )
{
  int i;
  int pwr = rd(dev->iobase, PWR_REG) & ~mask;
  wr(dev->iobase,PWR_REG,pwr);
	comedi_udelay(10);
	for(i = 0; i < PWR_TIMEOUT; i++) {
		if( !(rd(dev->iobase,PWR_REG) & PWR_READY_MASK) )
			return 0;

		comedi_udelay(10);
	}	
	
	return 	-ETIME;
}

//------------------------------------------------------------------------------
static int km1624_wait_ready( comedi_device* dev, int mask  )
{
	int i;
	for (i = 0; i < KM1624_DATA_TIMEOUT; i++) {
		if( !(rd(dev->iobase,3) & mask) )
			return 0;

		comedi_udelay(10);
	}	
	
	return 	-ETIME;
}
//------------------------------------------------------------------------------
static void km1624_init_adc( comedi_device* dev )
{
	wr( dev->iobase, KM1624_ADC0 + 2, 0);
	wr( dev->iobase, KM1624_ADC1 + 2, 0);
	wr( dev->iobase, KM1624_TIMER_H, 0);
}
//------------------------------------------------------------------------------
// Init card

static int km1624_attach(comedi_device* dev, comedi_devconfig* it)
{
  int iobase=0, id=0, num=0, ret=0,pwron_mask=0;
  comedi_subdevice* subdev;

  iobase = it->options[0];
  
  pwron_mask = it->options[1];
  if( pwron_mask == 0 )
  	pwron_mask = PWR_ALL;

  dev->board_name = DRIVER_NAME;
  dev->iobase = 0;
  
  if (alloc_private(dev, sizeof(km1624_private)) < 0) {
	printk(KERN_ERR "comedi%d(km1624): out of memory\n", dev->minor);
	return -ENOMEM;
  }
  memset(devpriv, 0, sizeof(km1624_private));

  if(!request_region(iobase, KM1624_SIZE, DRIVER_NAME)) {
	printk(KERN_ERR "comedi%d(km1624): I/O ports conflict\n", dev->minor);
	return -EBUSY;
  }

  dev->iobase = iobase;

  // читаем ID платы..
  outb(ID_REG,dev->iobase+2);
  id  = inw(dev->iobase)>>8;
  num = inw(dev->iobase)&0x00FF;

  /* determining device type */
  if( id != 0x12 ) /* && num != 0x01 ) */ {
	printk("comedi%d: KM1624 (1201) not found. You have ID='0x%x' num='0x%x' !\n", dev->minor, id, num);
	return -ENODEV;
  }

  printk("comedi%d: Found card KM1624. id='0x%x' num='0x%x'\n", dev->minor, id, num);

  if(alloc_subdevices(dev, 2) < 0) {
	printk(KERN_ERR "comedi%d(km1624): out of memory\n", dev->minor);
	return -ENOMEM;
  }

  ret = km1624_power_on(dev,pwron_mask);
//ret = km1624_power_on(dev,PWR_ADC);
//ret = km1624_power_on(dev,PWR_DAC0);
//ret = km1624_power_on(dev,PWR_DAC1);
//ret = km1624_power_on(dev,PWR_DAC2);
  if( ret < 0 ) {
	printk(KERN_ERR "comedi%d(km1624): Power ON failed (mask=0x%x)..\n", dev->minor,pwron_mask);
	return ret;
  }

  km1624_init_adc(dev);

  /* initializing subdevices */
  subdev = &dev->subdevices[0];
  subdev->type = COMEDI_SUBD_AI;
  subdev->subdev_flags = SDF_READABLE;
  subdev->n_chan = KM1624_AI_NCHANS;
  subdev->maxdata = 0x3FFF;
  subdev->insn_read = km1624_ai_subd_read;
  subdev->insn_config = km1624_ai_insn_config;

  subdev = &dev->subdevices[1];
  subdev->type = COMEDI_SUBD_AO;
  subdev->subdev_flags = SDF_WRITABLE;
  subdev->n_chan = KM1624_AO_NCHANS;
  subdev->maxdata = 0xFFFF;
  subdev->insn_write = km1624_ao_subd_write;
  subdev->insn_config = km1624_ao_insn_config;

  printk("comedi%d: KM1624 attached (PWR STATE: 0x%x)\n", dev->minor, rd(dev->iobase, PWR_REG) );
  return 0;
}
//------------------------------------------------------------------------------
static int km1624_ai_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data)
{
	int chan = CR_CHAN(insn->chanspec);
	int ret;
    int adc = KM1624_ADC0;
	int rdy = ADC0_RDY;

	if( chan < 0 && chan >= KM1624_AI_NCHANS )
	{
		printk(KERN_ERR "comedi%d(km1624_ai_subd_read): undefined chan %d. chan range is 0..%d\n", dev->minor, chan,KM1624_ADC0_MAXCHAN);
		return -EINVAL;
	}
	
	if( chan >= KM1624_ADC0_MAXCHAN )
	{
	    adc = KM1624_ADC1;
		rdy = ADC1_RDY;
	}

	wr(dev->iobase, adc + 0, chan<<3);
   	ret = km1624_wait_ready(dev, rdy);
	if( ret < 0 )
	{
		printk(KERN_ERR "comedi%d(km1624_ai_subd_read): timeout wait channel set..\n",dev->minor);
		return ret;
	}

//for( i=0;i<2; i++ ) {
	wr(dev->iobase, adc + 2, 0x4);
    // необходима пауза, иначе кажется плата не успевает преобразовать..
    comedi_udelay(2);
	ret = km1624_wait_ready(dev, rdy);
	if( ret < 0 )
	{
		printk(KERN_ERR "comedi%d(km1624_ai_subd_read): timeout wait data..\n",dev->minor);
		return ret;
	}
//}

    (*data) = rd(dev->iobase, adc + 1)&0x0FFF;
	return insn->n;
}
//------------------------------------------------------------------------------
// Analog Output
static int km1624_ao_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data)
{
	int ret = -EINVAL;
	int chan = CR_CHAN(insn->chanspec);
    int ndac = 0;

	if( chan < 0 || chan >=KM1624_AO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(km1624_ai_subd_write): undefined chan %d. chan range is 0..%d\n", dev->minor, chan,KM1624_AO_NCHANS);
		return -EINVAL;
	}

	ret = km1624_wait_ready(dev,DAC_RDY);
	if( ret < 0 )
	{
		printk(KERN_ERR "comedi%d(km1624_ao_subd_write): timeout wait DAC ready..\n",dev->minor);
		return ret;
	}

	ndac = 1 << chan;
    wr(dev->iobase,KM1624_DAC_COM,ndac);
	wr(dev->iobase,KM1624_DAC_DAT,(*data));

//  printk("comedi%d(km1624_ao_subd_write): DACstatus=0x%x\n",dev->minor,rd(dev->iobase,KM1624_DAC_COM));
	return insn->n;
}
//------------------------------------------------------------------------------
// Service functions 
static int km1624_ai_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data)
{
	return insn->n;
}

//------------------------------------------------------------------------------
static int km1624_ao_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data)
{
	if( data[0] == INSN_CONFIG_AIO_OUTPUT )
		return insn->n;
	
	printk(KERN_ERR "comedi%d(km1624_ao_insn_config): invalid command (nothing to be configured)\n", dev->minor);
	return -EINVAL;
}

//------------------------------------------------------------------------------
// Close Card
static int km1624_detach(comedi_device* dev)
{
	int ret;
	printk("comedi%d: remove km1624 (%d)\n",dev->minor,dev->n_subdevices);
	ret = km1624_power_off(dev,PWR_ALL);
	if( ret < 0 )
		printk(KERN_ERR "comedi%d (km1624): Power OFF failed..\n", dev->minor);
	
	if(dev->iobase)
		release_region(dev->iobase, KM1624_SIZE);

	return 0;
}

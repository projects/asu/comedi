/*******************************vv*****vvv**************************************\
 *                                                                         *
 *  comedi/drivers/unioxx5.c                                               *
 *  Driver for Fastwel UNIOxx-5 (analog and digital i/o) boards.           *
 *                                                                         *
 *  Copyright (C) 2006 Kruchinin Daniil (asgard) [asgard@etersoft.ru]      *
 *  Copyright (C) 2007 Pavel Vainerman (pv) [pv@etersoft.ru]               *
 *  Copyright (C) 2011 Vitaly Perov (vitperov) [vitperov@etersoft.ru]      *
 *                                                                         *
 *  COMEDI - Linux Control and Measurement Device Interface                *
 *  Copyright (C) 1998,2000 David A. Schleef <ds@schleef.org>              *
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
 *                                                                         *
 *                                                                         *
 *  --------------------------------                                       *
 *   Fastwel UNIOxx-5 specification                                        *
 *  --------------------------------                                       *
 *                                                                         *
 *  driver name: unioxx5.o                                                 *
 *  kernel module: unioxx5.ko                                              *
 *                                                                         *
 *  This card supports digital and analog I/O. It written for g01          *
 *  subdevices only.                                                       *
 *  channels range: 0 .. 23 dio channels                                   *
 *  and 0 .. 11 analog modules range                                       *
 *                                                                         *
 *  During attaching unioxx5 module displays modules identifiers           *
 *  (see dmesg after comedi_config) in format:                             *
 *  | [module_number] module_id |                                          *
 *                                                                         *
 *                                                                         *
 *  USE BITS: G01, P55                                                     *
 *  options: port_number<,subdevice_options,...>                           *
 *  subdevice with P55 firmware has one option: connection board type      *
 *  (see DevType enum). For example, if one has four P55 subdevs, the      *
 *  options list may be the following: 0x110,1,1,2,3 (provided that        *
 *  two TBI-24/0, one TBI-0/24 and one TBI-16/8 board are attached to      *
 *  UNIO card. If one has P55,G01,P55 and G01 subdevs, with two TBI-24/0   *
 *  boards attached, the options list may be like this: 0x110,1,1 since    *
 *  G01 firmware doesn't require extra options.                            *
\***************************************************************************/

#include <linux/comedidev.h>
#include <linux/ioport.h>

#define DRIVER_NAME "unioxx5"
#define UNIOXX5_SIZE 16
#define UNIOXX5_SUBDEV_BASE 0xA000 /* base addr of first subdev */
#define UNIOXX5_SUBDEV_ODDS 0x400
#define UNIOXX5_SUBDEV_MAX 4

#define UNIO96_1V2_BASE 0 /* base addr of first subdev */
#define UNIO96_1V2_SUBDEV_ODDS 4
#define UNIO96_1V2_SUBDEV_MAX 4

/* modules types */
#define MODULE_UNDEF_TYPE 0x00
#define MODULE_DI_TYPE 0x01
#define MODULE_DO_TYPE 0x02
#define MODULE_AI_TYPE 0x03
#define MODULE_AO_TYPE 0x04

//#define MODULE_OUTPUT_MASK 0x80 /* analog input/output */

/* constants for digital i/o */
#define UNIOXX5_NUM_OF_CHANS 24

/* constants for analog i/o */
#define TxBE  0x10 /* transmit buffer enable */
#define RxCA  0x20 /* 1 receive character available */
#define Rx2CA 0x40 /* 2 receive character available */
#define Rx4CA 0x80 /* 4 receive character available */

/* bytes mask errors */
#define Rx2CA_ERR_MASK 0x04 /* 2 bytes receiving error */
#define Rx4CA_ERR_MASK 0x08 /* 4 bytes receiving error */

#define WAIT_DATA_TIMEOUT 10000

#ifndef INSN_CONFIG_AIO_INPUT
#define INSN_CONFIG_AIO_INPUT 100
#endif

#ifndef INSN_CONFIG_AIO_OUTPUT
#define INSN_CONFIG_AIO_OUTPUT 101
#endif

#ifndef INSN_CONFIG_SUBDEV_TYPE
#define INSN_CONFIG_SUBDEV_TYPE 102
#endif

#ifndef UNIO_BTYPES
#define UNIO_BTYPES
#define UNIO_G01 1
#define UNIO_P55 2
#define UNIO_96_1v2 3
#endif

/*	
**********************************************************************
dtype:
======
	0 - no use
	1 - TBI 24/0
	2 - TBI 0/24
	3 - TBI 16/8

**********************************************************************
*/

enum DevType
{
	Unknown 	= 0,
	TBI_24_0 	= 1,
	TBI_0_24 	= 2,
	TBI_16_8 	= 3
};

static char UNKNOWN[] = "UNKNOWN TYPE";
static char UNDEFINED[] = "NO MODULE INSERTED OR ERROR";

static char IV_50M[]   = "73L - IV50M DUAL VOLTAGE INPUT";
static char IV_100M[]   = "73L - IV100M DUAL VOLTAGE INPUT";
static char IV_1[]   = "73L - IV1 DUAL VOLTAGE INPUT";
static char IV_5[]   = "73L - IV5 DUAL VOLTAGE INPUT";
static char IV_10[]  = "73L - IV10 DUAL VOLTADE INPUT";
static char IV_5B[]   = "73L - IV5B DUAL VOLTAGE INPUT";
static char IV_10B[]  = "73L - IV10B DUAL VOLTADE INPUT";

static char II_020[] = "73L - II020 DUAL CURRENT INPUT";
static char II_420[] = "73L - II420 DUAL CURRENT INPUT";
static char YY26026[] = "73YY - 26026 DUAL CURRENT INPUT";
 
static char ITCJ[] = "73L - ITCJ J-THERMOCOUPLE -210C TO 1200C";
static char ITCK[] = "73L - ITCK K-THERMOCOUPLE -100C TO 1372C";
static char ITCT[] = "73L - ITCT T-THERMOCOUPLE -240C TO 400C";
static char ITCR[] = "73L - ITCR R-THERMOCOUPLE -0C TO 1767C";
static char ITR100[] = "73L - ITR100 100 OHMS 2-WIRE RTD -50C TO 350C";
static char ITR1000[] = "73L - ITR1000 1000 OHMS 2-WIRE RTD -50C TO 350C";
static char ITR10[] = "73L - ITR10 10 OHMS 2-WIRE RTD -50C TO 350C";
static char YY26022[] = "73YY - 26022 100 OHMS 2-WIRE RTD -0C TO 100C";
static char ITR4100[] = "73L - ITR4100 100 OHMS 4-WIRE RTD -50C TO 350C";
static char YY26029_1[] = "73YY - 26029-1 25 TO 190 OHMS 4-WIRE RESISTANCE";
static char YY26025[] = "73YY - 26025 25 TO 190 OHMS 3-WIRE RESISTANCE";
static char ITR3100[] = "73L - ITR4100 100 OHMS 3-WIRE RTD -50C TO 350C";

static char OV_5[]   = "73L - OV5 DUAL VOLTAGE OUTPUT";
static char OV_xx[]   = "73L - OV5(?) DUAL VOLTAGE OUTPUT; -2.5 to 2.5V RANGE";
static char OV_10[]   = "73L - OV10 DUAL VOLTAGE OUTPUT";
static char OV_5B[]  = "73L - OV5B DUAL VOLATGE OUTPUT";
static char OV_10B[]   = "73L - OV10B DUAL VOLTAGE OUTPUT";

static char OI_020[] = "73L - OI020 DUAL CURRENT OUTPUT";
static char OI_420[] = "73L - OI420 DUAL CURRENT OUTPUT";
static char OI_024[] = "73L - OI024 DUAL CURRENT OUTPUT";
static char YY26028[] = "73YY - 26028 DUAL CURRENT OUTPUT";

static char ISeries[] = "70L - ISeries STANDARD DIGITAL INPUT";
static char YY27012[] = "70YY - 27012 24VAC/DC DIGITAL INPUT";
static char YY27013[] = "70YY - 27013 CONTACT CLOSURE DIGITAL INPUT";
static char YY27014[] = "70YY - 27014 80-140VAC/DC DIGITAL INPUT";
static char YY27015[] = "70YY - 27015 10-32VDC / 15-32VAC DIGITAL INPUT";
static char YY27016[] = "70YY - 27016 90-140VAC/DC DIGITAL INPUT";
static char YY27017[] = "70YY - 27017 180-280VAC/DC DIGITAL INPUT";

static char OSeries[] = "70L - OSeries STANDARD DIGITAL OUTPUT";
static char YY29002[] = "70YY - 29002 3A @ 250VAC/3A @ 48VDC RELAY";
static char YY29004[] = "70YY - 29004 1A @ 250VAC/0.625A @ 250VDC RELAY";
static char YY29006[] = "70YY - 29006 2A @ 48VAC/2A @ 48VDC RELAY";
static char YY29008[] = "70YY - 29008 2A @ 115VAC/2A @ 115VDC RELAY";
static char YY27018[] = "70YY - 27018 1A @ 250VDC SOLID STATE OUTPUT";
static char YY27019[] = "70YY - 27019 2A @ 60VDC SOLID STATE OUTPUT";
static char YY27020[] = "70YY - 27020 2A @ 280VAC SOLID STATE OUTPUT";

typedef struct {
	unsigned int opt_ptr; /* Pointer to current device options during configuring */
} unioxx5_priv;

#define devpriv ((unioxx5_priv *)dev->private)

/* 'private' structure for each subdevice */
typedef struct unioxx5_subd_priv {
  int usp_iobase;
  int usp_module_type[12]; 		/* 12 modules. each can be digital/analog input/output */
  unsigned char usp_prev_wr_data[12][4]; 	/* for saving previous written value for analog modules */
  unsigned char usp_prev_wr_val[3]; 		/* previous written value */
//  unsigned char usp_prev_chan_mask[3]; 	/* previous channel value */
  unsigned char btype;					/* type of bits */
  unsigned char dtype;					/* type of device */
} unioxx5_subd_priv;

static int unioxx5_attach(comedi_device* dev, comedi_devconfig* it);
static int unioxx5_subdev_write(comedi_device* dev, comedi_subdevice* subdev,
								comedi_insn* insn, lsampl_t* data);
static int unioxx5_subdev_read(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data);
static int unioxx5_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data);
static int unioxx5_detach(comedi_device* dev);
static int unioxx5_subdev_init(comedi_device *dev, comedi_subdevice* subdev, int subdev_iobase, 
									int minor, unsigned char btype,
									comedi_devconfig* it );
static int unioxx5_digital_write(unioxx5_subd_priv* usp, lsampl_t* data, 
								   int chan, int minor);
static int unioxx5_digital_read(unioxx5_subd_priv* usp, lsampl_t* data,
								  int chan, int minor);
// static void unioxx5_digital_config(unioxx5_subd_priv* usp, int mode);
static int unioxx5_analog_write(unioxx5_subd_priv* usp, lsampl_t* data, 
								  int chan, int minor);
static int unioxx5_analog_read(unioxx5_subd_priv* usp, lsampl_t* data,
					   int chan, int minor);
static int unioxx5_define_chan_offset(int chan_num, unioxx5_subd_priv* usp);
//static void unioxx5_analog_config(unioxx5_subd_priv* usp, int chan);
static int unioxx5_define_module_type( unioxx5_subd_priv* usp, int module ,int* module_type1 );

static int unioxx5_p55_init( unioxx5_subd_priv* usp, comedi_device *dev);
static int unioxx5_g01_init( unioxx5_subd_priv* usp, comedi_device *dev);
static int unio96_1v2_init( unioxx5_subd_priv* usp, comedi_device *dev);

static int unioxx5_p55_insn_config(comedi_device* dev, 
									comedi_subdevice* subdev,
									   comedi_insn* insn, lsampl_t* data);
static int unioxx5_g01_insn_config(comedi_device* dev, 
									comedi_subdevice* subdev,
									   comedi_insn* insn, lsampl_t* data);
static int unio96_1v2_insn_config(comedi_device* dev, 
									comedi_subdevice* subdev,
									   comedi_insn* insn, lsampl_t* data);
static char* module_type_to_string(const int module_type);

static comedi_driver unioxx5_driver = {
 driver_name:   DRIVER_NAME,
 module:        THIS_MODULE,
 attach:        unioxx5_attach,
 detach:        unioxx5_detach
};

static int wait_for(unioxx5_subd_priv* usp, int bitmask )
{
	int i;
	for( i=0; i<WAIT_DATA_TIMEOUT; i++ )
	{
  		if( inb(usp->usp_iobase+0) & bitmask )
			break;
		comedi_udelay(1);
	}
	
	if( i >= WAIT_DATA_TIMEOUT )
	{
		return -ETIMEDOUT;
	}
	
	return 0;
}

COMEDI_INITCLEANUP(unioxx5_driver);

static int unioxx5_attach(comedi_device* dev, comedi_devconfig* it)
{
  	int iobase, i, n_subd;
  	int id, num, ba;
	int isUNIO96_1=0;
  	unsigned char btype[UNIOXX5_SUBDEV_MAX];

	memset(btype,0,sizeof(btype));

  	iobase = it->options[0];

  	dev->board_name = DRIVER_NAME;
  	dev->iobase = iobase;

  	if(!request_region(dev->iobase, UNIOXX5_SIZE, DRIVER_NAME)) {
		printk(KERN_ERR "comedi%d: I/O port conflict (BA:0x%X)\n", dev->minor, dev->iobase);
		return -EIO;
  	}

	id = inb(dev->iobase + 0xB);
	num = inb(dev->iobase + 0xF);
	
	if( id == 'g' && num == 10 )
	{
		printk("comedi%d: Found UNIO96_1v2.(ba=%#5x)\n",dev->minor,dev->iobase);
		n_subd = 4;
		isUNIO96_1 = 1;
		iobase += UNIO96_1V2_BASE;
	}
	else
	{
	  	iobase += UNIOXX5_SUBDEV_BASE;

		/* defining number of subdevices and getting they types */
		for(i = n_subd = 0, ba = iobase; i < UNIOXX5_SUBDEV_MAX; i++, ba += UNIOXX5_SUBDEV_ODDS) 
		{
			id = inb(ba + 0xE);
			num = inb(ba + 0xF);
			if( id=='g' && num==1 )
			{
				printk("comedi%d: subdev %d type='g01'\n",dev->minor,n_subd);
				btype[i] = UNIO_G01;
				n_subd++;
			}
			else if( id=='p' && num==55 )
			{
				printk("comedi%d: subdev %d type='p55'\n",dev->minor,n_subd);
				btype[i] = UNIO_P55;
				n_subd++;
			}
			else
				printk("comedi%d: check subdev %d id='%d' num='%d'. Not supported. Ignore...\n",dev->minor,n_subd,id,num);
		}
	}
  /* unioxx5 can has from two to four subdevices */
  if(n_subd < 2) {
	printk(KERN_ERR "your card must has at least 2 'g01' or 'p55' or 'g10'(UNIO96_1v2) devices\n");
	return -EINVAL;
  }

  if(alloc_private(dev, sizeof(unioxx5_priv)) < 0) {
	printk(KERN_ERR "out of memory\n");
	return -ENOMEM;
  }

  if(alloc_subdevices(dev, n_subd) < 0) {
	printk(KERN_ERR "out of memory\n");
	return -ENOMEM;
  }

  devpriv->opt_ptr = 1; /* First option after port number */

	if( isUNIO96_1 == 1 )
	{
		outb(dev->iobase+15,0);	// Установить Банк=0
  		for( i = 0; i<UNIO96_1V2_SUBDEV_MAX; i++, iobase += UNIO96_1V2_SUBDEV_ODDS )
		{
			btype[i] = UNIO_96_1v2;
			if(unioxx5_subdev_init(dev, &(dev->subdevices[i]), iobase, dev->minor, btype[i], it) < 0)
	  			return -1;
		}
	}
	else
	{
		/* initializing each of subdevices */
  		for(i = 0; i<UNIOXX5_SUBDEV_MAX; i++, iobase += UNIOXX5_SUBDEV_ODDS) {
  			if( btype[i]!=0 ) {
				if(unioxx5_subdev_init(dev, &(dev->subdevices[i]), iobase, dev->minor, btype[i], it) < 0)
		  			return -1;
			}
  		}
	}

  printk("attached unioxx\n");
  return 1;
}

/* initializing subdevice with given address */
static int unioxx5_subdev_init(comedi_device *dev, comedi_subdevice* subdev, int subdev_iobase, 
									int minor, unsigned char btype, comedi_devconfig* it )
{
  unioxx5_subd_priv* usp;
  int ret;

  if((usp = (unioxx5_subd_priv*)kmalloc(sizeof(*usp), GFP_KERNEL)) == NULL) {
	printk(KERN_ERR "comedi%d: error! --> out of memory!\n", minor);
	return -ENOMEM;
  }

  memset(usp, 0, sizeof(*usp));
  usp->usp_iobase = subdev_iobase;
  usp->btype = btype;

  /* initial subdevice for digital or analog i/o */
  subdev->type = COMEDI_SUBD_DIO; // COMEDI_SUBD_DIO; // | COMEDI_SUBD_AI | COMEDI_SUBD_AO;
  subdev->private = usp;
  subdev->subdev_flags = SDF_READABLE | SDF_WRITABLE;
  subdev->n_chan = UNIOXX5_NUM_OF_CHANS;
  subdev->maxdata = 0xFFF;
  subdev->range_table = &range_digital;
  subdev->insn_read = unioxx5_subdev_read;
  subdev->insn_write = unioxx5_subdev_write;
  subdev->insn_config = unioxx5_insn_config;

  ret = 0;
  switch( btype )
  {
  	case UNIO_G01:
		ret = unioxx5_g01_init(usp,dev);
	break;

  	case UNIO_P55:
		usp->dtype = it->options[devpriv->opt_ptr++];
		ret = unioxx5_p55_init(usp,dev);
	break;
  	
	case UNIO_96_1v2:
		usp->dtype = it->options[devpriv->opt_ptr++];
		ret = unio96_1v2_init(usp,dev);
	break;

	default:
		break;
  }

  printk("subdevice comedi%d configured\n", minor);
  return 0;
}

static int unioxx5_g01_init( unioxx5_subd_priv* usp, comedi_device *dev )
{
	int i = 0;
	int mod_type1; //, mod_type2;

	// Настраиваем все каналы на аналоговый ввод/вывод
	if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
	outb( 1, usp->usp_iobase+0 );   	//Set Chanels to 1 0 for each module
	
	if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
	outw( 0x5555, usp->usp_iobase+1 );	// as said in documents
	
	if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
	outb( 0x55, usp->usp_iobase+3 );
	
//	if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
//	outb( 0, usp->usp_iobase+0 );

	printk("comedi%d: Checking Modules:\n", dev->minor);	

//	outb(1, usp->usp_iobase + 0);
	for(i = 1; i <= 12; i++)
	{
		unioxx5_define_module_type(usp, i, &mod_type1);
		printk("comedi%d: (%d) %s\n", dev->minor, i , module_type_to_string(mod_type1) );
//		if (mod_type1 != mod_type2)
//			printk("comedi%d: Module %d is unstable\n", dev->minor, i);
#if 0
		// Перенастраиваем каналы на DI/DO
		if( mod_type1 == MODULE_DI_TYPE )
		{
			outw(0x0000, usp->usp_iobase);
			if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
			outb(0x00, usp->usp_iobase+2);
			if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
		}
		else if( mod_type1 == MODULE_DO_TYPE )
		{
			outw(0xFFFF, usp->usp_iobase);
			if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
			outb(0xFF, usp->usp_iobase+2);
			if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
		}
#endif
	}

	outb(0, usp->usp_iobase + 0);
	if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;

	return 0;
}

//!!! This should be rewritten in more flexible way (each pin should be configured).
// Setting connection board type (TBI_xx_xx) should be performed at higher level.
static int unioxx5_p55_init( unioxx5_subd_priv* usp, comedi_device *dev )
{
	int i = 0;
	if( usp->btype != UNIO_P55 )
		return -1;

	switch(usp->dtype)
	{
		case TBI_0_24:
		{
			for(i = 0; i <= 23; i++)
				usp->usp_module_type[i / 2] = MODULE_DO_TYPE;
		
			memset(usp->usp_prev_wr_val,0,sizeof(usp->usp_prev_wr_val));
			outb(0, usp->usp_iobase + 0);
			outw(0, usp->usp_iobase + 1);
			outb(0, usp->usp_iobase + 3);
			printk("comedi%d(unioxx5_p55_init): init device type TBI_0_24\n",dev->minor);
		}
		break;

		case TBI_24_0:
		{
			for(i = 0; i <= 23; i++)
				usp->usp_module_type[i / 2] = MODULE_DI_TYPE;
		
			memset(usp->usp_prev_wr_val,0x00,sizeof(usp->usp_prev_wr_val));
			outb(0xFF, usp->usp_iobase+3);
			printk("comedi%d(unioxx5_p55_init): init device type TBI_24_0\n",dev->minor);
		}
		break;

		case TBI_16_8:
		{
			for(i = 0; i <= 15; i++)
				usp->usp_module_type[i / 2 ] = MODULE_DI_TYPE;
			for( ; i <= 23; i++)
				usp->usp_module_type[i / 2] = MODULE_DO_TYPE;
			usp->usp_prev_wr_val[0] = 0x00;
			usp->usp_prev_wr_val[1] = 0x00;
			usp->usp_prev_wr_val[2] = 0xFF;
			outb(0xFF, usp->usp_iobase+3);
			outb( (0x00&(1<<4)&(1<<2)) , usp->usp_iobase+3);
			printk("comedi%d(unioxx5_p55_init): init device type TBI_16_8\n",dev->minor);
		}
		break;
		
		default:
			printk(KERN_ERR "comedi%d(unioxx5_p55_init): unsupported device type %d\n", dev->minor, usp->dtype);
			return -EINVAL;
	}

	return 0;
}

static int unio96_1v2_init( unioxx5_subd_priv* usp, comedi_device *dev )
{
	int i = 0;
	if( usp->btype != UNIO_96_1v2 )
		return -1;

	// Установить Банк=0
	outb(dev->iobase+15,0);

	switch(usp->dtype)
	{
		case TBI_0_24:
		{
			for(i = 0; i <= 23; i++)
				usp->usp_module_type[i / 2] = MODULE_DO_TYPE;
		
			memset(usp->usp_prev_wr_val,0,sizeof(usp->usp_prev_wr_val));
			outb(0, usp->usp_iobase + 0);
			outw(0, usp->usp_iobase + 1);
			outb(0x80, usp->usp_iobase + 3);
			printk("comedi%d(unio96_1v2_init): init subdevice type TBI_0_24\n",dev->minor);
		}
		break;

		case TBI_24_0:
		{
			for(i = 0; i <= 23; i++)
				usp->usp_module_type[i / 2] = MODULE_DI_TYPE;
		
			memset(usp->usp_prev_wr_val,0x00,sizeof(usp->usp_prev_wr_val));
			outb(0x9B, usp->usp_iobase+3);
			printk("comedi%d(unio96_1v2_init): init subdevice type TBI_24_0\n",dev->minor);
		}
		break;

		case TBI_16_8:
		{
			for(i = 0; i <= 15; i++)
				usp->usp_module_type[i / 2 ] = MODULE_DI_TYPE;
			for( ; i <= 23; i++)
				usp->usp_module_type[i / 2] = MODULE_DO_TYPE;
			usp->usp_prev_wr_val[0] = 0x00;
			usp->usp_prev_wr_val[1] = 0x00;
			usp->usp_prev_wr_val[2] = 0x00;
			// гасим выходы
			outb(0, usp->usp_iobase + 0);
			outb(0, usp->usp_iobase + 1);
			outb(0x92, usp->usp_iobase+3);
			printk("comedi%d(unio96_1v2_init): init subdevice type TBI_16_8\n",dev->minor);
		}
		break;
		
		default:
			printk(KERN_ERR "comedi%d(unio96_1v2_init): unsupported device type %d\n", dev->minor, usp->dtype);
			return -EINVAL;
	}


	outb(dev->iobase+3 ,0x9B);	// Каналы 0-23  установить как выходы
    outb(dev->iobase+7 ,0x80);	// Каналы 24-47 установить как выходы
    outb(dev->iobase+11,0x80);	// Каналы 48-71 установить как выходы
    outb(dev->iobase+15,0x80);	// Каналы 72-95 установить как выходы

	return 0;
}

static int unioxx5_subdev_read(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data)
{
	unioxx5_subd_priv* usp = subdev->private;
	int chan, type;

	chan = CR_CHAN(insn->chanspec);
	type = usp->usp_module_type[chan / 2]; /* defining module type(analog or digital) */

//	printk("comedi%d(unioxx5_subdev_read): module %d type 0x%x\n", dev->minor, chan/2, usp->usp_module_type[chan / 2] );

	if( type == MODULE_DI_TYPE )
		return unioxx5_digital_read(usp, data, chan, dev->minor);


	if( type == MODULE_AI_TYPE )
		return unioxx5_analog_read(usp, data, chan, dev->minor);

	return -EINVAL;
}

static int unioxx5_subdev_write( comedi_device* dev, comedi_subdevice* subdev,
								 comedi_insn* insn, lsampl_t* data )
{
	unioxx5_subd_priv* usp = subdev->private;
	int chan, type;

	chan = CR_CHAN(insn->chanspec);
	type = usp->usp_module_type[chan / 2]; /* defining module type(analog or digital) */

	if( type == MODULE_DO_TYPE )
		return unioxx5_digital_write(usp, data, chan, dev->minor);

	if( type == MODULE_AO_TYPE )
		return unioxx5_analog_write(usp, data, chan, dev->minor);

	return -EINVAL;
}

static int unioxx5_insn_config(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data)
{
  unioxx5_subd_priv* usp = subdev->private;

  switch( usp->btype )
  {
  	case UNIO_P55:
		return unioxx5_p55_insn_config(dev,subdev,insn,data);
  	
	case UNIO_96_1v2:
		return unio96_1v2_insn_config(dev,subdev,insn,data);

  	case UNIO_G01:
		return unioxx5_g01_insn_config(dev,subdev,insn,data);
		
	default:
		return insn->n;
  }

  return insn->n;
}

static int unioxx5_p55_insn_config(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data)
{
  unioxx5_subd_priv* usp = subdev->private;

  if( usp->btype != UNIO_P55 )
    return insn->n;

  switch( data[0] ) {

  	case INSN_CONFIG_SUBDEV_TYPE:
	 	usp->dtype = data[1];
		unioxx5_p55_init(usp,dev);
	break;	
	
	default:
		break;
  }
  
  return insn->n;
}

static int unioxx5_g01_insn_config(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data)
{
  int chan_offset;
  int flags;
  int chan = CR_CHAN(insn->chanspec); // type
  int mask = 1 << (chan & 0x07);
  int mask2;
  unioxx5_subd_priv* usp = subdev->private;

  if( usp->btype != UNIO_G01 )
    return insn->n;

  if((chan_offset = unioxx5_define_chan_offset(chan,usp)) < 0) {
	  printk(KERN_ERR "comedi%d(unioxx5_insn_config): undefined chan %d. chan range is 0 .. 23\n",
			 dev->minor, chan);
	  return -EINVAL;
  }

  outb(1, usp->usp_iobase + 0);
  if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
	
 // flags = usp->usp_prev_wr_val[chan_offset - 1];
  flags = inb(usp->usp_iobase + chan_offset);

  switch(*data) {

  	case INSN_CONFIG_DIO_INPUT:
	{
		printk("comedi%d(unioxx5_insn_config): configuring module %d in DI\n",dev->minor, chan/2 );
		usp->usp_module_type[chan / 2] = MODULE_DI_TYPE;
       	flags &= ~mask;
	}
	break;
	
	case INSN_CONFIG_DIO_OUTPUT:
	{
		printk("comedi%d(unioxx5_insn_config): configuring module %d in D0\n",dev->minor, chan/2 );
		usp->usp_module_type[chan / 2] = MODULE_DO_TYPE;
		flags |= mask;
	}
	break;

	case INSN_CONFIG_AIO_INPUT:
	{
		printk("comedi%d(unioxx5_insn_config): configuring module %d in AI\n",dev->minor, chan/2 );
		usp->usp_module_type[chan / 2] = MODULE_AI_TYPE;
		// Для аналогвых входов должно быть выставлено "01"
		if( chan % 2 == 0 )
		{
			mask = 1 << (chan & 0x07);
			mask2 = 1 << ( (chan+1) & 0x07 );
		}
		else
		{
			mask = 1 << ( (chan-1) & 0x07 );
			mask2 = 1 << (chan & 0x07);
		}

		flags |= mask;
		flags &= ~mask2;
	}
	break;

	case INSN_CONFIG_AIO_OUTPUT:
	{
		printk("comedi%d(unioxx5_insn_config): configuring module %d in AO\n",dev->minor, chan/2 );
		usp->usp_module_type[chan / 2] = MODULE_AO_TYPE;
		// Для аналогвых входов должно быть выставлено "01"
		if( chan % 2 == 0 )
		{
			mask = 1 << (chan & 0x07);
			mask2 = 1 << ( (chan+1) & 0x07 );
		}
		else
		{
			mask = 1 << ( (chan-1) & 0x07);
			mask2 = 1 << (chan & 0x07);
		}

		flags |= mask;
		flags &= ~mask2;
	}
	break;

	default:
		printk(KERN_ERR "comedi%d: unknown flag='%d'\n", dev->minor,(*data));
		return -EINVAL;
  }

  printk("comedi%d(unioxx5_insn_config): configuring module %d flags=%x\n",dev->minor, chan/2, flags );

  outb(flags, usp->usp_iobase + chan_offset); /* changes type of _one_ channel */
  if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
  outb(0, usp->usp_iobase + 0); /* sets channels bank to 0(allows directly input/output) */
  if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
  //usp->usp_prev_wr_val[chan_offset - 1] = flags;

  return insn->n;
}

static int unio96_1v2_insn_config(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data)
{
  unioxx5_subd_priv* usp = subdev->private;

  if( usp->btype != UNIO_96_1v2 )
    return insn->n;

  switch( data[0] ) {

  	case INSN_CONFIG_SUBDEV_TYPE:
	 	usp->dtype = data[1];
		unio96_1v2_init(usp,dev);
	break;	
	
	default:
		break;
  }
  
  return insn->n;
}

static int unioxx5_detach(comedi_device* dev)
{
	printk("comedi%d: remove unioxx5\n",dev->minor);
	release_region(dev->iobase, UNIOXX5_SIZE);
	return 0;
}

static int unioxx5_digital_write(unioxx5_subd_priv* usp, lsampl_t* data, 
								   int chan, int minor)
{
	int chan_offset, module = chan/2;
	unsigned int val;
	int mask = 1 << (chan & 0x07);

	if((chan_offset = unioxx5_define_chan_offset(chan,usp)) < 0) {
		printk(KERN_ERR "comedi%d(unioxx5_digital_write): undefined chan %d. chan range is 0 .. 23\n", minor, chan);
		return -EINVAL;
	}

	switch( usp->btype )
	{
		case UNIO_G01:
		{
			if( usp->usp_module_type[module] != MODULE_DO_TYPE )
			{
				printk(KERN_ERR "comedi%d: module in position %d with id 0x%0x not DIGITAL OUTPUT TYPE!\n",
			   	minor,module, usp->usp_module_type[module]);
				return -EINVAL;
			}

			val = usp->usp_prev_wr_val[chan_offset - 1];
			if(*data)
				val |= mask;
			else
				val &= ~mask;

			outb(val, usp->usp_iobase + chan_offset);
			usp->usp_prev_wr_val[chan_offset - 1] = val; 
		}
		break;
		
		case UNIO_P55:
		case UNIO_96_1v2:
		{
			val = usp->usp_prev_wr_val[chan_offset];
			if(*data)
				val |= mask;
			else
				val &= ~mask;
			
			outb(val, usp->usp_iobase + chan_offset);
			usp->usp_prev_wr_val[chan_offset] = val;
		}
		break;
		
		default:
			break;
	}

  	return 0;
}

/* function for digital reading */
static int unioxx5_digital_read(unioxx5_subd_priv* usp, lsampl_t* data,
							   int chan, int minor)
{
	int chan_offset, mask = 1 << (chan & 0x07);

	if((chan_offset = unioxx5_define_chan_offset(chan,usp)) < 0) {
		printk(KERN_ERR "comedi%d(unioxx5_digital_read): undefined chan %d. chan range is 0 .. 23\n", minor, chan);
		return -EINVAL;
	}

	if( usp->btype == UNIO_G01 && usp->usp_module_type[chan/2] != MODULE_DI_TYPE )
	{
		printk(KERN_ERR "comedi%d: module in position %d with id 0x%0x not DIGITAL INPUT TYPE!\n",
		   minor, chan/2, usp->usp_module_type[chan/2]);
		return -EINVAL;
	}

	//printk("comedi%d(unioxx5_digital_read): chan=%d off=%d\n", minor, chan,chan_offset);
	(*data) = inb(usp->usp_iobase + chan_offset);

	if( usp->btype == UNIO_P55 || usp->btype == UNIO_96_1v2 )
	{
		mask = 1 << chan;
		(*data) <<=( (chan_offset)*8 );
	}
	else if( usp->btype == UNIO_G01 )
	{
		mask = 1 << (chan & 0x07);
		if(chan_offset > 1)
			chan -= 2 << chan_offset; /* this operation is created for correct readed value to 0 or 1 */
	}
	else 
	{
		printk(KERN_ERR "comedi%d: channel %d undefined type (must be G01, P55)\n",minor,chan);
		(*data) = 0;
		return -EINVAL;
	}

	(*data) &= mask;
	(*data) >>= chan;

	return 1;
}

static int unioxx5_analog_write(unioxx5_subd_priv* usp, lsampl_t* data, 
									int chan, int minor)
{
	int module = chan/2; //0..11
	int i =0;

	if( usp->btype != UNIO_G01 )
	{
		printk(KERN_ERR "comedi%d(unioxx5_analog_write): this subdevice not 'g01'\n",minor);
		return -EINVAL;
	}

	/* defining if given module can work on output */
	if( usp->usp_module_type[module] != MODULE_AO_TYPE )
	{
		printk(KERN_ERR "comedi%d: module in position %d with id 0x%x not ANALOG WRITE!\n",
		minor, module, usp->usp_module_type[module]);
		return -EINVAL;
	}

				  
//	printk("comedi%d: write to  module %d chan %d with type 0x%x, data: %d\n",
//						minor,module, chan, usp->usp_module_type[module],*data);

	i = (chan % 2) << 1;
	/* saving minor byte */
	usp->usp_prev_wr_data[module][i] = (unsigned char)(*data & 0x00FF);
	/* saving major byte */
	usp->usp_prev_wr_data[module][i+1] = (unsigned char)((*data & 0xFF00) >> 8);


	if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
	outb(module + 1, usp->usp_iobase + 5); 	/* sending module number to card(1 .. 12) */

	if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
	outb('W', usp->usp_iobase + 6); 		/* sends (W)rite command to module */

//	printk("module: %d  chanel:%d data: %d\n", module,chan,*data  ); 

	/* sending four bytes to module(one byte per cycle iteration) */
	for(i = 0; i < 4; i++) {
        if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
		outb(usp->usp_prev_wr_data[module][i], usp->usp_iobase + 6);
	}


//	while(!(RxCA&(inb(usp->usp_iobase+0))));

	return 1;
}

static int unioxx5_analog_read(unioxx5_subd_priv* usp, lsampl_t* data,
								   int chan, int minor)
{
	int module = chan/2; //Module num 0..11
	// int i=0;
	int try = 0;
	*data = 0;
	
	if( usp->btype != UNIO_G01 )
	{
		printk(KERN_ERR "comedi%d(unioxx5_analog_read): this subdevice not 'g01'\n",minor);
		return -EINVAL;
	}

	if( usp->usp_module_type[module] != MODULE_AI_TYPE )
	{
		printk(KERN_ERR "comedi%d: module in position %d with type 0x%x not ANALOG INPUT!\n",
										minor,module, usp->usp_module_type[module]);
		return -EINVAL;
	}
	

//	printk("comedi%d: read from module %d chan %d with type 0x%x\n",
//						minor,module, chan, usp->usp_module_type[module]);

	do 
	{
		if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
		outb( module+1, usp->usp_iobase+5 );

		if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
		outb( 'V', usp->usp_iobase+6 );
	

		if ( wait_for(usp,Rx4CA) == -ETIMEDOUT )	
		{
			printk("comedi%d: *** analog read timeout module %d\n", minor, module);
			return -ETIMEDOUT;
		}
		/* waits while reading four bytes will be allowed */
/*		for( ; i<WAIT_DATA_TIMEOUT; i++ )
		{
  			if( inb(usp->usp_iobase + 0) & Rx4CA ) 
				break;
			comedi_udelay(10);
		}

		if ( i >= WAIT_DATA_TIMEOUT)
		{
			printk("comedi%d: *** analog read timeout module %d\n", minor, module);
			return -EINVAL;
		}*/
	}
	while ( inb(usp->usp_iobase + 0)&Rx4CA_ERR_MASK && ++try < 5);
	
//	if ( try >= 5 )
//		printk ("*** ERROR analog read with errors\n");
	
	if( chan % 2 )
		*data = inw(usp->usp_iobase + 6); /* chan B */
	else
		*data = inw(usp->usp_iobase + 4); /* chan A */

//	printk(" *** unioxx: read from module %d chan %d : %d\n", module, chan, *data);
			
	
	return 1;
}




/*                                                    *\
 * this function defines if the given chan number     *
 * enters in default numeric interspace(from 0 to 23) *
 * and it returns address offset for usage needed     *
 * chan.                                              *
\*                                                    */
static int unioxx5_define_chan_offset( int chan_num, unioxx5_subd_priv* usp )
{
	if( chan_num < 0 || chan_num > 23 )
		return -1;

	switch(usp->btype)
	{
		case UNIO_G01:
			return (chan_num >> 3) + 1;
			
		case UNIO_P55:
		case UNIO_96_1v2:
			return (chan_num >> 3);
		break;
		
		default:
			break;
	}
	
	return -1;
}

static int unioxx5_define_module_type( unioxx5_subd_priv* usp, int module, int* module_type )
{
	int module_byte;
	unsigned char module_byte2 = 0;
//	unsigned int i=0;
	*module_type = 0;

    if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
	outb( module, usp->usp_iobase+5);	// ????? ?????? (?? 1 ?? 12)

    if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
	outb('H', usp->usp_iobase+6);

    if ( wait_for(usp,TxBE) == -ETIMEDOUT )	return -ETIMEDOUT;
	outb(0,usp->usp_iobase+6); 

	if ( wait_for(usp,Rx2CA) == -ETIMEDOUT )
	{	
		printk(KERN_ERR "comedi: Timeout while reading module type. \n" );
		return -ETIMEDOUT;
	}

 	if ( inb(usp->usp_iobase+0)&Rx2CA_ERR_MASK)
		printk(KERN_ERR "comedi: Error while readin module type (Rx2CA_ERR).\n" );

	*module_type = inw(usp->usp_iobase+6);
	module_byte = *module_type & 0xff;
	if(module_byte & 0x40) {
		usp->usp_module_type[(module-1)*2] = usp->usp_module_type[(module-1)*2+1] = 0;
		return -EINVAL; /* Reserved module type value, we don't know how to process this */
	}

	if(module_byte & 0x80) { /* Output */
		if(module_byte & 0x30) /* Kinds of analog output */
			module_byte2 = MODULE_AO_TYPE;
		else /* Otherwise digital */
			module_byte2 = MODULE_DO_TYPE;
	} else {/* Input */
		if(module_byte & 0x30) /* Kinds of analog input */
			module_byte2 = MODULE_AI_TYPE;
		else /* Otherwise digital */
			module_byte2 = MODULE_DI_TYPE;
	}

	usp->usp_module_type[module - 1] = module_byte2;

	return 0;
}

static char* module_type_to_string(const int module_type)
{
	switch (module_type)
	{
		case 0x0101:
			return ISeries;

		case 0x0202:
			return YY27012;

		case 0x0303:
			return YY27013;

		case 0x0404:
			return YY27014;

		case 0x0505:
			return YY27015;

		case 0x0606:
			return YY27016;

		case 0x0707:
			return YY27017;

		case 0x1010:
			return IV_50M;

		case 0x1111:
			return IV_100M;

		case 0x1212:
			return IV_1;

		case 0x1313:
			return IV_5;

		case 0x1414:
			return IV_10;

		case 0x1515:
			return IV_5B;

		case 0x1616:
			return IV_10B;

		case 0x2020:
			return II_420;

		case 0x2121:
			return II_020;

		case 0x2222:
			return YY26026;

		case 0x3030:
			return ITCJ;

		case 0x3131:
			return ITCK;

		case 0x3232:
			return ITCT;

		case 0x3333:
			return ITCR;

		case 0x3838:
			return ITR100;

		case 0x3939:
			return ITR1000;

		case 0x3a3a:
			return ITR10;

		case 0x3b3b:
			return YY26022;

		case 0x3c3c:
			return ITR4100;

		case 0x3d3d:
			return YY26029_1;

		case 0x3e3e:
			return YY26025;

		case 0x3f3f:
			return ITR3100;

		case 0x8080:
			return OSeries;

		case 0x8181:
			return YY29002;

		case 0x8282:
			return YY29004;

		case 0x8383:
			return YY29006;

		case 0x8484:
			return YY29008;

		case 0x8585:
			return YY27018;

		case 0x8686:
			return YY27019;

		case 0x8787:
			return YY27020;

		case 0x9090:
			return OV_5;

		case 0x9191:
			return OV_xx;

		case 0x9292:
			return OV_10;

		case 0x9393:
			return OV_5B;

		case 0x9494:
			return OV_10B;

		case 0xa0a0:
			return OI_020;

		case 0xa1a1:
			return OI_420;

		case 0xa2a2:
			return OI_024;

		case 0xa3a3:
			return YY26028;

		case 0:
			return UNDEFINED;
		default:
			return UNKNOWN;
	}
}

/***************************************************************************\
 *                                                                         *
 *  comedi/drivers/aixx5a.c                                                *
 *  Driver for Fastwel AI16-5A (AIC120), AI8-5A(AIC121), AIC123            *
 *  (analog + digital IO) boards.                                          *
 *                                                                         *
 *  Copyright (C) 2008 Pavel Vainerman (pv) [pv@etersoft.ru]               *
 *  Copyright (C) 2009 Yury Aliaev (mutabor) [mutabor@etersoft.ru]         *
 *  Copyright (C) 2011 Vitaly Perov (vitperov) [vitperov@etersoft.ru]      *
 *                                                                         *
 *  COMEDI - Linux Control and Measurement Device Interface                *
 *  Copyright (C) 1998,2000 David A. Schleef <ds@schleef.org>              *
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
 *                                                                         *
 *                                                                         *
 *  driver name: aixx5a.o                                                  *
 *  kernel module: aixx5a.ko                                               *
 *                                                                         *
 *  This card supports digital output (AIC120/121) or digital IO (AIC123)  *
 *  channels range: 0 .. 7 DI channels (AIC120/121),                       *
 *                  0 .. 23 DIO channels (AIC123)                          *
 *                  0 .. 1 AO channels,                                    *
 *                  0 .. 20 AI channels (AIC120/121),                      *
 *                  0 .. 31 AI channels (AIC123)                           *
 *                                                                         *
 *                                                                         *
 *  # comedi_config /dev/comedi0 aixx5a 0x150,1,2                          *
 *                                                                         *
 *  Parameter 1: IO base address                                           *
 *                                                                         *
 *  Parameter 2: card type, 0 -- AIC120/121, 1 -- AIC 123                  *
 *                                                                         *
 *  Parameter 3: averaging, 1 -- none, 2, 4, 8, 16 -- averaging by given   *
 *  number of samples                                                      *
 *                                                                         *
 *  Pseudo-channel (using for control purposes and extended functions):    *
 *                                                                         *
 *  100 (read) -- autoscanning: reads value from current channel and       *
 *  switches to the next channel                                           *
 *                                                                         *
 *  Instructions using to control the device:                              *
 *  INSN_CONFIG_AVERAGING                                                  *
 *  sets averaging value (2, 4, 8, 16 means averaging by                   *
 *  2, 4, 8, 16 samples respectively, any other value to switch it off)    *
 *                                                                         *
 *  INSN_CONFIG_TIMER_1                                                    *
 *  timer functions: sets acquisiton by timer at                           *
 *  given channel (writing to FIFO), value is sampling period (10 to       *
 *  655350 us), value = 0 switches timer off restoring acquisition on      *
 *  demand. Reading from FIFO is implemented by usual "read" command,      *
 *  channel number is ignored (it's set during timer initialization).      *
 *  setting the second paramer != 0 starts acquisition by timer + autoscan *
 *  starting from given channel.                                           *
 *                                                                         *
 *  INSN_CONFIG_INPUT_MASK (this and following instructions for AIC123     *
 *  only) sets current input mask for given channel (1 -- on, 0 -- off)    *
 *                                                                         *
 *  INSN_CONFIG_FILTER                                                     *
 *  1 to switch on 1 kHz filter, 0 to switch off                           *
 *                                                                         *
 *  INSN_CONFIG_0MA                                                        *
 *  1 to set 0..20 mA current DAC output, 0 -- 4..20 mA                    *
 *                                                                         *
 *  INSN_CONFIG_COMPL                                                      *
 *  1 to set complementary binary code for DAC, 0 -- direct                *
 *                                                                         *
 *  INSN_CONFIG_RESET                                                      *
 *  resets extended registers of AIC123 card                               *
 *                                                                         *
\***************************************************************************/

#include <linux/comedidev.h>
#include <linux/ioport.h>

#define DRIVER_NAME "aixx5a"
#define AIXX_SIZE 0x10

/* digital output */
#define AIXX_DO_BIAS 3
#define AIXX_DO_NCHANS (devpriv->type ? 24 : 8)

/* analog input/output */
#define AIXX_AI_NCHANS 32
#define AIXX_AO_NCHANS 508

/* DAC settings */
#define	AIXX_DAC_READY	0x40

#define	AIXX_DAC0	0
#define	AIXX_DAC1	1

/* Base code for AO */
#define AIXX_AO_BASE	0x18

/* Wait data timeout */
#define AIXX_DATA_TIMEOUT 10000

/* Bits */
#define ST_RDY 0x80 /* Ready */
#define ST_TMRFIFO 0x0810 /* Timer & fifo */
#define ST_TMR100 0x8 /* Use 100 kHz for timer clock instead of 1 MHz */
#define ST_FF_EMPTY 0x10 /* FIFO empty */
#define ST_FF_RST 0x10 /* FIFO reset */

/* Reading from 100th channel causes autoscanning procedire */
#define CONFIG_AUTOSCAN 100

/* Averaging. Use: comedi_write(100, value) Value may be 2, 4, 8, 16 for averaging by the given number of samples or any other to switch off averaging*/
#define CONFIG_AVERAGE 100

/* Timer functions, behaviour depends on value */
#define CONFIG_TIMER 200

/* Current inputs mask register (setting channel 300 + ch (ch = 0..7) causes the corresponding input to became current instead of voltage */
#define CONFIG_CURRENT 300

/* Writing 1 to 400th channel switches on 1.6 kHz input filter */
#define CONFIG_FILTER 400
/* Writing 1 to 401th channel switches current output to 0..20 mA mode instead of 4..20 mA */
#define CONFIG_0MA 401
/* Writing 1 to 402th channel switches DAC to complementary code instead fo direct */
#define CONFIG_COMPL 402
/* Writing 1 to 500-507 channels switches AO */
#define	CONFIG_AO 500

#define AIXX_AO_MINCH	(CONFIG_AO + 0)
#define AIXX_AO_MAXCH	(CONFIG_AO + 7)

/* Delay depending on amplification coefficient */
#define AIXX_DELAY comedi_udelay(devpriv->k10 ? delays[range] : 10)
/* Maximun channel number, depending on card type */
#define AIXX_MAXCH (devpriv->type ? 31 : 20)

static comedi_lrange range_analog_2={ 4, {
	BIP_RANGE(10),
	BIP_RANGE(5),
	BIP_RANGE(2.5),
	BIP_RANGE(1.25),
	}
};

static comedi_lrange range_analog_10={ 4, {
	BIP_RANGE(10),
	BIP_RANGE(1),
	BIP_RANGE(0.1),
	BIP_RANGE(0.01),
	}
};

static const unsigned int delays[] = {1600, 1920, 8960, 83200};

//------------------------------------------------------------------------------
// 'private' structure of digital output subdevice
typedef struct aixx_subd_private {
  int asp_iobase;
  int asp_prev_wr_val[3];
  int timer; /* 1 if timer is used */
  unsigned char k10; /* Amplification coefficient is power of 10 */
  unsigned char type; /* 0 for AIC120, 1 for AIC 123 */
  int AVG;
  int cpmask[4];	// Amplification coefficients masks
  int chan_range[AIXX_AI_NCHANS];	// Amplification coefficients at each channel
  int prev_channel, prev_range;
  unsigned char autoscan, timerchan;
  unsigned char control_byte;
  unsigned char aux_byte;
  unsigned char mask_byte;
} aixx_private;
#define devpriv ((aixx_private *) dev->private)

//------------------------------------------------------------------------------

static int aixx_attach(comedi_device* dev, comedi_devconfig* it);
static int aixx_detach(comedi_device* dev);

static int aixx_do_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data);
static int aixx_do_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data);
static int aixx_do_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data);
static int aixx_ai_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							 comedi_insn* insn, lsampl_t* data);
static int aixx_ao_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data);

static int aixx_ai_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data);
static int aixx_ao_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data);

//------------------------------------------------------------------------------

static comedi_driver aixx_driver = {
 driver_name: DRIVER_NAME,
 module:      THIS_MODULE,
 attach:      aixx_attach,
 detach:      aixx_detach
};

COMEDI_INITCLEANUP(aixx_driver);

//------------------------------------------------------------------------------
// Init card

static int aixx_attach(comedi_device* dev, comedi_devconfig* it)
{
  int iobase, id, num, filter, i;
  comedi_subdevice* subdev;

  iobase = it->options[0];
  filter = it->options[2];

  dev->board_name = DRIVER_NAME;
  
  if (alloc_private(dev, sizeof(aixx_private)) < 0) {
	printk(KERN_ERR "comedi%d: out of memory\n", dev->minor);
	return -ENOMEM;
  }
  memset(devpriv, 0, sizeof(aixx_private));

  if(!request_region(iobase, AIXX_SIZE, DRIVER_NAME)) {
	printk(KERN_ERR "comedi%d: I/O ports conflict\n", dev->minor);
	return -EBUSY;
  }

  /* defined iobase address */
  dev->iobase = iobase;
  id = inb(dev->iobase + 14);
  num = inb(dev->iobase + 15);

  /* determining device type (it must be 'A17'/'A27'/'A117'/'A127') */
  if(id != 'A' ||  (  num != 17 && num != 117 && num!=27 && num!=127 )  ) {
	printk("comedi%d: your device must be 'A17', 'A27', 'A117' or 'A127'. You have %c%d!\n", dev->minor, id,num);
	return -ENODEV;
  }

  if(alloc_subdevices(dev, 3) < 0) {
	printk(KERN_ERR "comedi%d: out of memory\n", dev->minor);
	return -ENOMEM;
  }


  /* Since autodetection is currently impossible due to the lack of documentation */
  devpriv->type = it->options[1]; /* 0 for AIC 120/121, 1 for AIC 123 */

  if(devpriv->type)
	if(!request_region(iobase + 0xA000, AIXX_SIZE, DRIVER_NAME)) {
		printk(KERN_ERR "comedi%d: I/O ports conflict\n", dev->minor);
		return -EBUSY;
	}

  if(num == 17 || num == 117)
	devpriv->k10 = 0;
  else
	devpriv->k10 = 1;

  /* setting amplification mask */
  outw(0 ,dev->iobase + 6);
  outw(0, dev->iobase + 8);
  comedi_udelay(devpriv->k10 ? 50 : 10);
  devpriv->prev_channel = devpriv->prev_range = -1;
  for (i=0; i<AIXX_AI_NCHANS; i++)
  	devpriv->chan_range[i] = 0;

  for (i=0; i<4; i++)
	devpriv->cpmask[i] = 0;


  switch (filter)
  {
	case 1:
		{devpriv->AVG = 0x0 ; break;}
	case 2:
		{devpriv->AVG = 0x400; break;}
	case 4:
		{devpriv->AVG = 0x500; break;}
	case 8:
		{devpriv->AVG = 0x600; break;}
	case 16:
		{devpriv->AVG = 0x700; break;}
	default:
	{
		printk(KERN_ERR "comedi%d: invalid Filter bit value. it must be 1 (not Filter), 2, 4, 8, 16 (number scan for 1 mesurement)\n", dev->minor);
		return -EINVAL;
	}
  }

  devpriv->asp_iobase = dev->iobase + AIXX_DO_BIAS;
  devpriv->timer = 0;

  /* initializing digital output subdevice */

  subdev = &dev->subdevices[0];
  subdev->type = devpriv->type ? COMEDI_SUBD_DIO : COMEDI_SUBD_DO;
  subdev->subdev_flags = devpriv->type ? SDF_WRITABLE | SDF_READABLE : SDF_WRITABLE;
  subdev->n_chan = AIXX_DO_NCHANS;
  subdev->maxdata = 0xF;
  subdev->range_table = &range_digital;
  subdev->insn_write = aixx_do_subd_write;
  if (devpriv->type) {
	subdev->insn_read = aixx_do_subd_read;
  }
  subdev->insn_config = aixx_do_insn_config;

  if(devpriv->type)
	devpriv->control_byte = inb(dev->iobase + 0xA000 + 3); /* load actual info about DIO port direction */

  /* initializing 1st analog output subdevice */

  subdev = &dev->subdevices[1];
  subdev->type = COMEDI_SUBD_AO;
  subdev->subdev_flags = SDF_WRITABLE;
  subdev->n_chan = AIXX_AO_NCHANS;
  subdev->maxdata = 0xFFF;
  subdev->insn_write = aixx_ao_subd_write;
  subdev->insn_config = aixx_ao_insn_config;

  /* initializing 2nd analog input subdevice */

  subdev = &dev->subdevices[2];
  subdev->type = COMEDI_SUBD_AI;
  subdev->subdev_flags = SDF_READABLE;
  subdev->n_chan = 101;
  subdev->maxdata = 0x3FFF;

  if(devpriv->k10)
	subdev->range_table = &range_analog_10;
  else
	subdev->range_table = &range_analog_2;

  subdev->insn_read = aixx_ai_subd_read;
  subdev->insn_config = aixx_ai_insn_config;

  printk("comedi%d: AIxx_5a attached\n", dev->minor);
  return 0;
}


//------------------------------------------------------------------------------
// digital output
static int aixx_do_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data)
{
  int channel, mask, val, ind;

  channel = CR_CHAN(insn->chanspec);
  mask = 1 << (channel & 0x7); /* counting mask */
  ind = channel >> 3; /* Channel, actual only for AIC123, always 0 for 120/121 */

  val = devpriv->asp_prev_wr_val[ind]; /* gets previous writed value */

  if(*data)
	val |= mask;
  else
	val &= ~mask;

  if(devpriv->type)
	outb(val, dev->iobase + 0xA000 + ind);
  else
	outb(val, devpriv->asp_iobase);

  devpriv->asp_prev_wr_val[ind] = val; /* saves current value */
  return 1;
}

//------------------------------------------------------------------------------
// digital input, only for AIC123
static int aixx_do_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data)
{
  int channel, mask, ind;

  if(!devpriv->type) {
	printk(KERN_ERR "comedi%d(aixx_do_subdev_config): DO port is readable only at AIC123, you have AIC120/AIC121\n", dev->minor);
	return -EINVAL;
  }

  channel = CR_CHAN(insn->chanspec);
  mask = 1 << (channel & 0x7); /* counting mask */
  ind = channel >> 3; /* Channel, actual only for AIC123, always 0 for 120/121 */

  *data = (inb(dev->iobase + 0xA000 + ind) & mask) ? 1 : 0;

  return 1;
}

static int aixx_set_amplification(comedi_device *dev, int channel, int range)
{
	int sdv, ba, old;
	int code = 0;

	/* Check if the appropriate amplification coefficient is set */
	if (devpriv->chan_range[channel] != range) {
		/* Amp. coeff. validation */
		switch (range)
		{
			case 0:
				code = 0;
				break;
			case 1:
				code = 1;
				break;
			case 2:
				code = 2;
				break;
			case 3:
				code = 3;
				break;
			default:
				printk(KERN_ERR "comedi%d: amplification coefficient can be from 0 to 3.\n", dev->minor);
				return 0;
		}
		/* New mask */
		ba = channel >> 2;	/* Base address offset */
		sdv = (channel & 3) * 2;	/* Bits offset */
		old = devpriv->cpmask[ba] & (3 << sdv);
		devpriv->cpmask[ba] = devpriv->cpmask[ba] - old;
		devpriv->cpmask[ba] = devpriv->cpmask[ba] + (code << sdv);

		/* Set new amp. coeff */
		outb(devpriv->cpmask[ba] ,dev->iobase + 6 + ba);
		devpriv->chan_range[channel] = range;
		AIXX_DELAY;
	}

	return 1;
}

//------------------------------------------------------------------------------
// Analog Input

static inline int aixx_normalize(int tmp)
{
	int zn, ch;

	zn = tmp & 0xC000;
	ch = tmp & 0x3FFF;

	if (zn > 0)
		return tmp - 0xFFFF;
	else
		return ch;
}

static int aixx_ai_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data)
{
	int i;
	int channel = CR_CHAN(insn->chanspec);

	/* Checking if autoscanning mode is used correctly, e.g. value is read from only 100th channel when
	   it's set and not read from 100th channel when it's not set. */
	if(devpriv->timer && (devpriv->autoscan != (channel == 100))) {
		printk(KERN_ERR "comedi%d(aixx_ai_subdev_read): invalid autoscan mode usage\n", dev->minor);
		return -EINVAL;
	}

	if(devpriv->timer && channel == devpriv->timerchan) { /* Acquisition by timer, read from FIFO, >1 reads at once is supported */
		int n_toread = insn->n;
		int n_samples = 0;

		while (n_toread-- && !(inb(dev->iobase + 11) & ST_FF_EMPTY))
			data[n_samples++] = aixx_normalize(inw(dev->iobase + 12));

		return insn->n = n_samples;
	} else { /* Normal 1-time acquisition */
		int autoscan = 0;
		int range = CR_RANGE(insn->chanspec);

		if(channel == 100) {
			devpriv->autoscan = autoscan = 1;
			channel = 0; /* In autoscan mode channel number isn't used */
		} else {
			if(channel > AIXX_MAXCH || channel < 0) {
				printk(KERN_ERR "comedi%d(aixx_ai_subdev_read): undefined chan %d. Chan range is 0..%d\n", dev->minor, channel, AIXX_MAXCH);
				return -EINVAL;
			}
			if(!aixx_set_amplification(dev, channel, range))
				return 0;
		}

		/* Set input type (single-ended or diff) */
		if(CR_AREF(insn->chanspec)!=AREF_DIFF)
			channel |= 0x20;
		else
			channel &= 0x1F;

		outw(autoscan, dev->iobase + 0);
		if(channel != devpriv->prev_channel || range != devpriv->prev_range || (!autoscan && devpriv->autoscan)) {
			outb(channel, dev->iobase + 2); /* writes channel number; means nothing for autoscan, but set input type */
			AIXX_DELAY; /* Maybe this may be omitted for autoscan mode for performance increasing */
			devpriv->prev_channel = channel;
			devpriv->prev_range = range;
			devpriv->autoscan = autoscan;
		}

		outw(ST_RDY|devpriv->AVG|autoscan, dev->iobase + 0); /* ST_RDY on */

		for (i = 0; i < AIXX_DATA_TIMEOUT; i++) {
			if (inb(dev->iobase + 0) & ST_RDY) { /* waiting for ST_RDY */
				*data = aixx_normalize(inw(dev->iobase + 2)); /* getting data */
				break;
			}

			comedi_udelay(10);
		}

		if (i >= AIXX_DATA_TIMEOUT)
			return -ETIME;

		return 1;
	}
}

//------------------------------------------------------------------------------
// Analog Output

static int aixx_ao_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data)
{
	int ret;
	int channel = CR_CHAN(insn->chanspec);

	printk("comedi%d(aixx_ao_subd_write): channel = %d\n", dev->minor, channel);

	if ((channel >= AIXX_DAC0) && (channel <= AIXX_DAC1)) {
		channel <<= 12;

		while(!(inb(dev->iobase) & AIXX_DAC_READY));
		outw(*data | channel, dev->iobase + 14);
		comedi_udelay(10);

		ret = 2;
	}
	else if ((channel >= AIXX_AO_MINCH) && (channel <= AIXX_AO_MAXCH)) {
		ret = -EPERM;

		if (devpriv->autoscan == 0) {
			channel = AIXX_AO_BASE + channel % CONFIG_AO;
			outb(channel, dev->iobase + 2);

			ret = 1;
		}
	}
	else {
		printk(KERN_ERR "comedi%d(aixx_ao_subd_write): invalid channel = %d\n", dev->minor, channel);
		ret = -EINVAL;
	}

	return ret;
}

//------------------------------------------------------------------------------
// Service functions 
static int aixx_ai_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data)
{
	int channel = CR_CHAN(insn->chanspec);
	int range = CR_RANGE(insn->chanspec);
	unsigned int period;
	unsigned char mask, khz100 = 0, autoscan = 0;

	if(!devpriv->type && data[0] > INSN_CONFIG_TIMER_1 && data[0] != INSN_CONFIG_AIO_INPUT) {
		printk(KERN_ERR "comedi%d(aixx_ai_insn_config): extended commands are only for AIC123, you have 120/121\n", dev->minor);
		return -EINVAL;
	}

	switch(data[0]) {
		case INSN_CONFIG_AIO_INPUT: /* It's already input, do nothing */
			break;
		case INSN_CONFIG_AVERAGING: /* Set averaging; data[1] -- averaging value (by 2, 4, 8 or 16 samples, any other to switch off) */
			switch(data[1]) {/* Value */
				case 2: {devpriv->AVG = 0x400; break;}
				case 4: {devpriv->AVG = 0x500; break;}
				case 8: {devpriv->AVG = 0x600; break;}
				case 16: {devpriv->AVG = 0x700; break;}
				default: {devpriv->AVG = 0; break;}
			}
			break;
		case INSN_CONFIG_TIMER_1: /* Timer functions; data[1]: period (10..65535 us, 0 to switch off) */
			/* data[2] != 0 enables autoscanning starting from given channel */
			period = data[1];

			if(channel > 20 && devpriv->type == 0) { /* AIC 120 has only 20 channes */
				printk(KERN_ERR "comedi%d(aixx_ai_subdev_write): invalid channel %d\n", dev->minor, channel);
				return -EINVAL;
			}
			if(data[2])
				autoscan = 1;

			if(period == 0) { /* Stop timer; return to acquisition on demand */
				devpriv->timer = 0;
				outw(devpriv->AVG, dev->iobase + 0); /* Clear timer and fifo bits (n++ bit is also cleared if set) */
				return 1;
			}
			if(period > 655350 || period < 10) {
				printk(KERN_ERR "comedi%d(aixx_ai_subdev_write): timer period too low or high %d\n", dev->minor, period);
				return -EINVAL;
			}
			if(period > 65535) { /* Use 100 kHz timer clock frequency instead of 1 MHz */
				khz100 = 1;
				period /= 10;
			}

			outw(0, dev->iobase + 0); /* Control register reset */
			if(!aixx_set_amplification(dev, channel, range))
				return 0;

			devpriv->timerchan = autoscan ? 100 : channel;
			/* Set input type (single-ended or diff) */
			if(CR_AREF(insn->chanspec)!=AREF_DIFF)
				channel |= 0x20;
			else
				channel &= 0x1F;

			if(channel != devpriv->prev_channel || range != devpriv->prev_range || devpriv->autoscan != autoscan) {
				outb(channel, dev->iobase + 2); /* writes channel number */
				AIXX_DELAY;
				devpriv->prev_channel = channel;
				devpriv->prev_range = range;
			}
			devpriv->autoscan = autoscan;

			outw(period, dev->iobase + 4); /* Set timer period */
			outb(ST_FF_RST, dev->iobase + 11); /* FIFO reset */
			outw(devpriv->AVG | ST_TMRFIFO | (khz100 ? ST_TMR100 : 0) | autoscan, dev->iobase + 0); /* Set timer, fifo and optionaly 100 kHz bits */

			devpriv->timer = 1;
			break;
		case INSN_CONFIG_INPUT_MASK: /* Set current input mask for the selected input */
			mask = 1 << channel;

			if(data[1])
				devpriv->mask_byte |= mask;
			else
				devpriv->mask_byte &= ~mask;
			outb(devpriv->mask_byte, dev->iobase + 0xA000 + 5);
			break;
		case INSN_CONFIG_FILTER: /* 0 to turn off 1.6 kHz analog filter, any other value to turn on */
			if(data[1])
				devpriv->aux_byte |= 0x1;
			else
				devpriv->aux_byte &= ~0x1;
			outb(devpriv->aux_byte, dev->iobase + 0xA000 + 11);
			break;
		case INSN_CONFIG_0MA: /* Switches DAC range to 0..20 mA instead of 4..20 */
			if(data[1])
				devpriv->aux_byte |= 0x2;
			else
				devpriv->aux_byte &= ~0x2;
			outb(devpriv->aux_byte, dev->iobase + 0xA000 + 11);
			break;
		case INSN_CONFIG_COMPL: /* Set DAC complementary binary code */
			if(data[1])
				devpriv->aux_byte |= 0x4;
			else
				devpriv->aux_byte &= ~0x4;
			outb(devpriv->aux_byte, dev->iobase + 0xA000 + 11);
			break;
		case INSN_CONFIG_RESET:
			outb(1, dev->iobase + 0xA000 + 12);
			devpriv->aux_byte = 0;
			devpriv->mask_byte = 0;
			devpriv->control_byte = inb(dev->iobase + 0xA000 + 3);
		default:
			printk(KERN_ERR "comedi%d(aixx_ai_dio_config): incorrect command %d\n", dev->minor, channel);
			return -EINVAL;
	}
	return insn->n;
}

//------------------------------------------------------------------------------
// Configure direction of AIC123 DIO port
static int aixx_do_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data)
{
	unsigned char bit;
	int chan = CR_CHAN(insn->chanspec);

	if(!devpriv->type) {
		if(data[0] == INSN_CONFIG_DIO_OUTPUT) /* For AIC120/121 it's always output */
			return insn->n;
		printk(KERN_ERR "comedi%d(aixx_do_subdev_config): DO port is configurable only at AIC123, you have AIC120/AIC121\n", dev->minor);
		return -EINVAL;
	}

	if(chan < 8)
		bit = 0x10;
	else if(chan < 16)
		bit = 0x2;
	else if(chan < 20)
		bit = 1;
	else bit = 8;

	switch(data[0]) {
		case INSN_CONFIG_DIO_INPUT:
			devpriv->control_byte |= bit;
			break;
		case INSN_CONFIG_DIO_OUTPUT:
			devpriv->control_byte &= ~bit;
			break;
		case INSN_CONFIG_DIO_QUERY:
			data[1] = (devpriv->control_byte & bit) ? COMEDI_INPUT : COMEDI_OUTPUT;
			return insn->n;
		default:
			printk(KERN_ERR "comedi%d(aixx_do_insn_config): invalid command\n", dev->minor);
			return -EINVAL;
	}
	outb(devpriv->control_byte, dev->iobase + 0xA000 + 3);
	return insn->n;
}

//------------------------------------------------------------------------------
// Pseudo-configure AO port (just check if it's AO)
static int aixx_ao_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data)
{
	if(data[0] == INSN_CONFIG_AIO_OUTPUT)
		return insn->n;
	else {
		printk(KERN_ERR "comedi%d(aixx_ao_insn_config): invalid command (nothing to be configured)\n", dev->minor);
		return -EINVAL;
	}
}

//------------------------------------------------------------------------------
// Close Card
static int aixx_detach(comedi_device* dev)
{
	printk("comedi%d: remove aixx5 (%d)\n",dev->minor,dev->n_subdevices);
	if(dev->iobase)
		release_region(dev->iobase, AIXX_SIZE);

	if(devpriv->type)
		release_region(dev->iobase + 0xA000, AIXX_SIZE);

	return 0;
}

/***************************************************************************\
 *                                                                         *
 *  comedi/drivers/adv_uno_dio.c                                           *
 *  Driver for Advantech UNO DIO boards.                                   *
 *                                                                         *
 *  Copyright (C) 2011 Pavel Vaynerman (pv) [pvetersoft.ru]                *
 *                                                                         *
 *  COMEDI - Linux Control and Measurement Device Interface                *
 *  Copyright (C) 1998,2000 David A. Schleef <ds@schleef.org>              *
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
 *                                                                         *
 *                                                                         *
 *  --------------------------------                                       *
 *   Advantech UNO DIO specification                                       *
 *  --------------------------------                                       *
 *  Tested for UNO-3272                                                    *
 *                                                                         *
 *  driver name: adv_uno_dio.o                                             *
 *  kernel module: adv_uno_dio.ko                                          *
 *                                                                         *
 *  This is simple driver. Supports only 4 DO and 4 DI inputs              *
 *  But the card supports any more functions ...                           *
 *                                                                         * 
 *  channels range: 0 .. 3 DI channels                                     *
 *  channels range: 4 .. 7 DO channels                                     *
 *                                                                         * 
 *  channel 100 - DIAG LED        read only                                *
 *      0 - disable diagnostic LED                                         * 
 *      1 - Light on                                                       *
 *      3 - ast flicker                                                    *
 *      5 - normal flick                                                   *
 *      7 - slow flicker                                                   *
 *                                                                         * 
 *  channel 101 - BUZZER          read only                                *
 *      0 - disable alarm buzzer                                           *
 *      1 - beep on                                                        *
 *      3 - short beep                                                     *
 *      5 - normal beep                                                    *
 *      7 - long beep                                                      *
 *                                                                         * 
 *  channel 102 - POWER STATUS     read only                               *
 *                                                                         *
 *                                                                         *  
\***************************************************************************/

#include <linux/comedidev.h>
#include <linux/ioport.h>

#define DRIVER_NAME "adv_uno_dio"
#define DIO_SIZE 0x10
#define SUBDEV_NUM 1
#define DIO_NCHANS 8
#define DO_BEGIN_CHAN 4
#define DEFAULT_IOBASE_ADDRESS	0x200
#define DI_OFFSET                0x00   // digital input register
#define DO_OFFSET                0x01   // digital output register

#define LED_OFFSET               0x10
#define LED_OFFSET1              0x14
#define PLED_OFFSET              0x14
#define BUZZER_OFFSET            0x11
#define POWER_OFFSET             0x18

#define LED_CHAN_NUM              100
#define BUZZER_CHAN_NUM           101
#define POWER_CHAN_NUM            102
#define MAX_NUM_CHAN              103

//int irqr = 7;
//int major = 0;
//int advhdbase = 0x000FE0C1;
//int adv_range = 32;

#define LSB 1.22

//------------------------------------------------------------------------------
// 'private' structure of subdevice
typedef struct adv_uno_dio_subd_private {
  int adv_iobase;
  unsigned short buff[2];
} adv_uno_dio_private;
#define devpriv ((adv_uno_dio_private *) dev->private)

//------------------------------------------------------------------------------
static int adv_uno_dio_attach(comedi_device* dev, comedi_devconfig* it);
static int adv_uno_dio_detach(comedi_device* dev);

static int adv_uno_dio_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							 comedi_insn* insn, lsampl_t* data);
static int adv_uno_dio_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data);

static int adv_uno_dio_insn_config(comedi_device* dev, comedi_subdevice* subdev,
								   comedi_insn* insn, lsampl_t* data);


//-----------------------------------------------------------------------------

static comedi_driver adv_uno_dio_driver = {
 driver_name: DRIVER_NAME,
 module:      THIS_MODULE,
 attach:      adv_uno_dio_attach,
 detach:      adv_uno_dio_detach
};

COMEDI_INITCLEANUP(adv_uno_dio_driver);

//------------------------------------------------------------------------------
// Init card

static int adv_uno_dio_attach(comedi_device* dev, comedi_devconfig* it)
{
  int iobase; //id, num, i;
  comedi_subdevice* subdev;

  iobase = it->options[0];
  if( !iobase )
      iobase = DEFAULT_IOBASE_ADDRESS;
  
  dev->board_name = DRIVER_NAME;
  dev->iobase = 0;

  if(!request_region(iobase, DIO_SIZE, DRIVER_NAME)) {
	printk(KERN_ERR "comed%d(adv_uno_dio): I/O ports conflict\n", dev->minor);
	return -1;
  }

  dev->iobase = iobase;

  /* defining device type */
  // printk("comedi%d: cannot check type of the card, decided DO32\n", dev->minor);

  if(alloc_subdevices(dev, SUBDEV_NUM) < 0) {
	printk(KERN_ERR "comedi%d(adv_uno_dio): out of memory\n", dev->minor);
	return -ENOMEM;
  }

  if (alloc_private(dev, sizeof(adv_uno_dio_private)) < 0) {
	printk(KERN_ERR "comedi%d(adv_uno_dio): out of memory\n", dev->minor);
	return -ENOMEM;
  }

  memset(devpriv, 0, sizeof(adv_uno_dio_private));

  /* initializing output subdevice */
  devpriv->adv_iobase 	= dev->iobase;
  subdev 		= &dev->subdevices[0];
  subdev->type 		= COMEDI_SUBD_DIO;
  subdev->subdev_flags = SDF_READABLE | SDF_WRITABLE;
  subdev->n_chan 	= MAX_NUM_CHAN; // DIO_NCHANS;
  subdev->maxdata 	= 0xFFFF;
  subdev->insn_write = adv_uno_dio_subd_write;
  subdev->insn_read  = adv_uno_dio_subd_read;
  subdev->insn_config = adv_uno_dio_insn_config;	

  printk("comedi%d: (adv_uno_dio) attached\n", dev->minor);
  return 0;
}

//------------------------------------------------------------------------------
static int adv_uno_dio_set_led_state( comedi_device* dev, lsampl_t* data )
{
	outb((*data), dev->iobase + LED_OFFSET);
	return 1;
}
//------------------------------------------------------------------------------
static int adv_uno_dio_set_buzzer_state( comedi_device* dev, lsampl_t* data )
{
	outb((*data), dev->iobase + BUZZER_OFFSET);
	return 1;
}
//------------------------------------------------------------------------------
static int adv_uno_dio_get_power_status( comedi_device* dev, lsampl_t* data )
{
	(*data) = inb(dev->iobase + POWER_OFFSET);
	return 1;
}
//------------------------------------------------------------------------------
static int adv_uno_dio_subd_read( comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data )
{
	short state = 0;
	int chan = CR_CHAN(insn->chanspec);
	
	if( chan == POWER_CHAN_NUM )
		return adv_uno_dio_get_power_status(dev,data);
	
	if( chan < 0 || chan >= DIO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(adv_uno_dio_subdev_read): undefined chan %d. Channel range is 0..%d\n", dev->minor, chan, DIO_NCHANS-1);
		return -EINVAL;
	}

	if( chan < DO_BEGIN_CHAN )
		state = inb(dev->iobase + DI_OFFSET);
	else
	{
		state = inb(dev->iobase + DO_OFFSET);
		chan = chan - DO_BEGIN_CHAN;
	}

	(*data) = (state >> chan) & 1;
	
	return 1;
}

//------------------------------------------------------------------------------
static int adv_uno_dio_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data)
{
	unsigned short chan = CR_CHAN(insn->chanspec);
	unsigned short mask;
	unsigned short val;

	if( chan == LED_CHAN_NUM )
		return adv_uno_dio_set_led_state(dev,data);

	if( chan == BUZZER_CHAN_NUM )
		return adv_uno_dio_set_buzzer_state(dev,data);

	if( chan < DO_BEGIN_CHAN || chan >= DIO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(adv_uno_dio_subdev_write): undefined chan %d. DO channel range is %d..%d\n", dev->minor, chan, DO_BEGIN_CHAN, DIO_NCHANS-1);
		return -EINVAL;
	}
	chan = chan - DO_BEGIN_CHAN;
	mask = 1 << (chan%16);
//	val = devpriv->buff[ind];
	val = inb(dev->iobase + DO_OFFSET);

	if (*data)
		val |= mask;
	else
		val &= ~mask;
		
	outb(val ,dev->iobase + DO_OFFSET);
//	devpriv->buff[ind] = val;

	return 1;
}

//------------------------------------------------------------------------------
static int adv_uno_dio_detach( comedi_device* dev )
{
	printk("comedi%d: remove adv_uno_dio\n",dev->minor);
	if( dev->iobase )
		release_region(dev->iobase, DIO_SIZE);
	
	return 0;
}
//------------------------------------------------------------------------------
static int adv_uno_dio_insn_config(comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data)
{
	return insn->n;
}

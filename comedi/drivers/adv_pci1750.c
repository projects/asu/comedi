/***************************************************************************\
 *                                                                         *
 *  comedi/drivers/adv_pci1750.c                                           *
 *  Driver for Advantech PCI-1750 DIO boards.                              *
 *                                                                         *
 *  Copyright (C) 2011 Pavel Vaynerman (pv) [pvetersoft.ru]                *
 *                                                                         *
 *  COMEDI - Linux Control and Measurement Device Interface                *
 *  Copyright (C) 1998,2000 David A. Schleef <ds@schleef.org>              *
 *                                                                         *
 *  This program is free software; you can redistribute it and/or modify   *
 *  it under the terms of the GNU General Public License as published by   *
 *  the Free Software Foundation; either version 2 of the License, or      *
 *  (at your option) any later version.                                    *
 *                                                                         *
 *  This program is distributed in the hope that it will be useful,        *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *  GNU General Public License for more details.                           *
 *                                                                         *
 *  You should have received a copy of the GNU General Public License      *
 *  along with this program; if not, write to the Free Software            *
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.              *
 *                                                                         *
 *                                                                         *
 *  --------------------------------                                       *
 *   Advantech PCI-1750 DIO specification                                  *
 *  --------------------------------                                       *
 *  driver name: adv_pci1750.o                                             *
 *  kernel module: adv_pci1750.ko                                          *
 *                                                                         *
 *  channels range: 0 .. 15 DI channels  (subdev 0)                        *
 *  channels range: 0 .. 15 DO channels  (subdev 1)                        *
 *                                                                         *
 * Note: is a simple driver only supports DIO                              *
 * (without the support of the interrupt)                                  * 
\***************************************************************************/

#include <linux/comedidev.h>
#include <linux/ioport.h>
#include "comedi_pci.h"

#define DRIVER_NAME "adv_pci1750"
#define SUBDEV_NUM 2
#define DI_NCHANS 16
#define DO_NCHANS 16

//------------------------------------------------------------------------------
// 'private' structure of subdevice
typedef struct adv_pci1750_subd_private {
  unsigned short prev_val;
  struct pci_dev* card;
} adv_pci1750_private;
#define devpriv ((adv_pci1750_private *) dev->private)

//------------------------------------------------------------------------------
static int adv_pci1750_attach(comedi_device* dev, comedi_devconfig* it);
static int adv_pci1750_detach(comedi_device* dev);

static int adv_pci1750_do_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							 comedi_insn* insn, lsampl_t* data);
static int adv_pci1750_do_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data);
static int adv_pci1750_di_subd_read(comedi_device* dev, comedi_subdevice* subdev,
							 comedi_insn* insn, lsampl_t* data);
//-----------------------------------------------------------------------------

static comedi_driver adv_pci1750_driver = {
 driver_name: DRIVER_NAME,
 module:      THIS_MODULE,
 attach:      adv_pci1750_attach,
 detach:      adv_pci1750_detach
};

COMEDI_INITCLEANUP(adv_pci1750_driver);

//------------------------------------------------------------------------------
// Init card

static int adv_pci1750_attach(comedi_device* dev, comedi_devconfig* it)
{
  comedi_subdevice* subdev;
  unsigned long iobase;
  struct pci_dev* card = NULL;

	if((card=pci_get_device(PCI_VENDOR_ID_ADVANTECH, 0x1750, card)))
	{
		printk("comed%d(adv_pci1750): found 'PCI-1750' card..\n", dev->minor);
//		printk("Name : %s", card->pretty_name);
//		printk("IRQ: %d", card->irq);
		//if( comedi_pci_enable(card,DRIVER_NAME) ) {
		if( comedi_pci_enable_no_regions(card) ) {
			printk("comed%d(adv_pci1750): Couldn't enable pci card\n", dev->minor);
			return -EIO;
		}
	}
	else
	{
		printk("comed%d(adv_pci1750): PCI-1750 not found!\n", dev->minor);
		return -EIO;
	}

  iobase = pci_resource_start(card, 2);
  dev->board_name = DRIVER_NAME;
  dev->iobase = 0;
/*
  if( pci_request_regions(card, DRIVER_NAME) < 0 ) {
	printk(KERN_ERR "comedi%d(adv_pci1750): I/O ports conflict\n", dev->minor);
	return -1;
  }
*/
  dev->iobase = iobase;

  if(alloc_subdevices(dev, SUBDEV_NUM) < 0) {
	printk(KERN_ERR "comedi%d(adv_pci1750): out of memory\n", dev->minor);
	return -ENOMEM;
  }

  if (alloc_private(dev, sizeof(adv_pci1750_private)) < 0) {
	printk(KERN_ERR "comedi%d(adv_pci1750): out of memory\n", dev->minor);
	return -ENOMEM;
  }

  memset(devpriv, 0, sizeof(adv_pci1750_private));
  devpriv->card = card;
  devpriv->prev_val = 0;
 
  /* initializing subdevice */
  subdev = &dev->subdevices[0];
  subdev->type 		= COMEDI_SUBD_DI;
  subdev->subdev_flags = SDF_READABLE;
  subdev->n_chan 	= DI_NCHANS;
  subdev->maxdata 	= 0xFF;
  subdev->insn_read  = adv_pci1750_di_subd_read;
	
  subdev = &dev->subdevices[1];
  subdev->type 		= COMEDI_SUBD_DO;
  subdev->subdev_flags = SDF_READABLE | SDF_WRITABLE;
  subdev->n_chan 	= DO_NCHANS;
  subdev->maxdata 	= 0xFF;
  subdev->insn_write = adv_pci1750_do_subd_write;
  subdev->insn_read  = adv_pci1750_do_subd_read;

  printk("comedi%d(adv_pci1750): PCI-1750 attached\n", dev->minor);
  return 0;
}

//------------------------------------------------------------------------------
static int adv_pci1750_do_subd_read( comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data )
{
	int chan = CR_CHAN(insn->chanspec);
	if( chan<0 || chan >= DO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(adv_pci1750_do_subdev_read): undefined chan %d. chan range is 0..%d\n", dev->minor, chan, DO_NCHANS);
		return -EINVAL;
	}

	(*data) = ( (devpriv->prev_val >> chan)&1 ? 0 : 1 ); // значение у нас инвертированное хранится
	return insn->n;
}

//------------------------------------------------------------------------------
static int adv_pci1750_do_subd_write(comedi_device* dev, comedi_subdevice* subdev,
							  comedi_insn* insn, lsampl_t* data)
{
	unsigned int chan = CR_CHAN(insn->chanspec);
	unsigned int mask = 1 << (chan%16);
	unsigned int val;

	if( chan < 0 || chan >= DO_NCHANS )
	{
		printk(KERN_ERR "comedi%d(adv_pci1750_subdev_write): undefined chan %d. chan range is 0..%d\n", dev->minor, chan,DO_NCHANS);
		return -EINVAL;
	}
	
	val = devpriv->prev_val;

	if (*data)
		val |= mask;
	else
		val &= ~mask;

	outw(val ,dev->iobase);
	devpriv->prev_val = val;

	return insn->n;
}
//------------------------------------------------------------------------------
static int adv_pci1750_di_subd_read( comedi_device* dev, comedi_subdevice* subdev,
							   comedi_insn* insn, lsampl_t* data )
{
	unsigned int chan = CR_CHAN(insn->chanspec);

    if( chan<0 || chan >= DI_NCHANS )
	{
		printk(KERN_ERR "comedi%d(adv_pci1750_di_subdev_read): undefined chan %d. chan range is 0..%d\n", dev->minor, chan, DI_NCHANS);
		return -EINVAL;
	}

	// в этой плате "инвертированная" логика.
    // 1 - нет сигнала
	// 0 - есть сигнал
	// поэтому "переворачиваем"..
	(*data) = ( inw(dev->iobase)>>chan ) & 1 ? 0 : 1;
	return insn->n;
}

//------------------------------------------------------------------------------
static int adv_pci1750_detach( comedi_device* dev )
{
	printk("comedi%d: remove adv_pci1750\n",dev->minor);

	if( dev->iobase )
	{
//		if (dev->irq) comedi_free_irq(dev->irq,dev);
		//comedi_pci_disable(devpriv->card);
		comedi_pci_disable_no_regions(devpriv->card);
	}
	return 0;
}
//------------------------------------------------------------------------------

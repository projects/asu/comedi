%define module_name	comedi
%define module_release	alt11
%define module_version	0.7.76

%define kversion	2.6.27
%define krelease	alt16.3.M50P.4.Build2
%define flavour		asu-def

%define base_arch %(echo %_target_cpu | sed 's/i.86/i386/;s/athlon/i386/')

%define module_dir /lib/modules/%kversion-%flavour-%krelease/%module_name

Name: kernel-modules-%module_name-%flavour
Version: %module_version
Release: %module_release

Summary: Data Acquisition driver for several DAQ boards
Summary(ru_RU.KOI8-R): ������� ��������� ������ ��� ��������� ���� DAQ

License: Distributable
Group: System/Kernel and hardware
URL: http://www.comedi.org

Packager: Kernel Maintainer Team <kernel@packages.altlinux.org>

ExclusiveOS: Linux

BuildPreReq: kernel-build-tools >= 0.7
BuildRequires: modutils
#BuildRequires:	perl
BuildRequires: rpm >= 4.0.2-75
BuildRequires: kernel-headers-modules-%flavour = %kversion-%krelease
BuildRequires: kernel-source-%module_name = %module_version

Provides: kernel-modules-%module_name-%kversion-%flavour-%krelease = %version-%release
Conflicts: kernel-modules-%module_name-%kversion-%flavour-%krelease < %version-%release
Conflicts: kernel-modules-%module_name-%kversion-%flavour-%krelease > %version-%release

PreReq: coreutils
PreReq: modutils
PreReq: kernel-image-%flavour = %kversion-%krelease
Requires(postun): kernel-image-%flavour = %kversion-%krelease
ExclusiveArch: %ix86

%description
Comedi is a data acquisition driver for Linux.  Together
with Comedilib, it allows Linux processes to acquire data from
supported DAQ cards.

%description -l ru_RU.KOI8-R
Comedi -- ��� ������� ��������� ������ ��� Linux. 
������ � Comedilib �� ��������� ���������������� ��������� �������� ������
�� �������������� ���� �����-����������� �����.

%prep
rm -rf kernel-source-%module_name-%module_version

tar -jxvf %kernel_src/kernel-source-%module_name-%module_version.tar.bz2

%setup -D -T -n kernel-source-%module_name-%module_version

%build
#%%__autoreconf -i -f
. %_usrsrc/linux-%kversion-%flavour/gcc_version.inc
./configure --with-linuxdir=%_usrsrc/linux-%kversion-%flavour --with-modulesdir=%buildroot/%module_dir --with-kernel-release=%kversion-%flavour-%krelease
#./configure --with-linuxdir=%_usrsrc/linux-%kversion-%flavour --with-modulesdir=%module_dir --with-modulesdeveldir=%module_dir --with-kernel-release=2.6.12-std26-up-alt10
%__make CC=gcc-$GCC_VERSION


%install
make prefix="%buildroot" install

#rm -rf %buildroot/lib/modules/%kversion-%flavour-%krelease/comedi
#mv %buildroot/lib/modules/%kversion-%flavour-%krelease/extra  %buildroot/lib/modules/%kversion-%flavour-%krelease/comedi

%post
%post_kernel_modules %kversion-%flavour-%krelease

%postun
%postun_kernel_modules %kversion-%flavour-%krelease

%files
%module_dir/*

%changelog
* Wed Feb 09 2011 Pavel Vainerman <pv@altlinux.ru> 0.7.76-alt1.132635.16.4
- add adv_uno_dio module
- for kernel 2.6.32-el-smp-alt11

* Sun Mar 29 2009 Yury Aliaev <mutabor@altlinux.ru> 0.7.76-alt1.132635.15
- built for kernel 2.6.27-alt15

* Wed Mar 25 2009 Yury Aliaev <mutabor@altlinux.ru> 0.7.76-alt1.132635.14
- new version

* Tue Jan 20 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.7.70-alt11
- new release (alt11), no comments -- Yury Aliaev

* Wed Sep 21 2005 Evgeny Sinelnikov <sin@altlinux.ru> 0.7.70-alt1.132128.1
- update spec to ALT kernel CVS specification

* Mon Sep 05 2005 Vitaly Lipatov <lav@altlinux.ru> 0.7.70-alt0.1
- first build with ALT kernel policy


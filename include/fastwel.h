/*
    fastwel.h
    header file for comedi Fastwel extensions

    COMEDI - Linux Control and Measurement Device Interface (Fastwel extension)
    Copyright (C) 2009 Yury G. Aliaev <mutabor@altlinux.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef _FASTWEL_H
#define _FASTWEL_H

#include <comedi.h>

enum configuration_ids_extra {

#ifndef INSN_CONFIG_COUNTER /* This instruction may be commented out in comedi.h */
	INSN_CONFIG_COUNTER = 19,
#endif

#ifndef INSN_CONFIG_DI_MODE
	/* Fastwel extensions */
	INSN_CONFIG_DI_MODE = 100, // Digital input mode, direct sampling or event capturing
	INSN_CONFIG_BOUNCE_SUPPRESSION = 101, // For digital inputs
	INSN_CONFIG_AVERAGING = 101, // Almost the same, but for analog
	INSN_CONFIG_INPUT_MASK = 102, // Current input mask for AIC 123
	INSN_CONFIG_0MA = 103, // 0..24 mA current output instead of 4..24 mA
	INSN_CONFIG_COMPL = 104, // Complementary DAC code instead of direct
	INSN_CONFIG_AIO_INPUT = 200, // Analog input or output (mainly for safety)
	INSN_CONFIG_AIO_OUTPUT = 201
	/* End of Fastwel extensions */
#endif
};
#endif /* _FASTWEL_H */